using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ChallengerManager : MonoBehaviour {
	public static ChallengerManager Instance;
	int CurrentDailyChallengeOrder = 0;
	int CurrentDailyChallengeType = 0;
	bool IsSameDailyChallengeCleared = false;
	List<int> ActiveWeeklyChallList ;
	DailyChallengeStaticData dailyChallengeStaticData;
	WeeklyChallengeStaticData weeklyChallengeStaticData;

	public bool IsWeeklyChallengeCompleted = false;
	public bool IsDailyChallengeCompleted = false;
	// Use this for initialization

	void Awake () {
		Instance = this;
	}

	void Start() {
		IsDailyChallengeCompleted = false;
		IsWeeklyChallengeCompleted = false;
		InitDailyChallenge ();
		InitWeeklyChallenge ();
	}

	void InitDailyChallenge() {
		dailyChallengeStaticData = DailyChallengeStaticData.Instance;
		CurrentDailyChallengeOrder = dailyChallengeStaticData.GetDailyCurrentChallengeOrder ();
		CurrentDailyChallengeType  = dailyChallengeStaticData.GetDailyCurrentChallengeType ();
		IsSameDailyChallengeCleared = dailyChallengeStaticData.IsTodayDailyChallengeCleared ();
		IntializeDailyChallenge ();
	}

	void InitWeeklyChallenge() {
		weeklyChallengeStaticData = WeeklyChallengeStaticData.Instance;
		IntializeWeeklyChallenge ();
	}

	// Update is called once per frame
	void Update () {
	
	}

	#region Weekly Challenge

	public void IntializeWeeklyChallenge() {

		if (weeklyChallengeStaticData.GetIsWeekCompleted ()) {
			CreateNewWeeklyChallenge ();
			return;	
		} 

		if (weeklyChallengeStaticData.GetWeekllyIsFirstTime()) {
			weeklyChallengeStaticData.SetWeekllyIsFirstTime ();
			CreateNewWeeklyChallenge ();
			return;
		}

		ActiveWeeklyChallList = weeklyChallengeStaticData.GetCurrentActiveWeeklyChall ();
	}

	public void CreateNewWeeklyChallenge() {
		weeklyChallengeStaticData.ResetAllWeeklyChallenges ();
		ActiveWeeklyChallList = new List<int> ();
		List<int> numbers = new List<int>();
		for (int i = 0; i < weeklyChallengeStaticData.TotalBtns ; i++) {
			numbers.Add(i);
		}

		List<int> randomNumbers = new List<int>();
		for (int i = 0; i < weeklyChallengeStaticData.ToShowBtns; i++) {
			var thisNumber = UnityEngine.Random.Range(0, numbers.Count);
			randomNumbers.Add (numbers[thisNumber]);
			numbers.RemoveAt(thisNumber);
		}

		ActiveWeeklyChallList = randomNumbers;
		weeklyChallengeStaticData.SetCurrentActiveWeeklyChall (ActiveWeeklyChallList);
		weeklyChallengeStaticData.SetWeeklyChallengeLastDate ();

		for (int i=0; i<ActiveWeeklyChallList.Count; i++) {
			Debug.Log (ChallengesTypesData.WeeklyChallengesTypesNames[ActiveWeeklyChallList[i] ] );	
		}
	}

	List<int> SortWeeklyChallenges(List<int> list) {
		List<int> NewList = new List<int>();
		int[] arr = list.ToArray ();

		for (int i = arr.Length - 1; i > 0; i--){
			for (int j = 0; j <= i - 1; j++){
				if (weeklyChallengeStaticData.Coins[arr[j]] > weeklyChallengeStaticData.Coins[arr[j + 1]]){
					int highValue = arr[j];
					arr[j] = arr[j + 1];
					arr[j + 1] = highValue;
				}
			}
		}
	
		NewList.AddRange (arr);

		return NewList;
	}

	public void CheckForWeeklyChallengeComplete() {
		IsWeeklyChallengeCompleted = false;
		for (int i=0; i<ActiveWeeklyChallList.Count; i++) {
			if(!weeklyChallengeStaticData.GetWeeklyChallCompleted (i)) {
				switch (ActiveWeeklyChallList[i]) {
				case 0:	WeeklyCollectCoins (i);
					break;
				case 1: WeeklyCollectGems (i);
					break;
				case 2:	WeeklyCollectpowerups (i);
					break;
				case 3:	WeeklyRun (i);
					break;
				case 4: WeeklyScorepoints (i);
					break;
				}
			}
		
		}
	}

	void WeeklyCollectCoins(int Order) {
		if((weeklyChallengeStaticData.GetCurrentActiveWeeklyChallSavedValue (Order) + DataManager.instance.getLevelCoins ()) >= GetWeeklyChallengeValue(Order)) {
			weeklyChallengeStaticData.SetCurrentActiveWeeklyChallSavedValue(Order ,weeklyChallengeStaticData.GetWeeklyChallValue (Order));
			OnCompleteWeeklyChallenge(Order);
		}
		else {
			weeklyChallengeStaticData.SetCurrentActiveWeeklyChallSavedValue(Order ,weeklyChallengeStaticData.GetCurrentActiveWeeklyChallSavedValue (Order) + DataManager.instance.getLevelCoins ());
		}
	}

	void WeeklyCollectGems(int Order) {
		if((weeklyChallengeStaticData.GetCurrentActiveWeeklyChallSavedValue (Order) + DataManager.instance.getLifeSaversFromGame ()) >= GetWeeklyChallengeValue(Order)) {
			weeklyChallengeStaticData.SetCurrentActiveWeeklyChallSavedValue(Order ,weeklyChallengeStaticData.GetWeeklyChallValue (Order));
			OnCompleteWeeklyChallenge(Order);
		}
		else {
			weeklyChallengeStaticData.SetCurrentActiveWeeklyChallSavedValue(Order ,weeklyChallengeStaticData.GetCurrentActiveWeeklyChallSavedValue (Order) + DataManager.instance.getLifeSaversFromGame ());
		}
	}

	void WeeklyCollectpowerups(int Order) {
		if((weeklyChallengeStaticData.GetCurrentActiveWeeklyChallSavedValue (Order) + DataManager.instance.TotalPowerInARun ()) >= GetWeeklyChallengeValue(Order)) {
			weeklyChallengeStaticData.SetCurrentActiveWeeklyChallSavedValue(Order ,weeklyChallengeStaticData.GetWeeklyChallValue (Order));
			OnCompleteWeeklyChallenge(Order);
		}
		else {
			weeklyChallengeStaticData.SetCurrentActiveWeeklyChallSavedValue(Order ,weeklyChallengeStaticData.GetCurrentActiveWeeklyChallSavedValue (Order) + DataManager.instance.TotalPowerInARun ());
		}
	}

	void WeeklyRun(int Order) {
		if((weeklyChallengeStaticData.GetCurrentActiveWeeklyChallSavedValue (Order) + DataManager.instance.getLongestRunFromGame ()) >= GetWeeklyChallengeValue(Order)) {
			weeklyChallengeStaticData.SetCurrentActiveWeeklyChallSavedValue(Order ,weeklyChallengeStaticData.GetWeeklyChallValue (Order));
			OnCompleteWeeklyChallenge(Order);
		}
		else {
			weeklyChallengeStaticData.SetCurrentActiveWeeklyChallSavedValue(Order ,weeklyChallengeStaticData.GetCurrentActiveWeeklyChallSavedValue (Order) + DataManager.instance.getLongestRunFromGame ());
		}
	}

	void WeeklyScorepoints(int Order) {
		if((weeklyChallengeStaticData.GetCurrentActiveWeeklyChallSavedValue (Order) + DataManager.instance.getScore ()) >= GetWeeklyChallengeValue(Order)) {
			weeklyChallengeStaticData.SetCurrentActiveWeeklyChallSavedValue(Order ,weeklyChallengeStaticData.GetWeeklyChallValue (Order));
			OnCompleteWeeklyChallenge(Order);
		}
		else {
			weeklyChallengeStaticData.SetCurrentActiveWeeklyChallSavedValue(Order ,weeklyChallengeStaticData.GetCurrentActiveWeeklyChallSavedValue (Order) + DataManager.instance.getScore ());
		}
	}

	int GetWeeklyChallengeValue(int Order) {
		return weeklyChallengeStaticData.GetWeeklyChallValue(Order);
	}

	void OnCompleteWeeklyChallenge(int Order) {
	
		if(weeklyChallengeStaticData.GetIsWeeklyChallCoins(Order)) {
			DataManager.instance.adjustTotalCoins (weeklyChallengeStaticData.GetWeeklyChallCoins(Order));
			DataManager.instance.setTotalCoinsLifetime (weeklyChallengeStaticData.GetWeeklyChallCoins(Order));
			Updatemanager.instance.OnUpdateEarn ();
		}

		if(weeklyChallengeStaticData.GetIsWeeklyChallDiamond(Order)) {
			//DataManager.instance.adjustTotalLifeSavers (weeklyChallengeStaticData.GetWeeklyChallCoins(Order));
			DataManager.instance.setTotalLifeSavers (weeklyChallengeStaticData.GetWeeklyChallCoins(Order));
			Updatemanager.instance.OnUpdateEarn ();
		}

		weeklyChallengeStaticData.SetWeeklyChallCompleted (Order);
		ChallegneGameOverManager.Instance.ShowWeeklyChallenge (Order);
		Debug.Log ("Weekly Challenge Completed" + weeklyChallengeStaticData.GetWeeklyChallName (Order));
		IsWeeklyChallengeCompleted = true;
	}

	#endregion








	#region Daily Challenges

	public void IntializeDailyChallenge() {
		//Debug.Log ("IsSameDailyChallengeCleared  "+ IsSameDailyChallengeCleared);
		//Debug.Log ("IsDailyChallengeDayPassed  "+ DailyChallengeStaticData.Instance.IsDailyChallengeDayPassed());
		//Debug.Log ("CurrentDailyChallengeOrder "+ CurrentDailyChallengeOrder);
		//Debug.Log ("CurrentDailyChallengeType  "+ (ChallengesTypesData.DailyChallengesTypesNames[dailyChallengeStaticData.GetDailyCurrentChallengeType()]) );


		if (DailyChallengeStaticData.Instance.IsDailyChallengeDayPassed()) {
			if(!IsSameDailyChallengeCleared) {
				ResetDailyChallenges();
			}
			CreateDailyChallenge();
			return;	
		}
	}
	
	public void CreateDailyChallenge() {
		int Rand = NewDailyChallengeTypeNo();
		CurrentDailyChallengeOrder = dailyChallengeStaticData.GetDailyCurrentChallengeOrder ();
		CurrentDailyChallengeType = Rand;
		IsSameDailyChallengeCleared = false;
		DailyChallengeTypeIntialize ();
		ResetOnNewChallenge ();
	}

	void ResetOnNewChallenge() {
		dailyChallengeStaticData.SetDailyCurrentChallengeType (CurrentDailyChallengeType);
		dailyChallengeStaticData.SetDailyChallengeSavedValue (0);
		dailyChallengeStaticData.SetIsTodayDailyChallengeCleared (false);
		dailyChallengeStaticData.SetCoinsRandomValue ();

		Debug.Log ("OnCreateDailyChallenge");
		Debug.Log ("Challenge Name: "+ ChallengesTypesData.DailyChallengesTypesNames[dailyChallengeStaticData.GetDailyCurrentChallengeType ()]);
		Debug.Log ("Challenge Value: "+ ChallengesTypesData.DailyChallengesTypesValues[dailyChallengeStaticData.GetDailyCurrentChallengeType ()]);
		Debug.Log ("Challenge Coins: "+ dailyChallengeStaticData.GetCoins(dailyChallengeStaticData.GetDailyCurrentChallengeOrder ()));
	}

	int NewDailyChallengeTypeNo() {
		for(int i= 0; i < dailyChallengeStaticData.GetDailyCompletedChallengesTypes ().Count ; i++) {
			//Debug.Log ("Completed Challenges " + (ChallengesTypesData.DailyChallengesTypesNames[ dailyChallengeStaticData.GetDailyCompletedChallengesTypes ()[i]]));
		}

		List<int> numbers = new List<int>();
		for (int i = 0; i < ChallengesTypesData.TotalDailyChallenges ; i++) {
			numbers.Add(i);
		}

		Debug.Log ("Total Challenges "+ numbers.Count);
		for (int i= 0; i < dailyChallengeStaticData.GetDailyCompletedChallengesTypes ().Count; i++) {
			numbers.Remove (dailyChallengeStaticData.GetDailyCompletedChallengesTypes ()[i]);	
		}

		if (numbers.Count == 0) {
			dailyChallengeStaticData.AllChallengesCompleted ();
			Debug.Log ("AllChallengesCompleted");
			for (int i = 0; i < ChallengesTypesData.TotalDailyChallenges ; i++) {
				numbers.Add(i);
			}
		}

		Debug.Log ("Remianing Challenges "+numbers.Count);
		int Rand = UnityEngine.Random.Range (0, numbers.Count - 1);
		return numbers [Rand];
	}

	public void ResetDailyChallenges() {
		dailyChallengeStaticData.ResetDailyChallenges ();
	}
	
	void DailyChallengeTypeIntialize() {
		switch(CurrentDailyChallengeType) {
		case 0 :   dailyChallengeStaticData.SetShowDailyProgressBar (true); break;
		case 1 :   break;
		case 2 :   dailyChallengeStaticData.SetShowDailyProgressBar (true); break;
		case 3 :   break;
		case 4 :   dailyChallengeStaticData.SetShowDailyProgressBar (true); break;
		case 5 :   break;
		case 6 :   dailyChallengeStaticData.SetShowDailyProgressBar (true); break;
		case 7 :   dailyChallengeStaticData.SetShowDailyProgressBar (true); break;
		case 8 :   break;
		case 9 :   dailyChallengeStaticData.SetShowDailyProgressBar (true); DataManager.instance.ActivateSaveMeUsedInGame (); break;
		case 10 :  dailyChallengeStaticData.SetShowDailyProgressBar (true); break;
		case 11 :  dailyChallengeStaticData.SetShowDailyProgressBar (true); break;
		}
	}
	
	public void CheckForDailyChallengeComplete() {
		IsDailyChallengeCompleted = false;
		if (IsSameDailyChallengeCleared) {
			return;
		}

		switch(CurrentDailyChallengeType) {
		case 0 : DailyChallengeTypeRun(false); 	break;
		case 1 : DailyChallengeTypeRun(true); 	 break;
		case 2 : DailyChallengeTypeScore(false); 	break;
		case 3 : DailyChallengeTypeScore(true); break;
		case 4 : DailyChallengeTypeCollectCoin(false); break;
		case 5 : DailyChallengeTypeCollectCoin(true); break;
		case 6 : DailyChallengeTypeCollectPowerUp(); break;
		case 7 : DailyChallengeTypeCollectGems(false); break;
		case 8 : DailyChallengeTypeCollectGems(true); break;
		case 9 : DailyChallengeTypeUseSaveMe(); break;
		case 10 : DailyChallengeTypeUseBlastOff(); break;
		case 11 : DailyChallengeTypeCollectSecrecyBox(); break;
		}
	}

	void DailyChallengeTypeRun(bool InOneRun) {

		if (InOneRun) {
			if( GetChallengeValue(1) <= DataManager.instance.getLongestRunFromGame ()) {;
				OnDailyChallengeComplete();
			}
		} else {
			if(GetChallengeValue(0) <= (dailyChallengeStaticData.GetDailyChallengeSavedValue () + DataManager.instance.getLongestRunFromGame ())) {
				dailyChallengeStaticData.SetDailyChallengeSavedValue (GetChallengeValue(0));
				OnDailyChallengeComplete();
			}
			else {
				dailyChallengeStaticData.SetDailyChallengeSavedValue (dailyChallengeStaticData.GetDailyChallengeSavedValue () + DataManager.instance.getLongestRunFromGame ());
			}
		}
	}

	void DailyChallengeTypeScore(bool InOneRun) {
		if (InOneRun) {
			if( GetChallengeValue(3) <= DataManager.instance.getScore (true)) {
				OnDailyChallengeComplete();
			}
			
		} else {
			if(GetChallengeValue(2) <= (dailyChallengeStaticData.GetDailyChallengeSavedValue () + DataManager.instance.getScore (true))) {
				dailyChallengeStaticData.SetDailyChallengeSavedValue (GetChallengeValue(2));
				OnDailyChallengeComplete();
			}
			else {
				dailyChallengeStaticData.SetDailyChallengeSavedValue (dailyChallengeStaticData.GetDailyChallengeSavedValue () + DataManager.instance.getScore (true));
			}
		}
	}

	void DailyChallengeTypeCollectCoin(bool InOneRun) {
		if (InOneRun) {
			if(GetChallengeValue(5) <= DataManager.instance.getLevelCoins ()) {
				OnDailyChallengeComplete();
			}
			
		} else {
			if(GetChallengeValue(4) <= (dailyChallengeStaticData.GetDailyChallengeSavedValue () + DataManager.instance.getLevelCoins ())) {
				dailyChallengeStaticData.SetDailyChallengeSavedValue (GetChallengeValue(4));
				OnDailyChallengeComplete();
			}
			else {
				dailyChallengeStaticData.SetDailyChallengeSavedValue (dailyChallengeStaticData.GetDailyChallengeSavedValue () + DataManager.instance.getLevelCoins ());
			}
		}
	}

	void DailyChallengeTypeCollectPowerUp() {
		if (GetChallengeValue (6) <= (dailyChallengeStaticData.GetDailyChallengeSavedValue () + DataManager.instance.TotalPowerInARun ())) {
			dailyChallengeStaticData.SetDailyChallengeSavedValue (GetChallengeValue(6));
			OnDailyChallengeComplete ();
		} else {
			dailyChallengeStaticData.SetDailyChallengeSavedValue (dailyChallengeStaticData.GetDailyChallengeSavedValue () + DataManager.instance.TotalPowerInARun ());
		}
	}

	void DailyChallengeTypeCollectGems(bool InOneRun) {
		if (InOneRun) {
			if( GetChallengeValue(8) <= DataManager.instance.getLifeSaversFromGame()) {
				OnDailyChallengeComplete();
			}
		} else {
			if(GetChallengeValue(7) <= (dailyChallengeStaticData.GetDailyChallengeSavedValue () + DataManager.instance.getLifeSaversFromGame ())) {
				dailyChallengeStaticData.SetDailyChallengeSavedValue (GetChallengeValue(7));
				OnDailyChallengeComplete();
			}
			else {
				dailyChallengeStaticData.SetDailyChallengeSavedValue (dailyChallengeStaticData.GetDailyChallengeSavedValue () + DataManager.instance.getLifeSaversFromGame ());
			}
		}
	}

	void DailyChallengeTypeUseSaveMe() {
		if(GetChallengeValue(9) <= (dailyChallengeStaticData.GetDailyChallengeSavedValue () + DataManager.instance.GetSaveMeUsedInGame ())) {
			dailyChallengeStaticData.SetDailyChallengeSavedValue (GetChallengeValue(9));
			OnDailyChallengeComplete();
		} else {
			dailyChallengeStaticData.SetDailyChallengeSavedValue (dailyChallengeStaticData.GetDailyChallengeSavedValue () + DataManager.instance.GetSaveMeUsedInGame ());
		}
	}

	void DailyChallengeTypeUseBlastOff() {
		if(GetChallengeValue(10) <= (dailyChallengeStaticData.GetDailyChallengeSavedValue () + DataManager.instance.GetBlastUsedInARun ())) {
			dailyChallengeStaticData.SetDailyChallengeSavedValue (GetChallengeValue(10));
			OnDailyChallengeComplete();
		} else {
			dailyChallengeStaticData.SetDailyChallengeSavedValue (dailyChallengeStaticData.GetDailyChallengeSavedValue () + DataManager.instance.GetBlastUsedInARun ());
		}
	}

	void DailyChallengeTypeCollectSecrecyBox() {
		if( GetChallengeValue(11) <= (dailyChallengeStaticData.GetDailyChallengeSavedValue () + DataManager.instance.TotalSecrecyBoxInARun ())) {
			dailyChallengeStaticData.SetDailyChallengeSavedValue (GetChallengeValue(11));
			OnDailyChallengeComplete();
		} else {
			dailyChallengeStaticData.SetDailyChallengeSavedValue (dailyChallengeStaticData.GetDailyChallengeSavedValue () + DataManager.instance.TotalSecrecyBoxInARun ());
		}
	}

	int GetChallengeValue(int Order) {
		return dailyChallengeStaticData.GetDailyChallengeValue (Order);
	}

	void OnDailyChallengeComplete() {
		DataManager.instance.adjustTotalCoins (dailyChallengeStaticData.GetCoins(dailyChallengeStaticData.GetDailyCurrentChallengeOrder()));
		DataManager.instance.setTotalCoinsLifetime (dailyChallengeStaticData.GetCoins(dailyChallengeStaticData.GetDailyCurrentChallengeOrder()));
		Updatemanager.instance.OnUpdateEarn ();

		Debug.Log ("OnDailyChallengeComplete");
		Debug.Log ("Challenge Name: "+ ChallengesTypesData.DailyChallengesTypesNames[dailyChallengeStaticData.GetDailyCurrentChallengeType ()]);
		Debug.Log ("Challenge Value: "+ ChallengesTypesData.DailyChallengesTypesValues[dailyChallengeStaticData.GetDailyCurrentChallengeType ()]);
		Debug.Log ("Challenge Coins: "+ dailyChallengeStaticData.GetCoins(dailyChallengeStaticData.GetDailyCurrentChallengeOrder ()));

		dailyChallengeStaticData.SetDailyChallengeCompleted (dailyChallengeStaticData.GetDailyCurrentChallengeOrder ());
		dailyChallengeStaticData.SetDailyCompletedChallengesTypes (CurrentDailyChallengeType);
		dailyChallengeStaticData.SetDailyChallengeLastDate ();
		dailyChallengeStaticData.SetDailyChallengeSavedValue (0);
		dailyChallengeStaticData.SetIsTodayDailyChallengeCleared (true);
		dailyChallengeStaticData.SetShowDailyProgressBar (false);
		dailyChallengeStaticData.SetDailyCurrentChallengeOrder (dailyChallengeStaticData.GetDailyCurrentChallengeOrder () + 1);

		IsSameDailyChallengeCleared = true;
	
		ChallegneGameOverManager.Instance.ShowDailyChallenge ();
		IsDailyChallengeCompleted = true;

//		LocalNotification.Instance.SetAlaram ();
	}
	#endregion
	
}
