﻿using UnityEngine;
using System.Collections;
using System;

public class SecrecyboxManager : MonoBehaviour {
	public static SecrecyboxManager Instance;
	public GameObject SecrecyBox;
	public GameObject[] Screens;
	public GameObject SunEffect;
	public GameObject TaptoContinue;
	public tk2dTextMesh Title;
	public tk2dTextMesh Description;
	public bool IsContinue = false;
	GameOverType Temp;
	GameObject Instant ;
	int CurrentBox = 0;
	// Use this for initialization
	void Awake() {
		Instance = this;
	}

	void OnEnable() {
	}

	public void OnTapTocontinue () {
		if (!IsContinue)
			return;
		Destroy (Instant);
		TaptoContinue.SetActive (false);
		GameManager.instance.CancelingGamePauseFromSecrecyBox ();
		GameManager.instance.gameOver (Temp, true);
		SecrecyBox.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		if (!SunEffect.GetComponent<Animation>().isPlaying)
						SunEffect.GetComponent<Animation>().Play ();
	}

	public void ShowSecrecy(GameOverType gameOverType) {
		Temp = gameOverType;
		SecrecyBox.SetActive (true);
		Title.text = "";
		Description.text = "";
		StartCoroutine ("delay");
	}

	IEnumerator delay()
	{
		int Rand = UnityEngine.Random.Range (0, 100);
		if(Rand >= 0 && Rand <= 39)
			Rand = 4;
			else if(Rand >= 40 && Rand <= 72)
					Rand = 5;
					else if(Rand >= 73 && Rand <= 76)
							Rand = 6;
							else if(Rand >= 77 && Rand <= 80)
								Rand = 7;
								else if(Rand >= 81 && Rand <= 84)
									Rand = 8;	
									else
									{
										int Rand1 = UnityEngine.Random.Range (0, 100);
										if(Rand1 <= 25)
											Rand = 0;
											else if(Rand1 <= 50)
													Rand = 1;
													else if(Rand1 <= 75)
															Rand = 2;
																else
																	Rand = 3;
									}													
		CurrentBox = Rand;
		Instant = (GameObject) Instantiate (Screens [Rand]);
		Instant.transform.parent = SecrecyBox.transform;
		Instant.transform.localPosition = new Vector3 (0, 0, 0);
		//Screens [Rand].transform.localPosition = new Vector3 (0, 0, 0);
		switch(Rand)
		{
		case 0: DataManager.instance.upgradePowerUp(PowerUpTypes.SixMultiplier);break;
		case 1: DataManager.instance.upgradePowerUp(PowerUpTypes.SpeedIncrease);break;
		case 2: DataManager.instance.setLifeSaversFromGame (1); break;
		case 3: DataManager.instance.upgradePowerUp(PowerUpTypes.Invincibility);break;
		//case 4: //DataManager.instance.upgradePowerUp();break;
		case 4:  DataManager.instance.addToCoins (200); break;
		case 5:  DataManager.instance.addToCoins (500); break;
		case 6:  DataManager.instance.addToCoins (1000); break;
		case 7:  DataManager.instance.addToCoins (2000); break;
		case 8:  DataManager.instance.addToCoins (3000); break;
		}
		yield return new WaitForSeconds(3);
		Title.text = "";
		Description.text = "";
		yield return new WaitForSeconds(1);
		TaptoContinue.SetActive (true);
		IsContinue = true;
	}
}
