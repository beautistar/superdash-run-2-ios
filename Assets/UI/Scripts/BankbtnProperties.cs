﻿using UnityEngine;
using System.Collections;

public class BankbtnProperties : MonoBehaviour {

	public static BankbtnProperties Instance;
	public tk2dSprite Symbol;
	public tk2dSprite SymbolColor;
	public tk2dTextMesh Value;
	public tk2dTextMesh Cost;
	public tk2dTextMesh Title;
	public int OrderInList ;
	public BankStaticData bankStaticData;

	public GameObject Buy;
	public GameObject BuyRestore;
	string AmountSymbol = "$ ";

	// Use this for initialization
	void Awake() {
		if (Instance == null || Instance != this)
						Instance = this;
	}

	void Start () {
	}

	public void OnBuyClickCoin()
	{
		BankManager.Instance.OnBuyClickCoin (OrderInList);
	}

	public void OnBuyClickLifeSaver()
	{
		BankManager.Instance.OnBuyClickLifeSaver (OrderInList);
	}

	public void OnRestoreClick()
	{
		BankManager.Instance.OnRestoreClick ();
	}

	public void SetDetailsCoin(int Order)
	{
		bankStaticData = BankStaticData.Instance;
		Title.text = bankStaticData.getCoinTitle (Order);
		Value.text = bankStaticData.getCoins (Order).ToString ();
		Cost.text = AmountSymbol +  bankStaticData.getCoinsAmount (Order).ToString ();
		Symbol.SetSprite (bankStaticData.getCoinsImage (Order));
		SymbolColor.color = bankStaticData.getCoinsColor (Order);
		OrderInList = Order;
	}

	public void SetAdsRestore(int Order)
	{
		bankStaticData = BankStaticData.Instance;
		Title.text = bankStaticData.getCoinTitle (Order);
		Cost.text = AmountSymbol + bankStaticData.getCoinsAmount (Order).ToString ();
		Symbol.SetSprite (bankStaticData.getCoinsImage (Order));
		SymbolColor.color = bankStaticData.getCoinsColor (Order);
		OrderInList = Order;
		BuyRestore.SetActive (true);
		Buy.SetActive (true);
		if (Order == 0) {
			if (!bankStaticData.getAds ())
				Buy.SetActive (false);
			Value.text = "No More Ads";
				
		} else {
			if(bankStaticData.getDoubleCoins ())
				Buy.SetActive (false);
			Value.text = "";
		}
	}

	public void SetDetailsLifeSaver(int Order)
	{
		bankStaticData = BankStaticData.Instance;
		Title.text = bankStaticData.getLifeSaverTitle (Order);
		Value.text = bankStaticData.getLifeSavers (Order).ToString () + " Diamonds";
		Cost.text =  AmountSymbol + bankStaticData.getLifeSaverAmount (Order).ToString () ;
		Symbol.SetSprite (bankStaticData.getLifeSaverImage (Order));
		SymbolColor.color = bankStaticData.getLifeSaverColor (Order);
		OrderInList = Order;
	}

	public void ShowBuy() {
		Buy.SetActive (true);
	}
	public void HideBuy() {
		Buy.SetActive (false);
	}

	public void ShowBuyRestore() {
		BuyRestore.SetActive (true);
	}

	public void HideBuyRestore() {
		BuyRestore.SetActive (false);
	}
	
}
