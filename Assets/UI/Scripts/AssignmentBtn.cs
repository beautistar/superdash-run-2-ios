﻿using UnityEngine;
using System.Collections;

public class AssignmentBtn : MonoBehaviour {
	public tk2dTextMesh Title;
	public tk2dTextMesh Description;
	public int Goal;

	public bool IsCleared = false;
	public tk2dSprite Tick;

	// Use this for initialization
	void Start () {
	
		//SetTick ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SetTick(int Value)
	{
		if (Value == 1) {	
			Tick.SetSprite ("right_sign"); 
			IsCleared = true;
		} 
		else {
			Tick.SetSprite ("pendingicon"); 
		}
	}
}
