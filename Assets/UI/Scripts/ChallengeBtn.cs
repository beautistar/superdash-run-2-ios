using UnityEngine;
using System.Collections;

public class ChallengeBtn : MonoBehaviour {
	public int Order;
	public tk2dTextMesh HeaderText;
	public tk2dSprite Symbol;
	public tk2dSprite Front;
	public GameObject Tick;
	public tk2dSprite CoinsSymbol;
	public tk2dTextMesh Coins;
	
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void SetDetails(int order, bool IsCompleted) {
		this.Order = order;
		if (Order >= DailyChallengeStaticData.Instance.TotalBtns) {
			Debug.Log ("Order "+order);
			Debug.Log ("TotalBtns "+DailyChallengeStaticData.Instance.TotalBtns);
			HeaderText.text = "Day "+(order + 1);	
			Coins.text = DailyChallengeStaticData.Instance.GetCoins (order).ToString ();
			CoinsSymbol.SetSprite (DailyChallengeStaticData.Instance.CoinsIcon [DailyChallengeStaticData.Instance.TotalBtns ]);
			if (IsCompleted) 
				ShowTick ();
			else
				ResetBtn ();
			
			Debug.Log ("HeaderText " + HeaderText.text);
			Debug.Log ("Coins " + Coins.text);
			Debug.Log ("CoinsSymbol " + CoinsSymbol.name);	
			return;
			
		}
		this.Order = order;
		HeaderText.text = DailyChallengeStaticData.Instance.GetHeaderName (Order);
		Symbol.SetSprite (DailyChallengeStaticData.Instance.GetSymbolName(Order));
		Front.color = DailyChallengeStaticData.Instance.BtnsColor [Order];
		CoinsSymbol.SetSprite (DailyChallengeStaticData.Instance.CoinsIcon [Order]);
		Coins.text = DailyChallengeStaticData.Instance.GetCoins (order).ToString ();

		if (IsCompleted) 
			ShowTick ();
		else
			ResetBtn();

	}

	public void SetDetailsGameOver(int order, bool IsCompleted) {
		if (Order < DailyChallengeStaticData.Instance.TotalBtns) {
			HeaderText.text = DailyChallengeStaticData.Instance.GetHeaderName (DailyChallengeStaticData.Instance.HeaderName.Length );	
			Coins.text = DailyChallengeStaticData.Instance.GetCoins (order).ToString ();
			CoinsSymbol.SetSprite (DailyChallengeStaticData.Instance.CoinsIcon [DailyChallengeStaticData.Instance.TotalBtns ]);
			if (IsCompleted) 
					ShowTick ();
			else
					ResetBtn ();

			Debug.Log ("HeaderText " + HeaderText.text);
			Debug.Log ("Coins " + Coins.text);
			Debug.Log ("CoinsSymbol " + CoinsSymbol.name);	
			return;
				
		}
	}

	public void SetDetailsWeekly(int order) {
		this.Order = order;
		HeaderText.text = "";
		Symbol.SetSprite (WeeklyChallengeStaticData.Instance.GetWeeklyChallSymbolName (order));
		CoinsSymbol.SetSprite (WeeklyChallengeStaticData.Instance.GetCoinsSymbolName (WeeklyChallengeStaticData.Instance.GetIsWeeklyChallCoins (order)));
		Coins.text = WeeklyChallengeStaticData.Instance.GetWeeklyChallCoins (order).ToString ();
		ShowTick ();	
	}


	public void ShowTick() {
		Tick.SetActive (true);
		Color Col = Color.white;
		Col.a = 0.6f;
		Symbol.color = Col;
	}

	public void ResetBtn() {
		Tick.SetActive (false);
		Color Col = Color.white;
		Col.a = 1f;
		Symbol.color = Col;
	}
}
