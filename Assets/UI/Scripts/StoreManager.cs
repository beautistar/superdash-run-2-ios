﻿using UnityEngine;
using System.Collections;

public enum StoreMenus	{ Character, PermanentPurchases , Upgrade, SingleUse };

public class StoreManager : MonoBehaviour {
	public tk2dUIScrollableArea scrollbarArea;
	public int Childs =0;
	public bool Run = false;
	public int StretchValue = 2;
	public bool IsActive = false;
	public float PixelsPerUnit = 71;
	public int DescriptionLength = 2;
	public StoreMenuScript[] InstanceOfChilds ;
	public static StoreManager Instance;
	public StoreMenuScript CurrentActiveStoreMenu;
	public Vector2 IntialPosition = new Vector2(0,4);

	public GameObject CharacterBtn;
	public GameObject PermanentBtn;
	public GameObject UpgradeBtn;
	public GameObject SingleUseBtn;
	bool NotFirstTime = false;
	int SingleUseValue = 4;
	int storeOrder = 0;
	int buttonOrder = 0;
	GameObject[] TempInstanceOfChilds;
	// Use this for initialization
	void Awake() {
		if(Instance == null || Instance != this)
			Instance = this;

	}

	void OnEnable(){
		if (NotFirstTime) {
			SetDetails ();
		} 
		scrollbarArea.OnScroll += HandleOnScroll;
	}

	void OnDisable(){
		scrollbarArea.OnScroll -= HandleOnScroll;
	}


	void HandleOnScroll (tk2dUIScrollableArea obj){
		/*if (obj.Value == 1) {
			obj.allowSwipeScrolling  = false;

		} else
			obj.allowSwipeScrolling  = true;
		Debug.Log (obj.VisibleAreaLength);*/

		//Debug.Log (	"VisibleAreaLength :"+obj.VisibleAreaLength);	
		//Debug.Log (	"ContentLength :"+obj.ContentLength);
	}

	void SetDetails() {
		StaticFunction.SlideValue = this.StretchValue;
		SingleUseValue = DataManager.instance.SingleUse;
	
		SetDetails (0, CharacterBtn);
		//SetDetails (1, PermanentBtn);
		SetDetails (2, UpgradeBtn);
		SetDetails (3, SingleUseBtn);

		if (NotFirstTime) {
			if(CurrentActiveStoreMenu != null && IsActive)
				CurrentActiveStoreMenu.CurrentActiveBtn.Reset ();		
		}
	}

	void DeleteSecrecyBox() {

	}

	void Start () {	
		SetDetails ();
		NotFirstTime = true;
	}
	
	public void ActiveCalled(int OrderInList) {
		if (TempInstanceOfChilds == null) {		
			TempInstanceOfChilds = new GameObject[InstanceOfChilds.Length];
			for (int i=0; i<InstanceOfChilds.Length; i++)
				TempInstanceOfChilds [i] = InstanceOfChilds [i].gameObject;
		}

		StaticFunction.CalledFunct (OrderInList, true, TempInstanceOfChilds);	

		CurrentActiveStoreMenu = InstanceOfChilds [OrderInList];
		IsActive = true;
		StartCoroutine ("IncreaseContentlength", 2);
	}
	
	public void NormalCalled(int OrderInList) {
		StaticFunction.CalledFunct (OrderInList, false,TempInstanceOfChilds);
		IsActive = false;
		StartCoroutine ("IncreaseContentlength", -2);
	}
	
	IEnumerator IncreaseContentlength(int Value){  // Fixes the scrollbar length according to no of player records.
		yield return null;
	
		//float f = scrollbarArea.GetComponent<tk2dUIScrollableArea> ().measurecontentlength() ;
		scrollbarArea.GetComponent<tk2dUIScrollableArea> ().ContentLength += Value;
		if (scrollbarArea.Value > 0.91f)
			scrollbarArea.Value = 1.0f;
	}

	public void OnStoreBuyClick(int StoreOrder, int ButtonOrder) {
		this.storeOrder = StoreOrder;
		this.buttonOrder = ButtonOrder;

		StoreMenus Menu = (StoreMenus)StoreOrder;

		switch(Menu) {
		case StoreMenus.Character : 	int cost = DataManager.instance.getCharacterCost(buttonOrder);
										if (DataManager.instance.getTotalCoins() < cost) {
											UIGameManager.Instance.PopUp ("Insuffiecient Coins");
										return;
										}
										GameManager.instance.purchaseCharacter (ButtonOrder);	break;
		case StoreMenus.PermanentPurchases :    GameManager.instance.purchasePermamnentPurchase (ButtonOrder); 
												break;
		case StoreMenus.Upgrade : 				this.buttonOrder = ButtonOrder +1; GameManager.instance.upgradePowerUp ((PowerUpTypes) ButtonOrder + 1); ;
												break;
		case StoreMenus.SingleUse :				GameManager.instance.purchaseSingleUse ((PowerUpTypes) ButtonOrder + SingleUseValue); 
												break;
		}
	}

    public void OnSuccessFullBuy() {
		InstanceOfChilds[storeOrder].InstanceOfChilds[buttonOrder].CoinsObject.text = "";
		InstanceOfChilds[storeOrder].InstanceOfChilds[buttonOrder].InstanceStoreDroplist.HideBuy ();
		GameManager.instance.selectCharacter(buttonOrder);
		GoogleAnalyticsScreenName ();
	}

	public void OnSuccessFullBuyUpgrade() {
		if (DataManager.instance.getPowerUpLevel ((PowerUpTypes)buttonOrder) >= DataManager.instance.GetTotalPowerLevels ()) {	
						InstanceOfChilds [storeOrder].InstanceOfChilds [buttonOrder - 1 ].CoinsObject.text = "";		
						InstanceOfChilds [storeOrder].InstanceOfChilds [buttonOrder - 1].InstanceStoreDroplist.HideBuy ();
		}
		InstanceOfChilds[2].InstanceOfChilds[buttonOrder -1 ].CoinsObject.text = DataManager.instance.getPowerUpCost ((PowerUpTypes) buttonOrder) == -1 ? "" : DataManager.instance.getPowerUpCost ((PowerUpTypes) buttonOrder).ToString ();
		InstanceOfChilds[2].InstanceOfChilds[buttonOrder -1].InstanceStoreLevel.SetLevel (DataManager.instance.getPowerUpLevel ((PowerUpTypes) buttonOrder ) - 1); 
		GoogleAnalyticsScreenName ();
	}

	public void OnSuccessFullBuySingleUse() {
		InstanceOfChilds [3].InstanceOfChilds [buttonOrder].SetYouHave (DataManager.instance.getPowerUpLevel ((PowerUpTypes)buttonOrder + SingleUseValue));
		GoogleAnalyticsScreenName ();
	}

	void SetDetails(int Count, GameObject gameObject){

		switch(gameObject.name)
		{
		case "CharacterBtn" : 	 InstanceOfChilds[Count].SetDetails (DataManager.instance.getCharacterCount(),CharacterBtn,IntialPosition.y,Count);
								IntialPosition.y -= (DataManager.instance.getCharacterCount()*2); InstanceOfChilds[Count].OrderInList = Count;	break;

		case "PermanentBtn" :   	InstanceOfChilds[Count].SetDetails (2,PermanentBtn,IntialPosition.y,Count);
							IntialPosition.y -= ((2)*2);  InstanceOfChilds[Count].OrderInList = Count;	break;

		case "UpgradeBtn" : 	 InstanceOfChilds[Count].SetDetails (DataManager.instance.SingleUse ,UpgradeBtn,IntialPosition.y,Count);  //
								IntialPosition.y -= (DataManager.instance.SingleUse*2 -2 );  InstanceOfChilds[Count].OrderInList = Count;	 break;

		case "SingleUseBtn" :	 InstanceOfChilds[Count].SetDetails (DataManager.instance.SingleUse  ,SingleUseBtn,IntialPosition.y, Count );
							IntialPosition.y -= (DataManager.instance.SingleUse*2);  InstanceOfChilds[Count].OrderInList = Count;	break;
		}

		IntialPosition.y -= 2;
	}

	public void GoogleAnalyticsScreenName()
	{
		string ScreenName = "";
		switch(storeOrder)
		{
		case 0: ScreenName = "Store.Character"+ DataManager.instance.getCharacterTitle (buttonOrder) ;  break;
			//case 1: ScreenName = "Store.PermanentPurchases"+ DataManager.instance.getCharacterTitle (buttonOrder) ;   break;
		case 2: ScreenName = "Store.Upgrade"+ DataManager.instance.getPowerUpTitle ( (PowerUpTypes) storeOrder ).ToString (); break;
		case 3: ScreenName = "Store.SingleUse"+ DataManager.instance.getPowerUpTitle ( (PowerUpTypes) storeOrder + SingleUseValue).ToString ();  break;
			
		}
		
		UIGameManager.Instance.GoogleAnalyticsScreenName (ScreenName);
	}

}
