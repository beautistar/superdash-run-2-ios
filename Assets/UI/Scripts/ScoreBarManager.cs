﻿using UnityEngine;
using System.Collections;

public class ScoreBarManager : MonoBehaviour {
	public enum ScoreBars	{ Coins, Diamonds };
	public static ScoreBarManager instance;
	public tk2dTextMesh Score;
	public tk2dTextMesh Diamond;
	void Awake() {
		instance = this;	
	}

	// Use this for initialization
	void Start () {
		Score.text = DataManager.instance.getTotalCoins ().ToString ();
		Diamond.text = DataManager.instance.getTotalLifeSavers ().ToString ();
	}

	public void OnUpdate() {
		//StartCoroutine (Delay());
		Score.text = DataManager.instance.getTotalCoins ().ToString ();
		Diamond.text = DataManager.instance.getTotalLifeSavers ().ToString ();
	}

	IEnumerator Delay() {
		int CurrentScore = System.Int32.Parse (Score.text);
		int diff = DataManager.instance.getTotalCoins () - CurrentScore;
		for(int i = 0; i <diff ; i++ ) {
			CurrentScore += i;
			yield return new WaitForSeconds(0.001f);
			Score.text = CurrentScore.ToString ();
		}

		yield return null;
	}

	public void OnCoinBarClick() {
		MainMenuManager.LoadScreen ("02Bank"); 
	}

	public void OnDiamondBarClick() {
		MainMenuManager.LoadScreen ("02Bank"); 
	}
}
