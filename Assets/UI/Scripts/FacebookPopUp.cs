﻿using UnityEngine;
using System.Collections;
using Facebook.Unity;

public class FacebookPopUp : MonoBehaviour {
	public static FacebookPopUp Instance;
	public GameObject facebookPopup;
	public int RemindingDays;
	public int LoginCoins = 7000;

	// Use this for initialization
	void Awake () {
		Instance = this;
	}
	
	
	void OnEnable() {
		FBIntegrate.OnFBLoggedIn += HandleOnFBLoggedIn;
		FBIntegrate.OnFBLoginFailed += HandleOnFBLoginFailed;;
	}
	
	
	void OnDisable() {
		FBIntegrate.OnFBLoggedIn -= HandleOnFBLoggedIn;
		FBIntegrate.OnFBLoginFailed -= HandleOnFBLoginFailed;
	}


	public void Init() {
		StartRatePopUpNew ();
	}

	void StartRatePopUpNew() {
		//	Debug.Log ("StartRatePopUpNew "+ (PlayerPrefs.GetInt ("RemindDays", 0) % 3) + " " + PlayerPrefs.GetInt ("ShowRemindDays", 0));
		if ((PlayerPrefs.GetInt ("RemindDaysFacebook", System.DateTime.Today.Day) % RemindingDays) == 0) {
			UIGameManager.Instance.ShowWall ("",true);
			PopUpManager.Instance.IsPopOn = true;
			facebookPopup.SetActive (true);	
			
		} else {
			facebookPopup.SetActive (false);
		}
		//Are you enjoying TakeOff Running? Rate it now
	}

	public void OnConnectClick() {
		if (FB.IsLoggedIn) {
			SuccefullyLogin();
		} else {
			FBIntegrate.Instance.Login ();	
		}
		PlayerPrefs.SetInt ("RemindDaysFacebook",  System.DateTime.Today.Day + 1);
	}

	void OnCancelClick() {
		facebookPopup.SetActive (false);
		UIGameManager.Instance.HideWall ();
	}

	

	void HandleOnFBLoggedIn () {
		SuccefullyLogin ();
	}
	
	void HandleOnFBLoginFailed (string obj) {
		UnsuccesfulLogin ();
	}

	
	void SuccefullyLogin() {
		UIGameManager.Instance.HideWall ();
		/*DataManager.instance.adjustTotalCoins (LoginCoins);
		DataManager.instance.setTotalCoinsLifetime (LoginCoins);
		Updatemanager.instance.OnUpdateCoins();*/
		facebookPopup.SetActive (false);
	}

	void UnsuccesfulLogin() {
		UIGameManager.Instance.HideWall ();
		facebookPopup.SetActive (false);
	}


}
