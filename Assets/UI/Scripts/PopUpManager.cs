﻿using UnityEngine;
using System.Collections;

public class PopUpManager : MonoBehaviour {
	public static PopUpManager Instance;
	public bool IsPopOn = false;

	// Use this for initialization
	void Awake () {
		Instance = this;
	}
	
	public void InitPopUps() {

		GooglePlusPopUp.Instance.Init ();
		if (IsPopOn)
			return;

		Pushup.Instance.Init ();
		if (IsPopOn)
			return;

		RatePopUpManager.Instance.Init ();
		if (IsPopOn)
			return;

		FacebookPopUp.Instance.Init ();
	}


}
