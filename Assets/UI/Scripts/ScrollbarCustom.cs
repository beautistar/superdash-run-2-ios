using UnityEngine;
using System.Collections;

public class ScrollbarCustom : MonoBehaviour {
	public GameObject[] Screens;
	public tk2dTextMesh PlayerName ;
	public tk2dTextMesh PlayerDescription ;
	public Vector3 BasePosition;
	public bool Vertical = false;
	public int GapDistance = 7;
	public bool SlideWithAnimation = false;
	public int Count = 0;
	public float TimeToChangeScreen = 0.5f;
	public tk2dTextMesh CostText;
	static public ScrollbarCustom instance;
	bool FirstTime = false;
	float[] ScreensX  ;
	// Use this for initialization
	public void Awake(){
		instance = this;
	
	}
	public void OnEnable(){
		if(FirstTime)
			Start ();
	}

	public void SetPurchased(int Count) {
		if (DataManager.instance.getCharacterCost (Count) == -1)
						CostText.text = "Purchased";	
		else {
			CostText.text = DataManager.instance.getCharacterCost (Count).ToString ();
		}
	}

	void Start () {
		FirstTime = true;
		int PlayerNo = DataManager.instance.getSelectedCharacter ();

		SetPurchased (PlayerNo);
		int x = 0;
		Count = 0;
		for (int i=0; i<Screens.Length; i++) {
			Screens [i].transform.localPosition = new Vector3 (x, 0, 0);
			for(int j=0;j<Screens [i].transform.childCount;j++){
				Screens [i].transform.GetChild (j).localPosition = new Vector3 (0, 0, 0);
				if(i != 1 && j != 0 )
					Screens [i].transform.GetChild (j).localRotation = new Quaternion(0,0,0,0);
			}

			if(i  == 1  ) {
				Screens [i].transform.GetChild (0).localRotation = new Quaternion(0,0,0,0); 
				Screens [i].transform.GetChild (0).Rotate (new Vector3(0,340,0)); 
			}
				
			x += GapDistance;
		}

		Move (true, PlayerNo, false);
	}

	public void ScrollLeft() {
	
		Move (true, 1,SlideWithAnimation);
	}
	public void ScrollRight() {
	
		Move (false, 1,SlideWithAnimation);
	}

	void Move(bool left,int MoveCount,bool SlideWithAnimation) {
		if(iTween.tweens.Count > 0)
			return;

		if (left) {
			if (Count < Screens.Length - 1) {
				MoveFunct(-GapDistance*MoveCount,SlideWithAnimation);
				Count = ( Count + 1 ) * MoveCount;
			}
			else {
				Reset (true);
				Count = 0;
			}
		} 
		else {
			if (Count > 0) {
				MoveFunct(GapDistance*MoveCount,SlideWithAnimation);
				Count = ( Count - 1 ) * MoveCount;
			}
			else {
				Reset (false);
				Count = Screens.Length-1;
			}
		}
	
		GameManager.instance.selectCharacter(Count);
		PlayerName.text = DataManager.instance.getCharacterTitle (Count);
		PlayerDescription.text = DataManager.instance.getCharacterDescription (Count);
		SetPurchased (Count);
		UIGameManager.Instance.GoogleAnalyticsScreenName ("Change Player."+ DataManager.instance.getCharacterTitle (DataManager.instance.getSelectedCharacter ()));
		//CostText.text = DataManager.instance.getCharacterCost (Count).ToString ();
	}

	void Reset(bool Forward){
		int x = 0;
		if (Forward) {
			x = GapDistance;
			for (int i=0; i<Screens.Length-1; i++) {
				Screens [i].transform.localPosition = new Vector3 (x, 0, 0);		
					x += GapDistance;
			}
			MoveFunct(-GapDistance*1,SlideWithAnimation);
			StartCoroutine (Delay (Screens[Screens.Length-1], Screens.Length*GapDistance));
		} 
		else {
			x = -GapDistance;
			for (int i=Screens.Length - 1; i>0; i--) {
				Screens [i].transform.localPosition = new Vector3 (x, 0, 0);		
				x -= GapDistance;
			}
			MoveFunct(GapDistance*1,SlideWithAnimation);
			StartCoroutine (Delay (Screens[0], Screens.Length*-GapDistance));
		}
	}

	IEnumerator Delay(GameObject Go , int Distance){
		yield return new WaitForSeconds (TimeToChangeScreen);
		Go.transform.Translate (new Vector3 (Distance,0,0));
		//Go.transform.position = new Vector3 (Distance,0,0);
	}

	void MoveFunct(int Distance,bool SlideWithAnimation){
		for(int i=0;i<Screens.Length;i++) {

			if(SlideWithAnimation) {
				iTween.MoveBy(Screens[i].transform.gameObject, iTween.Hash("x", Distance,"time",TimeToChangeScreen, "easeType", iTween.EaseType.easeInExpo, "loopType", "none", "delay", 0));
			}
			else {
				Screens[i].transform.Translate (Distance, 0, 0);
			}
		}
	}

}
