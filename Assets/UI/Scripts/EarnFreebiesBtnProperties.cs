﻿using UnityEngine;
using System.Collections;

public class EarnFreebiesBtnProperties : MonoBehaviour {
	public tk2dSprite Symbol;
	public tk2dSprite Front;

	public tk2dTextMesh Title;
	public tk2dTextMesh Value;
	public tk2dTextMesh Description;
	public tk2dTextMesh EarnbtnText;
	public GameObject Tick;
	public GameObject Coins;
	public GameObject Earn;

	public int OrderInList;
	public EarnFreebiesStaticData EarnStaticData;
	// Use this for initialization
	void Start () {

	}

	public void OnEarned()
	{
		Coins.SetActive (false);
		Earn.SetActive (false);
		Tick.SetActive (true);
	}

	public void OnReset()
	{
		Coins.SetActive (true);
		Earn.SetActive (true);
		Tick.SetActive (false);
	}

	public void OnEarnClickCoin()
	{
		EarnFreebiesManager.Instance.OnEarnClickCoin (OrderInList);
	}
	
	public void OnEarnClickLifeSaver()
	{
		EarnFreebiesManager.Instance.OnEarnClickLifeSaver (OrderInList);
	}

	public void SetDetailsCoin(int Order) {
	
		#if UNITY_IPHONE
		EarnStaticData = EarnFreebiesStaticData.Instance;
		Title.text = EarnStaticData.getOffersCoinTitle (Order);
		Value.text = EarnStaticData.getOffersCoins (Order).ToString ();
		Description.text = EarnStaticData.getOffersCoinDesciption	(Order).ToString () ;
		Symbol.SetSprite (EarnStaticData.getOffersCoinsImage (Order));
		Front.color = EarnStaticData.getOffersCoinsColor (Order);
		OrderInList = Order;
		EarnbtnText.text = EarnStaticData.getOffersCoinsEarnText (Order);
	

		if( EarnStaticData.getOfferEarned (EarnTypes.Coins,Order) == 1) {
			OnEarned ();
		}
//		else {
//			Coins.SetActive (false); 
//		}
//
//		if(Order == 5 && EarnStaticData.getOfferEarned (EarnTypes.Coins,Order) == 0) {
//			Coins.SetActive (true); 
//		}
//
//		if(Order == 7 && EarnStaticData.getOfferEarned (EarnTypes.Coins,Order) == 0) {
//			Coins.SetActive (true); 
//		}
		#endif
	
		#if UNITY_ANDROID
		EarnStaticData = EarnFreebiesStaticData.Instance;
		Title.text = EarnStaticData.getOffersCoinTitle (Order);
		Value.text = EarnStaticData.getOffersCoins (Order).ToString ();
		Description.text = EarnStaticData.getOffersCoinDesciption	(Order).ToString () ;
		Symbol.SetSprite (EarnStaticData.getOffersCoinsImage (Order));
		Front.color = EarnStaticData.getOffersCoinsColor (Order);
		OrderInList = Order;
		EarnbtnText.text = EarnStaticData.getOffersCoinsEarnText (Order);
		if( EarnStaticData.getOfferEarned (EarnTypes.Coins,Order) == 1) {
			OnEarned ();
		}
		#endif

	}

	public void SetDetailsLifeSaver(int Order)
	{
		EarnStaticData = EarnFreebiesStaticData.Instance;
		Title.text = EarnStaticData.getOffersLifeSaverTitle (Order);
		Value.text = EarnStaticData.getOffersLifeSavers (Order).ToString () + " LifeSavers";
		Description.text = EarnStaticData.getOffersLifeSaversDesciption (Order).ToString ();
		Symbol.SetSprite (EarnStaticData.getOffersLifeSaverImage (Order));
		Front.color = EarnStaticData.getOffersLifeSaverColor (Order);
		OrderInList = Order;
		EarnbtnText.text = EarnStaticData.getOffersLifesaverEarnText (Order);
		if (EarnStaticData.getOfferEarned (EarnTypes.LifeSaver, Order) == 1) {
			OnEarned ();
		}
	}


}
