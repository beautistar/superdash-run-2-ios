﻿using UnityEngine;
using System.Collections.Generic;

public class InGameAlertsManager : MonoBehaviour {
	public static InGameAlertsManager Instance;
	public tk2dTextMesh Title;
	public tk2dTextMesh Description;
	public GameObject Tick;
	public List<string> titlelist = new List<string>();
	public List<string> descriptionlist = new List<string>();
	public List<bool> ticklist = new List<bool>();
	// Use this for initialization
	void Awake () {

		Instance = this;
	}

	void OnEnable() {
		MyGUIManager.GameStartedEvent += HandleGameStartedEvent;
		MyGUIManager.RestartGameEvent += HandleRestartGameEvent;
	}

	void HandleRestartGameEvent () {
		Init ();
	}

	void OnDisable()	{
		MyGUIManager.GameStartedEvent -= HandleGameStartedEvent;
		MyGUIManager.RestartGameEvent -= HandleRestartGameEvent;
	}

	 void HandleGameStartedEvent () {
		Init ();
	}

	public void Init(){
		this.transform.localPosition = new Vector3 (0, 9, -3);
	}

	public void ShowAlert(string title,string Description, bool ShowTick) {
		if (GetComponent<Animation>().isPlaying) {
			if(ShowTick)
				titlelist.Add ("Assignment Completed");
			else
				titlelist.Add (title);

			descriptionlist.Add (Description);
			ticklist.Add (ShowTick);
			return;
		}

		if (ShowTick)
			Title.text = "Assignment Completed";
				else
			Title.text = title;

		this.Description.text = Description;
		Tick.SetActive (ShowTick);
		GetComponent<Animation>().PlayQueued ("Alert");
	}

	void AlertBack() {
		GetComponent<Animation>().PlayQueued ("Alertback");
		if (titlelist.Count > 0) {
			ShowAlert (titlelist [0], descriptionlist [0], ticklist [0]);
			titlelist.RemoveAt (0);
			descriptionlist.RemoveAt (0);
			ticklist.RemoveAt (0);
		} else {
			titlelist.Clear ();
			descriptionlist.Clear ();
			ticklist.Clear ();

		}
	}
}
