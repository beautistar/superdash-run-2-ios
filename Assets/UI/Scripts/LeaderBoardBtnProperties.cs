﻿using UnityEngine;
using System.Collections;

public class LeaderBoardBtnProperties : MonoBehaviour {
	public static int SerialNo = 0;
	public GameObject ProfilePicture;
	public tk2dTextMesh Serial;
	public tk2dTextMesh Name;
	public tk2dTextMesh Score;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetData(int i){
		Serial.text = (i+1).ToString ();
		Name.text = FBIntegrate.Instance.ScoresNames[i].ToString ();
		if (Name.text.Length > 13) {
			Name.text = Name.text.Substring (0, 12) +"..";	
		}
		Score.text = FBIntegrate.Instance.ScoresScore[i].ToString ();
		ProfilePicture.GetComponent<Renderer>().material.mainTexture = FBIntegrate.Instance.ProfilePics [i];
	}
}
