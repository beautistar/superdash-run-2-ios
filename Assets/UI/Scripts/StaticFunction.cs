using UnityEngine;
using System.Collections.Generic;
using System;
public class StaticFunction : MonoBehaviour
{
	public static int SlideValue ;
		// Use this for initialization
	public static void CalledFunct(int OrderInList, bool Down, GameObject[] InstanceOfChilds)
	{
		for (int i=OrderInList+1; i<InstanceOfChilds.Length; i++) {
			if(Down) {
				StaticFunction.SlideDown (InstanceOfChilds[i]);
			}
			else {
				StaticFunction.SlideUp (InstanceOfChilds[i]);
			}
		}
	}


	public static void SlideDown(GameObject Go)
	{

		Go.transform.Translate (new Vector3(0,-SlideValue,0));
	}
	
	public static void SlideUp(GameObject Go)
	{
		Go.transform.Translate (new Vector3(0,SlideValue,0));
	}
	
}


