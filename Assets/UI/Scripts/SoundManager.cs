﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {
	public static SoundManager Instance;
	public AudioClip ThemeMusic;
	public AudioSource SoundAudioSrc;
	public AudioSource MusicAudioSrc;

	void OnEnable(){
		SettingStaticData.OnMusicOnOff += HandleOnMusicOnOff;
		SettingStaticData.OnSoundOnOff += HandleOnSoundOnOff;
		SettingStaticData.OnSoundValue += HandleOnSoundValue;
		SettingStaticData.OnMusicValue += HandleOnMusicValue;
	}

	void OnDisable(){
		SettingStaticData.OnMusicOnOff -= HandleOnMusicOnOff;
		SettingStaticData.OnSoundOnOff -= HandleOnSoundOnOff;
		SettingStaticData.OnSoundValue -= HandleOnSoundValue;
		SettingStaticData.OnMusicValue -= HandleOnMusicValue;
	}

	void HandleOnSoundOnOff (){
		SoundAudioSrc.mute = !SettingStaticData.Instance.getSoundOnOff();
	}

	void HandleOnSoundValue (){
		SoundAudioSrc.volume = SettingStaticData.Instance.getSoundValue ();
	}
	
	void HandleOnMusicOnOff (){
		MusicAudioSrc.mute = !SettingStaticData.Instance.getMusicOnOff();
	}

	void HandleOnMusicValue (){
		MusicAudioSrc.volume = SettingStaticData.Instance.getMusicValue ();
	}

	// Use this for initialization
	void Awake() {	
		Instance = this;
	}

	void Start () {  	// Setting volume of music from last saved.
		HandleOnMusicOnOff ();
		HandleOnMusicValue ();
		HandleOnSoundOnOff ();
		HandleOnSoundValue ();
	}

	public void PlayThemeMusic() {
		MusicAudioSrc.Play ();
	}

	public void EndThemeMusic() {
		MusicAudioSrc.Stop ();
	}

	void Update(){  // Called on each frame. Changes volume if player changes values by Setting Button on pause menu.
	}

	void Play(AudioClip clip)  // plays clip passed as parameter.
	{
		SoundAudioSrc.volume = SettingStaticData.Instance.getSoundValue ();
		SoundAudioSrc.PlayOneShot(clip, AudioListener.volume);
	}
}
