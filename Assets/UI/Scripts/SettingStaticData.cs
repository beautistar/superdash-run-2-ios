﻿using UnityEngine;
using System.Collections;
using System;
public enum SettingOrder { Tutorials, Notification, Music, Sound, MusicSlider, SoundSlider};
public class SettingStaticData : MonoBehaviour {

	public int ItemsCount;
	public string[] Title;
	public string[] Description;
	public static event Action OnSoundOnOff; 
	public static event Action OnMusicOnOff; 
	public static event Action OnSoundValue; 
	public static event Action OnMusicValue; 

	public static SettingStaticData Instance;
	
	void Awake()
	{
		if (Instance == null || Instance != this)
			Instance = this;
	}
	
	public int getItemsCount()
	{
		return ItemsCount;
	}
	
	public string getItemsTitle(int Order)
	{
		return Title [Order];
	}

	public string getItemsDescription(int Order)
	{
		return Description [Order];
	}

	public void setItems(int Order) {
		if(getItems (Order) == 0)
			PlayerPrefs.SetFloat (string.Format ("Setting{0}",(Order)),1);
		else
			PlayerPrefs.SetFloat (string.Format ("Setting{0}",(Order)),0);
	}

	public void setItems(int Order, float Value) {
		PlayerPrefs.SetFloat (string.Format ("Setting{0}",(Order)),Value);
	}
	
	public float getItems(int Order) {
		if(Order == 0)
			return PlayerPrefs.GetFloat(string.Format ("Setting{0}",(Order)),1);
		else
			return PlayerPrefs.GetFloat(string.Format ("Setting{0}",(Order)),1);	
	}

	public bool getFacebook(){
		// get Value From Social Manager
			return false;
	}

	public void setFacebook(){
		// Set Value Through Social manager
	}

	public bool getTwitter(){
		// get Value From Social Manager
			return true;
	}
	
	public void setTwitter(){
		// Set Value Through Social manager
	}


	public bool getTutorial(){
		return getItemBool (SettingOrder.Tutorials);
	}
	
	public void setTutorial(){
		setItems ((int )SettingOrder.Tutorials);
	}

	public bool getNotification(){
		return Pushup.Instance.GetPush ();
	}
	
	public void setNotification(){
		Pushup.Instance.SetPush ();
	}

	public bool getMusicOnOff(){
		return getItemBool (SettingOrder.Music);
	}
	
	public void setMusicOnOff(){
	
		setItems ((int )SettingOrder.Music);
		OnMusicOnOff ();
	}

	public bool getSoundOnOff(){
		return getItemBool (SettingOrder.Sound);
	}
	
	public void setSoundOnOff(){
		setItems ((int )SettingOrder.Sound);
		OnSoundOnOff ();
	}

	public float getMusicValue(){
		return getItems ((int)SettingOrder.MusicSlider);
	}
	
	public void setMusicValue(float Value){
		setItems ((int ) SettingOrder.MusicSlider,Value);
		OnMusicValue ();
	}

	public float getSoundValue(){
		return getItems ((int)SettingOrder.SoundSlider);
	}
	
	public void setSoundValue(float Value){
		setItems ((int )SettingOrder.SoundSlider,Value);
		OnSoundValue ();
	}
	
	bool getItemBool(SettingOrder settingOrder)
	{

		if((getItems((int)settingOrder) ) == 1)
			return true;
		else
			return false;
	}
	
	public void ClearAll() {
		for (int i=0; i<ItemsCount; i++) {
			PlayerPrefs.SetInt (string.Format ("Setting{0}",(i)),0);	
		}
	}

}
