﻿using UnityEngine;
using System.Collections;

public class Pushup : MonoBehaviour {
	public static Pushup Instance;
	public GameObject PushUP;

	void Awake() {
		Instance = this;
	}

	// Use this for initialization
	public void Init () {
		if (PlayerPrefs.GetInt ("PushStart", 0) == 0) {
			PopUpManager.Instance.IsPopOn = true;
			UIGameManager.Instance.ShowWall ("",true);
			PushUP.SetActive (true);
			PlayerPrefs.SetInt ("PushStart", 1);
		}
		else {
			PushUP.SetActive (false);
		}
	}

	public bool GetPush() {
		if (PlayerPrefs.GetInt ("Push", 0) == 1)
			return false;
		else
			return true;
	}

	public void OnOkClick(){
		SetPush ();
	}
	
	void OnDontAllow(){
		SetPush ();
	}
	
	public void SetPush() {
		if (PlayerPrefs.GetInt ("Push", 0) == 1)
			PlayerPrefs.SetInt ("Push", 0);
		else
			PlayerPrefs.SetInt ("Push",1);

		PopUpManager.Instance.IsPopOn = false;
		PushUP.SetActive (false); 
		UIGameManager.Instance.HideWall ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}


}
