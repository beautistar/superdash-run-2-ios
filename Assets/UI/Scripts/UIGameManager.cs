﻿using UnityEngine;
using System.Collections;

public class UIGameManager : MonoBehaviour {

	public GameObject Wall;
	public GameObject MenuWall;
	public tk2dTextMesh Text;
	public static UIGameManager Instance;
	public bool IsWall;
	public bool IsAndroid = false;
	public bool IsAmazon = false;
	public bool IsIOS = false;
	public string AndroidVersion = "1.5";
	public string AmazonVersion = "1.1";
	public string iOSVersion = "1.1";
	public bool IsTesting = false;
	public tk2dUIScrollableArea bankScrollArea ;
	public string PackageName = "com.zidapps.superdashrun";
	public string GameName = "Super Dash Run";
	float Timer;
	float StopTime = 10;
	float stoptime = 10;
	// Use this for initialization
	void Awake () {
		Instance = this;
	}

	void OnApplicationPause(bool pauseStatus) {
		if (pauseStatus && GameManager.instance.IsGameRunnnig) {
				//MyGUIManager.instance.PauseScreen ();
		} else if(GameManager.instance.IsGameRunnnig){
//			GameManager.instance.pauseGame (true);
		}
	}

	void Start() {
		stoptime = StopTime;
	}
	
	// Update is called once per frame
	void Update () {

		if (!IsWall) {
			Timer = 0;
			StopTime = stoptime;
			return;
		}

		if (IsWall && Timer > StopTime) {
			//Debug.Log ("Timer "+Timer);
			HideWall ();
			Timer = 0;
			StopTime = stoptime;
		}
		else if(IsWall) {

			Timer += Time.fixedDeltaTime;
		}
	}

	public void ShowWall(string Text) {
	
		IsWall = true;
		StopTime = stoptime;
		Wall.SetActive (true);
		//Debug.Log (IsWall);
		this.Text.text = Text;
	}

	public void ShowWall(string Text, float Timer) {
		IsWall = true;
		StopTime = Timer;
		Wall.SetActive (true);
		this.Text.text = Text;
	}

	public void ShowWall(string Text, bool ShowPermanent) {
		//IsWall = true;
		//StopTime = Timer;
		Wall.SetActive (true);
		this.Text.text = Text;
	}
	
	public void HideWall(){
		//Debug.Log ("HideWall");
		IsWall = false;
		Wall.SetActive (false);
		//Wall.transform.localPosition = new Vector3 (0, 0, -5);
	}

	public void ShowMenuWall() {
		MenuWall.SetActive (true);
		this.Text.text = "";
	}

	public void HideMenuWall(){
		MenuWall.SetActive (false);
	}

	public void PopUp(string Text){
		ShowWall (Text);
		StartCoroutine ("Delay");
	}

	IEnumerator Delay(){
		yield return new WaitForSeconds(1);
		HideWall ();
	}

	public void GoogleAnalyticsScreenName(string ScreenName){
//		if (GoogleAnalytics.instance)
//			GoogleAnalytics.instance.LogScreen(ScreenName);
	}

}
