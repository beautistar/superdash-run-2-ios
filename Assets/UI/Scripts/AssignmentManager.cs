﻿using UnityEngine;
using System.Collections;

public class AssignmentManager : MonoBehaviour {
	public GameObject Level;
	public int CurrentLevel;
	public static AssignmentManager Instance;
	private MissionManager missionManager;
	public AssignmentLevelManager assignmentlevel;
	bool FirstTime = false;
	// Use this for initialization
	void Awake()
	{
		if(Instance == null || Instance != this)
			Instance = this;

	}

	void Start () {
		SetAssignments ();
		FirstTime = true;
	}

	void OnEnable()
	{
		if(FirstTime)
			SetAssignments ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	 void SetAssignments() {
		missionManager = MissionManager.instance;
		CurrentLevel = missionManager.GetAssignmentLevel();
		assignmentlevel.SetAssignmentsBtn (CurrentLevel);
		assignmentlevel.gameObject.name = "Level " +(CurrentLevel.ToString () +1);
	}
}
