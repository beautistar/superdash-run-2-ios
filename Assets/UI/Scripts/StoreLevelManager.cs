﻿using UnityEngine;
using System.Collections;

public class StoreLevelManager : MonoBehaviour {
	public tk2dSprite[] Levels;
	public int SpriteNo = 17;
	string SpriteNoString = "llevel_active";
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetLevel(int Level)
	{

		if (Level > Levels.Length) {
			Level = Levels.Length;		
		}

		for (int i=0; i<Level; i++) {
			Levels[i].SetSprite ("llevel_active");
				
		}
	}
}
