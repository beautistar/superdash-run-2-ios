﻿using UnityEngine;
using System.Collections;
using System;

public class PlayVideoManager : MonoBehaviour {
	public static event Action VideoCompleted;
	public string VideoPathIOS ="Tutorial.mp4";
	public string VideoPathAndroid ="Tutorial.mp4";
	public static PlayVideoManager Instance;
	string path ="";
	void Awake()
	{
		Instance = this;
	}
	// Use this for initialization
	public void PlayVideo (){
		Debug.LogError("Playing video");
		StartCoroutine ("PlayMovie");
	}

	protected IEnumerator PlayMovie(){
		//Debug.Log (VideoPath);
		#if UNITY_ANDROID	
		path = VideoPathAndroid;
		#endif
		#if UNITY_IPHONE	
		path = VideoPathIOS;
		#endif
		if(System.IO.File.Exists(path))
			Debug.LogWarning("File exists");
		else
			Debug.LogWarning("File Not found");

		Handheld.PlayFullScreenMovie(path, Color.clear,  FullScreenMovieControlMode.Hidden, FullScreenMovieScalingMode.AspectFill);
		yield return new WaitForSeconds(0.1f);
		VideoCompleted();
	}	
}
