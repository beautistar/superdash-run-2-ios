﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class WeeklyChallengeStaticData : MonoBehaviour {
	public static WeeklyChallengeStaticData Instance;
	public GameObject WeeklyChallengeBtn;
	public int TotalBtns = 5;
	public int ToShowBtns = 3;
	public string[] SymbolName;
	public string[] CoinsSymbolName;
	public int[] Coins;
	List<int> CurrentWeeklyActiveChallList;
	// Use this for initialization
	void Awake () {
		Instance = this;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void Start() {
		CurrentWeeklyActiveChallList = GetCurrentActiveWeeklyChall ();
	}

	public string GetWeeklyChallSymbolName(int Order) {
		return SymbolName[Order];
	}
	public string GetCoinsSymbolName(bool Coin) {
		if (Coin)
			return CoinsSymbolName [0];
		else
			return CoinsSymbolName [1];
	}


	public int GetWeeklyChallCoins(int Order) {
		return Coins[Order];
	}
	public string GetWeeklyChallName(int Order) {
		return ChallengesTypesData.WeeklyChallengesTypesNames[CurrentWeeklyActiveChallList[Order]].Replace (":"," ");
	}
	public int GetWeeklyChallValue(int Order) {
		return ChallengesTypesData.WeeklyChallengesTypesValues [CurrentWeeklyActiveChallList[Order]];
	}
	

	public List<int> GetCurrentActiveWeeklyChall() {
		List<int> list = new List<int> ();
		for (int i =0; i<TotalBtns; i++) {
			int Val = PlayerPrefs.GetInt (string.Format ("CurrentActiveWeeklyChall{0}",i),0);
			if(Val == 1) {
				list.Add (i);
			}
		}
		return list;
	}
	public void SetCurrentActiveWeeklyChall(List<int> list) {
		CurrentWeeklyActiveChallList = list;
		for (int i =0; i<list.Count; i++) {
			PlayerPrefs.SetInt (string.Format ("CurrentActiveWeeklyChall{0}",list[i]),1);
		}
	}



	public bool GetWeeklyChallCompleted(int Order) {
		if (PlayerPrefs.GetInt (string.Format ("WeeklyChallCompleted{0}", CurrentWeeklyActiveChallList[Order]), 0) == 1)
			return true;
		return false;
	}
	public void SetWeeklyChallCompleted(int Order) {
		PlayerPrefs.SetInt (string.Format ("WeeklyChallCompleted{0}", CurrentWeeklyActiveChallList[Order]), 1);
	}




	public int GetCurrentActiveWeeklyChallSavedValue(int Order) {
		return PlayerPrefs.GetInt (string.Format ("CurrentActiveWeeklyChallSavedValue{0}",CurrentWeeklyActiveChallList[Order]),0);
	}
	public void SetCurrentActiveWeeklyChallSavedValue(int Order, int Value) {
		PlayerPrefs.SetInt (string.Format ("CurrentActiveWeeklyChallSavedValue{0}",CurrentWeeklyActiveChallList[Order]),Value);
	}


	public bool GetIsWeekCompleted() {
		DateTime t1 = DateTime.Parse (GetWeeklyChallengeLastDate());
		TimeSpan  t = System.DateTime.Today.Subtract (t1);
		if (t.Days > 6) {
			return true;
		}
		return false;
	}


	public string GetWeeklyChallengeLastDate() {
		return PlayerPrefs.GetString ("WeeklyChallengeLastDate",System.DateTime.Now.Date.ToString ());
	}
	public void SetWeeklyChallengeLastDate() {
		PlayerPrefs.SetString ("WeeklyChallengeLastDate",DateTime.Today.Date.ToString ());
	}



	public bool GetIsWeeklyChallDiamond(int Order) {
		if (Order == 2)
			return true;
		return false;
	}
	public bool GetIsWeeklyChallCoins(int Order) {
		if (Order < 2)
			return true;
		return false;
	}


	public bool GetWeekllyIsFirstTime() {
		//PlayerPrefs.SetInt ("WeekllyIsFirstTime", 0);
		if (PlayerPrefs.GetInt ("WeekllyIsFirstTime", 0) == 0)
			return true;
		return false;
	}

	public void SetWeekllyIsFirstTime() {
		PlayerPrefs.SetInt ("WeekllyIsFirstTime", 1);
	}

	public void ResetAllWeeklyChallenges() {
		for (int i =0; i<TotalBtns; i++) {
			PlayerPrefs.SetInt (string.Format ("CurrentActiveWeeklyChall{0}",i),0);
			PlayerPrefs.SetInt (string.Format ("WeeklyChallCompleted{0}", i), 0);
			PlayerPrefs.SetInt (string.Format ("CurrentActiveWeeklyChallSavedValue{0}",i),0);
		}
	}

}
