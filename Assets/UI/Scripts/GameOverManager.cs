using UnityEngine;
using Facebook.Unity;

public class GameOverManager : MonoBehaviour {
	public tk2dTextMesh Coin;
	public tk2dTextMesh Score;
	public tk2dTextMesh TwitterText;
	public tk2dTextMesh FacebookText;
	public GameObject HighscoreBox;
	private DataManager dataManager;
	public GameObject FBConnect;

	public GameObject MainPopUp;
	public tk2dTextMesh MainPopUpText;
	public GameObject TwitterMain;
	public GameObject FacebookMain;
	public GameObject TwitterPopup;
	public GameObject FacebookPopup;
	public bool IsGameover = false;
	bool IsFBCoinsUsed = false;
	bool PostScore = false;
	
	//public GameObject Assignment; 	
	// Use this for initialization

	// Update is called once per frame
	void OnEnable() {
		if(IsFBCoinsUsed)
			FBConnect.SetActive(false);
	}
	
	void OnDisable() {
	}

	private const int SHOW_EVERY_TIMES = 3;
	private static int _gameCount = 3;

	public void SetScoreCoin() {
		dataManager = DataManager.instance;
		if(Score != null)
		   Score.text = dataManager.getScore ().ToString ();
		if(Coin != null)
			Coin.text = dataManager.getLevelCoins().ToString();

		if (dataManager.IsLevelHighscore && IsGameover) {
			HighscoreBox.SetActive (true);
			if(FB.IsLoggedIn) {
				PostScore = true;
				PostFacebookScore();
				SortFacebookScore();
			}
		} else {
			HighscoreBox.SetActive (false);
		}

		if (!IsGameover) {
			return;	
		}

		FacebookPopup.SetActive (false);
		FacebookMain.SetActive (false);
		TwitterPopup.SetActive (false);
		TwitterMain.SetActive (false);


		++_gameCount;
		if (_gameCount >= SHOW_EVERY_TIMES) {
			_gameCount = 0;
			MainPopUp.SetActive (true);
		} else {
			FbReminder.Instance.Show();
		}


		#if UNITY_ANDROID
		MainPopUpText.text = "Share Your Score and get FREE COINS NOW";
		#endif
		#if UNITY_IPHONE
		MainPopUpText.text = "Share / Brag Your Score";
		#endif

		ChallengerManager.Instance.CheckForDailyChallengeComplete ();
		ChallengerManager.Instance.CheckForWeeklyChallengeComplete ();
	}
	
	void MainPopupNotNow() {
		MainPopUp.SetActive (false);
	}

	public void TwitterPopupCancel() {
		TwitterPopup.SetActive (false);
		TwitterMain.SetActive (false);
		UIGameManager.Instance.HideWall ();
	}
	
	public void TwitterPopupOk() {
		MainPopUp.SetActive (false);
		TwitterPopup.SetActive (false);
		TwitterMain.SetActive (true);
		TwitterText.text = "I got " + dataManager.getScore ().ToString () + " points while dashing from the parents! Nobody can outrun me in @SuperDashRun";
	}

	public void TwitterMainCancel() {
		TwitterMain.SetActive (false);
		MainPopUp.SetActive (false);
		UIGameManager.Instance.HideWall ();
	}

	public void TwitterMainTweet() {
		if (!TwitterManage.IsLogined) {
			UIGameManager.Instance.ShowWall ("Connecting Twitter...", 15);
			TwitterManage.Instance.LoginWithCompose (TwitterText.text);
		} else {
//			UIGameManager.Instance.ShowWall ("Posting score...", 15);
			TwitterMain.SetActive (false);
			MainPopUp.SetActive (false);
			TwitterManage.Instance.PostScore (TwitterText.text);
		}
	}

	public void FacebookPopupCancel() {
		FacebookPopup.SetActive (false);
		FacebookMain.SetActive (false);
		UIGameManager.Instance.HideWall ();
	}
	
	public void FacebookPopupOk() {	
		MainPopUp.SetActive (false);
		FacebookPopup.SetActive (false);
		FacebookMain.SetActive (true);
		FacebookText.text = " Nobody can top my score of " + dataManager.getScore ().ToString () + " ! If you think you can, show me what you got! I will be waiting!";
	}

	public void FacebookMainCancel() {
		FacebookMain.SetActive (false);
		MainPopUp.SetActive (false);
		UIGameManager.Instance.HideWall ();
	}
	
	public void FacebookMainPost() {	
		PostScore = false;
		ShareScore ();
	}

	void PostFacebookScore() {
		if (MyGUIManager.instance.PlayCount <= 3) {
			ShareScore();
			PostScore = true;
		}
	}

	void ShareScore() {
		if (!FB.IsLoggedIn) {
			UIGameManager.Instance.ShowWall ("Connecting...",20);
			FBIntegrate.Instance.Login ();	
		} 
		else {
			HandleOnFBLoggedIn();
		}
	}

	void SortFacebookScore() {
		FBIntegrate.Instance.ScoresScore [FBIntegrate.Instance.Myno] = dataManager.getHighScore ();
		for (int i=0; i<FBIntegrate.Instance.ScoresScore.Count - 1; i++) {
			for(int j=0;j<FBIntegrate.Instance.ScoresScore.Count - 1 - i;j++) {
				
				if(System.Int32.Parse (FBIntegrate.Instance.ScoresScore [j].ToString()) < System.Int32.Parse( FBIntegrate.Instance.ScoresScore [j+1].ToString ())){
					object temp = FBIntegrate.Instance.ScoresScore [j];
					FBIntegrate.Instance.ScoresScore [j] = FBIntegrate.Instance.ScoresScore [j+1];
					FBIntegrate.Instance.ScoresScore [j+1] = temp;
					
					if(FBIntegrate.Instance.ProfilePics.Count > j+1 && FBIntegrate.Instance.ProfilePics [j] != null ) {
						Texture temp2 = FBIntegrate.Instance.ProfilePics [j];
						FBIntegrate.Instance.ProfilePics [j] = FBIntegrate.Instance.ProfilePics [j+1];
						FBIntegrate.Instance.ProfilePics [j+1] = temp2;
					}
				}
			}
		}
		
		/*for (c = 0 ; c < ( n - 1 ); c++)
		{
			for (d = 0 ; d < n - c - 1; d++)
			{
				if (array[d] > array[d+1]) /* For decreasing order use < 
				{
					swap       = array[d];
					array[d]   = array[d+1];
					array[d+1] = swap;
				}
			}
		}*/
	}
	
//	void HandletwitterLoginFailed (string error){
//		Debug.Log (error);
//		TwitterMainCancel ();
//	}
//	

//	
//	void HandletwitterPostFailed (string error) {
//		Debug.Log (error);
//		TwitterMainCancel ();
//	}
//	
//	void HandletwitterPost () {
//		TwitterPopup.SetActive (false);
//		TwitterMainCancel ();
//
//		UpdateEarnedCoins ();
//
//		UIGameManager.Instance.PopUp ("Successfully Done..");
//	}

	void HandleOnFBLoggedIn () {
		string LinkCaption = "I got " + dataManager.getScore ().ToString () + " points while out running the parents";
		string LinkDescription = "Let's now see what you get on Super Dash Run!";

		if (PostScore) {
			if(MyGUIManager.instance.PlayCount <= 3)
				UIGameManager.Instance.ShowWall ("Updating Leaderboard...",15);
			FBIntegrate.Instance.PostAppScore (dataManager.getScore ().ToString ());
			PostScore = false;
		} else {
			UIGameManager.Instance.ShowWall ("Connecting...",20);
			FBIntegrate.Instance.PostStatusOntimeline (LinkCaption,LinkDescription);
		}
	}
	
	void HandleOnScorePostFailedCall (string obj) {
		if (MyGUIManager.instance.PlayCount <= 3) {
			UIGameManager.Instance.HideWall ();
			UIGameManager.Instance.PopUp ("Could Not Connect \n Try Again");
		}
	}
	
	void HandleOnScorePostCall () {
		if (MyGUIManager.instance.PlayCount <= 3) {
			UIGameManager.Instance.HideWall ();
		}
		UIGameManager.Instance.HideWall ();
	}

	void HandleOnFBLoginFailed (string error) {
		print ("HandleOnFBLoginFailed "+error);
		if (MyGUIManager.instance.PlayCount <= 3) {
			UIGameManager.Instance.HideWall ();
			UIGameManager.Instance.PopUp ("Could Not Connect \n Try Again");
		}
		UIGameManager.Instance.HideWall ();
	}

	void HandleOnPostStatusFailedCall (){
		print ("HandleOnPostStatusFailedCall ");
		FacebookMainCancel ();
		UIGameManager.Instance.HideWall ();
		UIGameManager.Instance.PopUp ("Could Not Connect \n Try Again");
	}
	
	void HandleOnPostStatusCall (){
		if (IsGameover) {
			FacebookMain.SetActive (false);	
			MainPopupNotNow();
		}

		FBConnect.SetActive (false);
		IsFBCoinsUsed = true;

		UpdateEarnedCoins ();
	
		UIGameManager.Instance.PopUp ("Successfully Done..");
	}

	void UpdateEarnedCoins()
	{
		#if UNITY_ANDROID
		dataManager.adjustTotalCoins (100);
		dataManager.setTotalCoinsLifetime (100);
		Updatemanager.instance.OnUpdateEarn ();
		#endif
	}


}
