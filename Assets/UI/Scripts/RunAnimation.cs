﻿using UnityEngine;
using System.Collections;

public class RunAnimation : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (this.gameObject.GetComponent<Renderer>().isVisible) {
								if (!GetComponent<Animation>().isPlaying)
										GetComponent<Animation>().Play ();
				}
	}

	void OnBecameVisible() {
		GetComponent<Animation>().Play ();
	}
}
