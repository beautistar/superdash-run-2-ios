﻿using UnityEngine;
using System.Collections;

public class IngamePowerProgress : MonoBehaviour {
	public static IngamePowerProgress Instance;
	public GameObject PowerUpGameobject;
	public GameObject PowerUpGameobject2;
	bool IsCurrentPowerActive1 = false;	
	bool IsCurrentPowerActive2 = false;
	int CurrentOrder = 0;
	public tk2dSprite Symbol;
	public tk2dSprite Symbol2;
	public GameObject[] Icons;
	public GameObject[] Icons2;
	public float GapIcons;
	float Gapx;
	// Use this for initialization
	void Awake () {
		Instance = this;
	}

	void OnEnable(){
		MyGUIManager.GameStartedEvent += HandleGameStartedEvent;
		MyGUIManager.GameEndEvent += HandleGameEndEvent; 
		MyGUIManager.RestartGameEvent += HandleRestartGameEvent;
	}

	void OnDisable(){
		MyGUIManager.GameStartedEvent -= HandleGameStartedEvent;
		MyGUIManager.GameEndEvent -= HandleGameEndEvent; 
		MyGUIManager.RestartGameEvent -= HandleRestartGameEvent;
	}

	public void StartLoader(float Timer, int Order) {
		Gapx = 0;
		InGameUIManager.Instance.RemovePowerUps ();
		float TimerPerIcon = Timer / 12;
		//Debug.Log (TimerPerIcon);
		if (!IsCurrentPowerActive1) {
			CallLoader1(TimerPerIcon,Order);
			//Debug.Log ("1");
		} else { 
			if(Order == CurrentOrder) {
				StopCoroutine ("Loader");
				CallLoader1(TimerPerIcon,Order);
				//Debug.Log ("2");
			}
			else {
				CallLoader2(TimerPerIcon,Order);
				//Debug.Log ("3");
			}
		}

		CurrentOrder = Order;
	}


	void CallLoader1(float Timer, int Order){
		IsCurrentPowerActive1 = true;
		PowerUpGameobject.SetActive (true);
		ArrangeIcons (Icons);
		StartCoroutine ("Loader", Timer);
		if (Order < 4) {
			Symbol.SetSprite (StoreStaticData.Instance.getUpgradeCoinsImage (Order));	
		} else {
			Symbol.SetSprite (StoreStaticData.Instance.getSingleUseCoinsImage (Order - 4));	
		}
	}

	void CallLoader2(float Timer, int Order) {
		IsCurrentPowerActive2 = true;
		PowerUpGameobject2.SetActive(true);
		ArrangeIcons (Icons2);
		StartCoroutine ("Loader2", Timer);
		if (Order < 4) {
			Symbol2.SetSprite (StoreStaticData.Instance.getUpgradeCoinsImage (Order));	
		} else {
			Symbol2.SetSprite (StoreStaticData.Instance.getSingleUseCoinsImage (Order - 4));	
		}
	}

	public IEnumerator Loader(float Timer)	{
		for (int i=0; i < Icons.Length; i++) {
			yield return new WaitForSeconds(Timer);
			Icons[i].gameObject.SetActive (false);
		}

		PowerUpGameobject.SetActive (false);
		IsCurrentPowerActive1 = false;
	}

	public IEnumerator Loader2(float Timer)	{
		for (int i=0; i < Icons2.Length; i++) {
			yield return new WaitForSeconds(Timer);
			Icons2[i].gameObject.SetActive (false);
		}
		
		PowerUpGameobject2.SetActive (false);
		IsCurrentPowerActive2 = false;
	}

	int Lines ;
	int Line2 ;
	
	public void ResumeLoader1(float RemTime, float TotalTime) {
		//Debug.Log ("ResumeLoader1");
		float TimerPerIcon = TotalTime / 12;
		Lines =  (int)(RemTime / TimerPerIcon) + 1;
		//Debug.Log ("RemTime "+RemTime);
		//Debug.Log ("TotalTime "+TotalTime);
		//Debug.Log ("TimerPerIcon "+TimerPerIcon);
		//Debug.Log ("Lines "+Lines);
		for (int i=0; i<Icons.Length - Lines; i++) {
			if(Icons.Length  > i )
				Icons[i].gameObject.SetActive (false);	
		}
		//Debug.Log ("ResumeLoader11");
		StartCoroutine ("Resumer",TimerPerIcon);
	}
	
	 IEnumerator Resumer(float Timer)	{
		for (int i=Icons.Length - Lines; i < Icons.Length; i++) {
			yield return new WaitForSeconds(Timer);
			Icons[i].gameObject.SetActive (false);
		}
		PowerUpGameobject.SetActive (false);
		IsCurrentPowerActive1 = false;
	}
	
	public void ResumeLoader2(float RemTime, float TotalTime) {
		float TimerPerIcon = TotalTime / 12;
		Line2 =  (int)(RemTime / TimerPerIcon) + 1;
		for (int i=0; i<Icons2.Length - Line2; i++) {
			Icons2[i].gameObject.SetActive (false);	
		}

		StartCoroutine ("Resumer2",TimerPerIcon);
	}

	 IEnumerator Resumer2(float Timer)	{
		for (int i=Icons2.Length - Line2; i < Icons2.Length; i++) {
			yield return new WaitForSeconds(Timer);
			Icons2[i].gameObject.SetActive (false);
		}
		PowerUpGameobject2.SetActive (false);
		IsCurrentPowerActive2 = false;
	}
	
	void ArrangeIcons(GameObject[] Icons) {
		for (int i=Icons.Length-1; i>=0; i--) {
			Icons[i].SetActive (true);
			Icons[i].transform.localPosition  =  new Vector3(Gapx,0,0);
			Gapx += GapIcons;
		}
	}

	void HandleRestartGameEvent (){
		HandleGameStartedEvent ();
	}
	
	void HandleGameEndEvent (){
		
	}

//	public void StopAllCoroutines()	{
//		HandleGameStartedEvent ();
//	}
	
	void HandleGameStartedEvent (){
		StopCoroutine ("Loader2");
		StopCoroutine ("Loader");
		StopCoroutine ("Resumer");
		StopCoroutine ("Resumer2");
		PowerUpGameobject.SetActive (false);
		PowerUpGameobject2.SetActive (false);
		IsCurrentPowerActive1 = false;
		IsCurrentPowerActive2 = false;
		Gapx = 0;
	}
	
}
