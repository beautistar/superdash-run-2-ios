﻿using UnityEngine;
using System.Collections;

public class CornerBtnManager : MonoBehaviour {
	public tk2dSprite ButtonGraphic;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ChangeSprite(bool Down)
	{
		if (Down) {
			ButtonGraphic.SetSprite (88);
		} 
		else {
			ButtonGraphic.SetSprite (61);	
		}
	}
}
