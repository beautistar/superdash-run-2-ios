﻿using UnityEngine;
using System.Collections;

public class InGamePowerUp : MonoBehaviour {
	public tk2dSprite Symbol;
	public tk2dTextMesh Text;
	public int Order  = 0;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnClickPower(){
		InGameUIManager.Instance.OnClickPowerUps (this.Order);
	}

	public void SetDetails(int Order,string SpriteName) {
		Text.text = DataManager.instance.getPowerUpLevel ((PowerUpTypes)Order).ToString ();
		this.Order = Order;
		Symbol.SetSprite (SpriteName);
	}
}
