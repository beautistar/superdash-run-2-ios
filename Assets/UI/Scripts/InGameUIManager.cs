using UnityEngine;
using System.Collections;
using Facebook.Unity;

public class InGameUIManager : MonoBehaviour {
	public static InGameUIManager Instance;
	public tk2dTextMesh Score;
	public tk2dTextMesh Coin;
	public tk2dTextMesh Multiplier;
	public GameObject PowerUpPreFab;
	public GameObject ScooterCruiserText; 

	public GameObject Highscore ;
	public tk2dTextMesh PlayerHighScore;
	public GameObject PlayerPicture;
	public GameObject Border;
	public tk2dSprite PlayerSprite;
	public InGameAlertsManager AlertPopup;
	public bool DoesPassScore = false;
	InGamePowerUp[] PowerUps;
	private GameManager gameManager;
	private DataManager dataManager;
	int SingleUse = 0 ;
	public float InitialY = -6;
	public float PowerUpTimer = 6;
	float timer = 0;
	bool Cleared = false;
	int CurrentFBId = 0;
	bool GameStarted = false;
	bool StopCountingHighScore = false;
	Vector3 highscorePos ;
	void Awake() {
		Instance = this;
	}
	// Use this for initialization
	void OnEnable () {
		GameStarted = false;
	}

	public void StartGame(){
		DoesPassScore = false;
		dataManager = DataManager.instance;
		gameManager = GameManager.instance;
		if(dataManager.getHighScore () > -1)
			StopCountingHighScore = false;
		else
			StopCountingHighScore = true;
	
		this.Score.text = "0";
		Coin.text = "0";
		SetMultiplier ();
		Highscore.SetActive (true);
		SetMultiplier ();
		SingleUse = DataManager.instance.SingleUse;
		PlayerHighScore.text = dataManager.getHighScore ().ToString ();
		RemovePowerUps ();
		SetPowerUps ();

		if (DataManager.instance.getPowerUpLevel (PowerUpTypes.Invincibility ) > 0)
			ScooterCruiserText.SetActive (true);
		else
			ScooterCruiserText.SetActive (false);

		if (FB.IsLoggedIn) {
			if(FBIntegrate.Instance.scores.Count > 0)
				CurrentFBId = FBIntegrate.Instance.scores.Count - 1;
			ClearedCurrenrScore = true;
			PlayerPicture.SetActive (true);
			PlayerSprite.gameObject.SetActive (false);
//			FBIntegrate.Instance.getProfileImage ();
//			PlayerPicture.GetComponent<Renderer>().material.mainTexture = FBIntegrate.Instance.ProfilePics [CurrentFBId];
			Border.SetActive (true);
		}
		else {
			PlayerPicture.SetActive (false);
			PlayerSprite.gameObject.SetActive (true);
			Border.SetActive (false);
			PlayerSprite.SetSprite (StoreStaticData.Instance.getCharacterProfileImage (DataManager.instance.getSelectedCharacter ()));
			StopCountingNormal = false;

		}
		ToCompareScore = 0;
		timer = 0;
		Cleared = false;
		GameStarted = true;
	}

	public void ResumeGame(){
		GameStarted = true;
	}

	public void StartCoutingScore(){
		GameStarted = true;
	}
	
	// Update is called once per frame
	void Update () {

		try{
		if (!GameStarted)
			return;

		SetMultiplier ();

		if (FB.IsLoggedIn) {
			OnFacebook();	
		} else {
			OnNormal();
		}

		if (System.Int32.Parse (this.Score.text) > DataManager.instance.getHighScore () && !StopCountingHighScore) {
			InGameAlertsManager.Instance.ShowAlert ("CONGRATULATION !!","NEW HIGHSCORE : " +dataManager.getScore ().ToString (),false);	
			StopCountingHighScore = true;
		}
			
		if (Cleared)
			return;

		if (timer >= PowerUpTimer) {
			RemovePowerUps();	
			Cleared = true;
			timer = 0;
		}
		else {
			timer += Time.deltaTime;	
		}
		
		}
		catch(UnityException e) {
			Debug.Log (e.ToString ());

		}
	}

	bool ClearedCurrenrScore = false;
	int ToCompareScore = 0;

	void OnFacebook() {
		int CurrentScore = System.Int32.Parse (this.Score.text);
		if (CurrentFBId == -1 || FBIntegrate.Instance.ScoresScore.Count == 0 || FBIntegrate.Instance.ProfilePics.Count == 0 ) {
			Highscore.SetActive (false);
			return;
		}

		if (FBIntegrate.Instance.ScoresScore.Count >= CurrentFBId && ClearedCurrenrScore) {
				ToCompareScore = System.Int32.Parse (FBIntegrate.Instance.ScoresScore [CurrentFBId].ToString ());
					if (FBIntegrate.Instance.ProfilePics.Count < CurrentFBId || (FBIntegrate.Instance.ProfilePics.Count >= CurrentFBId && FBIntegrate.Instance.ProfilePics [CurrentFBId] == null)) {
						PlayerPicture.SetActive (false);
						PlayerSprite.gameObject.SetActive (true);
						Border.SetActive (false);
						PlayerSprite.SetSprite ("defaulticon");
					} else {
						PlayerPicture.SetActive (true);
						PlayerPicture.GetComponent<Renderer>().material.mainTexture = FBIntegrate.Instance.ProfilePics [CurrentFBId];
					}
				ClearedCurrenrScore = false;

		} else { if (CurrentScore > ToCompareScore) {
					CurrentFBId --;
					ClearedCurrenrScore = true;
					DoesPassScore = true;
				} else {
					PlayerHighScore.text = (ToCompareScore - CurrentScore).ToString ();
				}
			}
	}


	bool StopCountingNormal = false;

	void OnNormal() {
		if (StopCountingNormal)
			return;

		int CurrentScore = System.Int32.Parse (this.Score.text);
		if (CurrentScore > dataManager.getHighScore ()) {
			Highscore.SetActive (false);
			StopCountingNormal = true;
		} else {
			PlayerHighScore.text = ( dataManager.getHighScore () - CurrentScore).ToString ();
			FaceBookFriend(PlayerHighScore.text);
			}
	}

	void FaceBookFriend(string scoreDiff)
	{
		if (System.Int32.Parse (scoreDiff) < 10) 
		{
			PlayerController.instance.FacebookFriendReSetter (false);
		}
		else if (System.Int32.Parse (scoreDiff) < 100) 
		{
			PlayerController.instance.FacebookFriendSetter (true);
		}

	}

	public void SetScore(int Score) {
		this.Score.text = Score.ToString () ;
	}

	public void SetCoin(int Coin) {
		this.Coin.text = Coin.ToString ();
	}

	public void OnClickPowerUps(int Order){
		PowerUpTypes p = (PowerUpTypes) Order;
		switch (Order) {
		case 4 : ActivatePowerUp(p); GameManager.instance.decreaseSingleUseOnly(p); Destroy(PowerUps[Order - SingleUse].gameObject); break;
		case 5 : ActivatePowerUp(p); GameManager.instance.decreaseSingleUseOnly(p); Destroy(PowerUps[Order - SingleUse].gameObject);break;
		case 6 : ActivatePowerUp(p); GameManager.instance.decreaseSingleUseOnly(p); Destroy(PowerUps[Order - SingleUse].gameObject);break;
		case 7 : Destroy(PowerUps[Order - SingleUse].gameObject);break;
		}
	}

	public void SetMultiplier() {
		if (dataManager.levelSixMultiplier == 1) {
			Multiplier.text = (DataManager.instance.getMultiplier () * 6).ToString ();
		}
		else
			Multiplier.text = DataManager.instance.getMultiplier ().ToString () ;
	}

	void SetPowerUps(){
	
		PowerUps = new InGamePowerUp[SingleUse];
		string SpriteName = "";
		for (int i=1; i<SingleUse-1; i++) {
			//Debug.Log (DataManager.instance.getPowerUpLevel ((PowerUpTypes) i+ SingleUse));
			if(DataManager.instance.getPowerUpLevel ((PowerUpTypes) i+ SingleUse ) > 0)  {
				GameObject Go = (GameObject) Instantiate (PowerUpPreFab);
				PowerUps[i] = Go.GetComponent<InGamePowerUp>();
				PowerUps[i].transform.parent = this.gameObject.transform;
				PowerUps[i].transform.localPosition = new Vector3(-Resolutions.GetWidth ()/2+1.5f,InitialY,0);
			
				switch(i)
				{
				case 0: SpriteName = "6xpower"; break;
				case 1: SpriteName = "6xpower"; break;
				case 2: SpriteName = "boost"; break;
				case 3: SpriteName = "bolt"; break;
				}
				PowerUps[i].SetDetails (i+SingleUse,SpriteName );
				InitialY += 1.5f;
			}
		}
		InitialY = -6;
	}

	public void RemovePowerUps() {
		ScooterCruiserText.SetActive (false);
		if (PowerUps == null)
						return; 
		for (int i=0; i<PowerUps.Length; i++) {
			if(PowerUps[i] != null)
				Destroy (PowerUps[i].gameObject);	
		}
	}

	void ActivatePowerUp(PowerUpTypes powerUpTypes){
		if(powerUpTypes == PowerUpTypes.SixMultiplier)
			PlayerController.instance.PlaySixMultiplierParticleSystem();
		else	
			gameManager.activatePowerUp(powerUpTypes, true);
		dataManager = DataManager.instance;
		dataManager.SetLevelPowerUpCount(powerUpTypes);
		//deactivate();
	}


}
