﻿using UnityEngine;
using System.Collections;

namespace ognsdk{
	public class OGN {
#if UNITY_ANDROID
		private static AndroidJavaClass playerClass = new AndroidJavaClass (Constant.REFLECTION_UNITY_BASE);
		private static AndroidJavaObject activityContext =  playerClass.GetStatic<AndroidJavaObject> (Constant.METHOD_UNITY_CURRENTACTIVITY);

		private AndroidJavaClass ogn= new AndroidJavaClass(Constant.REFLECTION_OGNSDK_OGN);
		private AndroidJavaObject ognInstance;
#endif
		private static OGN ognUnity;

		private OGN(string appKey, string appSecret){
#if UNITY_ANDROID
			ognInstance = ogn.CallStatic<AndroidJavaObject>(Constant.METHOD_ANDROID_GETINSTANCE,activityContext, appKey, appSecret);
#endif
		}

		public static OGN initOGN(string appKey, string appSecret){

			if (ognUnity == null) {
				ognUnity = new OGN (appKey, appSecret);
			}
			 
			return ognUnity;

		}


		
		public void postScore(Score score, int level, bool levelCleared){

#if UNITY_ANDROID
			AndroidJavaObject longScore = new AndroidJavaObject (Constant.REFLECTION_JAVA_LONG, score.getScore());
			AndroidJavaObject intLevel = new AndroidJavaObject (Constant.REFLECTION_JAVA_INTEGER, level);
			AndroidJavaObject booleanLevelCleared = new AndroidJavaObject (Constant.REFLECTION_JAVA_BOOLEAN, levelCleared);

			AndroidJavaObject scoreObj = new AndroidJavaObject(Constant.REFLECTION_OGNSDK_SCORE, longScore, score.getLabel());
			ognInstance.Call (Constant.METHOD_OGNSDK_OGN_POSTSCORE, scoreObj, intLevel, booleanLevelCleared);
#endif
		}

		public void postScore(Score score){
#if UNITY_ANDROID
			AndroidJavaObject longScore = new AndroidJavaObject (Constant.REFLECTION_JAVA_LONG, score.getScore());

			AndroidJavaObject scoreObj = new AndroidJavaObject(Constant.REFLECTION_OGNSDK_SCORE, longScore, score.getLabel());
			ognInstance.Call (Constant.METHOD_OGNSDK_OGN_POSTSCORE, scoreObj);
#endif
		}
		
		public void sendEvent(Event gameEvent){
#if UNITY_ANDROID
			AndroidJavaClass csUtils= new AndroidJavaClass(Constant.REFLECTION_OGNSDK_CSUTILS);
			csUtils.CallStatic (Constant.METHOD_OGNSDK_SENDEVENT, ognInstance, gameEvent.ToString ());
#endif
		}
	}
}
