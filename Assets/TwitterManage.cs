﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Runtime.InteropServices;
using TwitterKit.Unity;

public class TwitterManage : MonoBehaviour {

	public  static TwitterManage Instance;

	public static bool IsLogined = false;

	TwitterSession Mysession;

	string stScore;

	void Awake() {

		if(Instance == null || Instance != this)
			Instance = this;
	}

	void Start()
	{
		Twitter.Init ();

		if (!PlayerPrefs.HasKey ("IsLogined")) {
			PlayerPrefs.SetInt ("IsLogined", 0);
			IsLogined = false;
		}

//		if (PlayerPrefs.GetInt ("IsLogined") == 0) {
//			IsLogined = false;
//		} else {
//			IsLogined = true;
//		}
	}

	public void Login()
	{
		if (!IsLogined) {
			Twitter.LogIn (LoginComplete, (ApiError error) => {
//				UM_ExampleStatusBar.text = error.message;
			});
		}
	}

	public void LoginWithCompose(string stMSG)
	{
		stScore = stMSG;
		if (!IsLogined) {
			Twitter.LogIn (LoginCompleteWithCompose, (ApiError error) => {
//				UM_ExampleStatusBar.text = error.message;
			});
		}
	}

	public void Logout()
	{
		if (IsLogined) {
			
			IsLogined = false;

			PlayerPrefs.SetInt ("IsLogined", 0);

			Twitter.LogOut ();
		}
	}

	public void LoginComplete(TwitterSession session) 
	{
		Mysession = session;				

		IsLogined = true;

		PlayerPrefs.SetInt ("IsLogined", 1);

		if(PlayerPrefs.GetInt ("FirstTimeTwitter",0) == 0) {

			//EarnFreebiesStaticData.Instance.setOfferEarned (EarnTypes.Coins, 7);

//			LoginCoins.SetActive (false);
//			LoginEarn.SetActive (false);
//			LoginTick.SetActive (true);

			DataManager.instance.adjustTotalCoins (EarnFreebiesStaticData.Instance.getOffersCoins (7));
			DataManager.instance.setTotalCoinsLifetime (EarnFreebiesStaticData.Instance.getOffersCoins (7));
			PlayerPrefs.SetInt ("FirstTimeTwitter", 1);
			//Debug.Log ("2");
			Updatemanager.instance.OnUpdateCoins();
			//Debug.Log ("3");
		}
	}

	public void LoginCompleteWithCompose(TwitterSession session) 
	{
		Mysession = session;				

		IsLogined = true;

		PlayerPrefs.SetInt ("IsLogined", 1);

		if(PlayerPrefs.GetInt ("FirstTimeTwitter",0) == 0) {

			//EarnFreebiesStaticData.Instance.setOfferEarned (EarnTypes.Coins, 7);

//			LoginCoins.SetActive (false);
//			LoginEarn.SetActive (false);
//			LoginTick.SetActive (true);

			DataManager.instance.adjustTotalCoins (EarnFreebiesStaticData.Instance.getOffersCoins (7));
			DataManager.instance.setTotalCoinsLifetime (EarnFreebiesStaticData.Instance.getOffersCoins (7));
			PlayerPrefs.SetInt ("FirstTimeTwitter", 1);
			//Debug.Log ("2");
			Updatemanager.instance.OnUpdateCoins();
			//Debug.Log ("3");
		}

		Application.CaptureScreenshot("Screenshot.png");
		UnityEngine.Debug.Log ("Screenshot location=" + Application.persistentDataPath + "/Screenshot.png");
		string imageUri = "file://" + Application.persistentDataPath + "/Screenshot.png";
		Twitter.Compose (session, imageUri, stScore, null,
			(string tweetId) => { UnityEngine.Debug.Log ("Tweet Success, tweetId=" + tweetId); },
			(ApiError error) => { UnityEngine.Debug.Log ("Tweet Failed " + error.message); },
			() => { Debug.Log ("Compose cancelled"); }
		);
	}

	public void PostScore(string stMSG)
	{
		if (IsLogined) {
			Application.CaptureScreenshot ("Screenshot.png");
			UnityEngine.Debug.Log ("Screenshot location=" + Application.persistentDataPath + "/Screenshot.png");
			string imageUri = "file://" + Application.persistentDataPath + "/Screenshot.png";
			Twitter.Compose (Mysession, imageUri, stMSG, null,
				(string tweetId) => {
//					UM_ExampleStatusBar.text = "Tweet Success, tweetId=" + tweetId;
				},
				(ApiError error) => {
//					UM_ExampleStatusBar.text = "Tweet Failed " + error.message;
				},
				() => {
//					UM_ExampleStatusBar.text = "Compose cancelled";
				}
			);

			//EarnFreebiesStaticData.Instance.setOfferEarned (EarnTypes.Coins, 2);

//			PostCoins.SetActive (false);
//			PostEarn.SetActive (false);
//			PostTick.SetActive (true);

			DataManager.instance.adjustTotalCoins (EarnFreebiesStaticData.Instance.getOffersCoins (2));
			DataManager.instance.setTotalCoinsLifetime (EarnFreebiesStaticData.Instance.getOffersCoins (2));

		} else {
			LoginWithCompose (stMSG);
		}
	}

	public void PostScoreFromMain()
	{
		PostScore("I got " + DataManager.instance.getScore ().ToString () + " points while dashing from the parents! Nobody can outrun me in @SuperDashRun");
	}
}
