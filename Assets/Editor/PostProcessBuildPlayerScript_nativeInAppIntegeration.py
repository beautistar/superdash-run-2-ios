# @author mayank
# PostProcessScript (Integerate Native iOS code with unity generated xcode)
# Created : June 2013
# Project : PocketFightsApp

import os
import shutil
import file_helper
from sys import argv
from mod_pbxproj import XcodeProject


pathXcodeProject = argv[1]
pathXcodeProject1 = argv[0]
pathToIntegerationFolder = os.path.abspath(os.path.join(pathXcodeProject, ".."))
target = argv[2]
# filesToAddPath = argv[2]

print("PostProcessBuildPlayerScript Started")
print("XCode Path : " + pathXcodeProject)
print "Project Integeration path : %s" %pathToIntegerationFolder
print "Project Integeration path1 : %s" %pathXcodeProject1

project = XcodeProject.Load(pathXcodeProject + '/Unity-iPhone.xcodeproj/project.pbxproj')

########### Updating Plist ###########

def updatePlist():
	# content to add in plist
	addToPlist = '''
			<key>UIMainStoryboardFile</key>
			<string>MainStoryboard</string>
			<key>FacebookAppID - 2</key>
			<string>464703810278492</string>
			<key>FacebookAppID</key>
			<string>464703810278492</string>
			<key>CFBundleURLTypes</key>
			<array>
				<dict>
					<key>CFBundleURLSchemes</key>
					<array>
						<string>fb464703810278492</string>
					</array>
				</dict>
			</array>
		</dict>
		</plist>
	'''

	print("Updating Plist file")
	f = open(pathXcodeProject + '/Info.plist', 'r+')
	content = f.read()
	f.seek(content.find('</dict>\n</plist>'))
	f.write(addToPlist)
	f.close()

#####################################


########### Adding Frameworks ###########

def addFrameworksToProject():
	print "Adding Frameworks"
	frameworksList = {
	   "frameworksCore" : {
	   "path" : "System/Library/Frameworks",
	   "ext" : "framework",
	   "tree" : 'SDKROOT',
	    "list" : [
          {"name" : "AssetsLibrary"},
          {"name" : "Accelerate"},
          {"name" : "CoreText"},
          {"name" : "CoreData"},
          {"name" : "QuartzCore"},
	      {"name" : "StoreKit"},
	      {"name" : "SystemConfiguration"},
	      {"name" : "AdSupport"},
	      {"name" : "AudioToolbox"},
	      {"name" : "CoreTelephony"},
	      {"name" : "MessageUI"},
	      {"name" : "Twitter"},
	      {"name" : "Foundation"},
	      {"name" : "EventKit"},
	      {"name" : "EventKitUI"}
	    ]
	  }
	}

	def addFrameworks(frameworksList):
		framework_group = project.get_or_create_group("Frameworks")
		for key in iter(frameworksList):
			print "Adding Framework set : %s" %key
			addFrameworksSet(frameworksList[key], parent=framework_group)

	def addFrameworksSet(frameworkSet, parent=None):
		path = frameworkSet['path']
		ext = frameworkSet['ext']
		f_list = frameworkSet['list']
		tree = frameworkSet.get("tree", 'SDKROOT')

		for entry in f_list:
			name = entry['name'] + '.' + ext
			weak = entry.get('weak', False)
			fullPath = os.path.join(path, name)
			print "'%s', tree='%s', weak=%s" %(fullPath, tree, weak)
			project.add_file(fullPath, tree=tree, parent=parent, weak=weak)

	addFrameworks(frameworksList)

#####################################

########### Replacing Files/Folders ###########

def replaceFilesInProject():
	relPathToReplaceFiles = "files/ToReplace" # path should be relative to integeration folder
	srcPath_FilesToReplace = os.path.join(pathToIntegerationFolder, relPathToReplaceFiles)
	dstPath_FilesToReplace = pathXcodeProject
	print "Replacing Files from %s => %s" %(srcPath_FilesToReplace, dstPath_FilesToReplace)
	#replace files/folders only if exist (if file doesn't exist in dstPath it will not copy(add) those files)
	file_helper.recursive_replaceFiles(srcPath_FilesToReplace, dstPath_FilesToReplace)

#####################################


###########  Adding Files/Folders ###########

def addFilesInProjectWithoutNativeCode():
	addFilesInProject("files/ToAddTemp")

def addFilesInProjectWithNativeCode():
	addFilesInProject("files/ToAdd")

def addFilesInProject(relPath):
	relPathToAddFiles = relPath
	srcPath_FilesToAdd = os.path.join(pathToIntegerationFolder, relPathToAddFiles)
	dstPath_FilesToAdd = pathXcodeProject
	print "Adding Files from %s => %s" %(srcPath_FilesToAdd, dstPath_FilesToAdd)

	# @Note : it will only create link not actually copy files
	# TODO : add ignore files
	def add_files_to_project(src):
		files = os.listdir(src)
		for name in files:
			fullPath = os.path.join(src, name)
			if os.path.isdir(fullPath):
				print "Adding Folder \"%s\"" %fullPath
				project.add_folder(fullPath, excludes=["^.*\.DS_Store"])
			elif os.path.isfile(fullPath):
				print "Adding File \"%s\"" %fullPath
				project.add_file(fullPath)
			else:
				print "\"%s\" is not a file nor directory. So couldn't add" % fullPath

	add_files_to_project(srcPath_FilesToAdd)

#####################################

###########  Adding Native iOS App Code ###########

def addNativeIOSCode():
	print "Adding Native iOS App Code"
	relPathToNativeCode = "AppNative/PocketFights/FinalMerge"
	srcPath_NativeCode = os.path.join(pathToIntegerationFolder, relPathToNativeCode)

	classes_group = project.get_or_create_group("Classes")
	project.add_folder(srcPath_NativeCode, parent=classes_group)


# def enableGCC_OBJ_Flag() :
# 	project.add_other_buildsetting('GCC_ENABLE_OBJC_EXCEPTIONS', 'YES')

#####################################

###########  Adding Plugins Code ###########

def addPluginsFromUnity():
	print "TODO: method not yet implemented"
	# TODO : Implement this method
	# relPathToNativeCode = "AppNative/PocketFights/Native"
	# srcPath_NativeCode = os.path.join(pathToIntegerationFolder, relPathToNativeCode)

	# classes_group = project.get_or_create_group("Classes")
	# project.add_folder(srcPath_NativeCode, parent=classes_group)
	# project.add_folder(filesToAddPath, excludes=["^.*\.meta$"], parent=classes_group);

#####################################


############  Adding Plugins Code ###########

# def :
# 	print "TODO: method not yet implemented"
	# TODO : Implement this method
	# relPathToNativeCode = "AppNative/PocketFights/Native"
	# srcPath_NativeCode = os.path.join(pathToIntegerationFolder, relPathToNativeCode)

	# classes_group = project.get_or_create_group("Classes")
	# project.add_folder(srcPath_NativeCode, parent=classes_group)
	# project.add_folder(filesToAddPath, excludes=["^.*\.meta$"], parent=classes_group);

#####################################



def endEditingXcodeProject():
	print "Saving Updated Xcode Project"
	if project.modified:
		project.backup()
		project.saveFormat3_2() #IMPORTANT, DONT USE THE OLD VERSION!


########### Build Process ###########

# Integerated Code with iOS Native Screens
def buildWithNativeCode():
	# updatePlist()
  addFrameworksToProject()
  addFilesInProjectWithNativeCode()
  endEditingXcodeProject()
	# replaceFilesInProject()
	# addNativeIOSCode()
	# enableGCC_OBJ_Flag()


# Only Unity Screens (Scenes)
# NOTE : u need to enable StartScene GUI Buttons to switch b/w scenes (in unity project StartScene.cs file (#if UNITY_EDITOR))
def buildWithoutNativeCode():
	addFilesInProjectWithoutNativeCode()
	endEditingXcodeProject()

#####################################

########### Run Build Process ###########

# buildWithoutNativeCode()
buildWithNativeCode()


#####################################
