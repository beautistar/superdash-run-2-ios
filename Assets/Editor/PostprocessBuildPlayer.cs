// @author mayank
// src @ https://gist.github.com/darktable/3173013 and made few changes

using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditor.Callbacks;
using System.IO;

// This script will run all python script files that starts with "postprocessbuildplayer_"
public class PostprocessBuildPlayer : ScriptableObject {
	#if UNITY_IPHONE
  [PostProcessBuild(999)] // 999 : max the number script will be run in the end (i use high number to make sure all other postprocess scripts run before this script)
  static void OnPostprocessBuildPlayer(BuildTarget target, string buildPath) {
    string editorPath = Path.GetFullPath(Path.GetDirectoryName(AssetDatabase.GetAssetPath(MonoScript.FromScriptableObject(ScriptableObject.CreateInstance<PostprocessBuildPlayer>()))));

    if (!Directory.Exists(editorPath)) {
      Debug.LogError("No directory at: " + editorPath);
      return;
    }

    if (!Directory.Exists(buildPath)) {
      Debug.LogError("No directory at: " + buildPath);
      return;
    }

    var files = Directory.GetFiles(editorPath, "*.*", SearchOption.TopDirectoryOnly);

    files = System.Array.FindAll<string>(files, (x) => {
      var name = Path.GetFileName(x).ToLower();

      return name.StartsWith("postprocessbuildplayerscript_") && !name.EndsWith("meta");
    });

    if (files.Length == 0) {
      Debug.LogError("No postprocess scripts at: " + editorPath);
      return;
    }

    // string filesToCopyPath = Application.dataPath + "/Plugins/UnityNative/UnityNative";

    var args = new string[] {
      buildPath,
      // filesToCopyPath,
      target.ToString()
    };

    // to stop executing post build player script .. uncomment return
    // return;

    // Build Process Start
    foreach (var file in files) {
      Debug.Log("Running PostProcessBuildPlayerScript : " + file);
      using (var process = new System.Diagnostics.Process()) {
        process.StartInfo.FileName = "python";
        process.StartInfo.Arguments = file + " " + string.Join(" ", args);
        Debug.Log(process.StartInfo.Arguments);
        process.StartInfo.RedirectStandardError = true;
        process.StartInfo.UseShellExecute = false;

        process.Start();
        string error = process.StandardError.ReadToEnd();

        process.WaitForExit();

        if (process.ExitCode != 0) {
          Debug.LogError(process.ExitCode + " : " + error);
        }
      }
    }
    // Build Process End
  }
#endif
}
