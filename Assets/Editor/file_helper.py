import os
import shutil

# this will replace files (only files that exists)
def recursive_replaceFiles(src, dest, ignore=None):
    if os.path.isdir(src):
        if not os.path.isdir(dest):
            print("Dest Dir \"%s\" doesn't exist."  % dest);
            return None
            # os.makedirs(dest)
        files = os.listdir(src)
        if ignore is not None:
            ignored = ignore(src, files)
        else:
            ignored = set()
        for f in files:
            if f not in ignored:
                recursive_replaceFiles(os.path.join(src, f),
                                    os.path.join(dest, f),
                                    ignore)
    else:
        if (os.path.isfile(dest)):
            shutil.copyfile(src, dest)
        else:
            print("Dest File \"%s\" doesn't exist. So couldn't be replaced." % dest)

