﻿using UnityEngine;
using System.Collections;

public class ScaleChangeScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	/*void Update () {
		/*if(!renderer.isVisible)
			Resources.UnloadAsset(renderer.material.mainTexture);
	}*/

	public void ChangeScale()
	{
		if(transform.localScale.x > 0)
            transform.localScale = new Vector3(-1,1,1);
        else
            transform.localScale = new Vector3(1,1,1);   
	}
}
