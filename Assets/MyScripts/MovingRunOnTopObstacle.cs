﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class MovingRunOnTopObstacle : MonoBehaviour 
{
public int movingSquareDistanceRunOnTop;
public int movingSpeedObstacleRunOnTop;
bool moving = false,collidedStopMoving = false;
Transform thisTransform;
Vector3 localPos = new Vector3(-1,-1,-1);
private Transform playerTransform;
	private int platformLayer;
    private int floorLayer;
	private int wallLayer;
    private int pitLayer;
    private int obstacleLayer;
    private int platformJumpLayer;
    private int movingObstacleLayer;
    private int turnTriggerLayer;
    private bool isGamePaused;
	// Use this for initialization
	void Start () 
	{
		thisTransform = transform;
		//localPos.x = transform.localPosition.x;
		//localPos.y = transform.position.y;
		//localPos.z = transform.localPosition.z;
		playerTransform = PlayerController.instance.transform;
		platformLayer = LayerMask.NameToLayer("Platform");
        floorLayer = LayerMask.NameToLayer("Floor");
		wallLayer = LayerMask.NameToLayer("Wall");
        pitLayer = LayerMask.NameToLayer("Pit");
        obstacleLayer = LayerMask.NameToLayer("Obstacle");
        platformJumpLayer = LayerMask.NameToLayer("PlatformJump");
        movingObstacleLayer = LayerMask.NameToLayer("MovingObstacle");
        turnTriggerLayer = LayerMask.NameToLayer("TurnTrigger");
	}

	public void OnEnable()
    {
        GameManager.instance.onPauseGame += onPauseGame;
    }

    public void OnDisable()
    {
    	SetMyLoacation();
        GameManager.instance.onPauseGame -= onPauseGame;
    }

    public void onPauseGame(bool paused)
    {
    	isGamePaused = paused;
    }
	
	// Update is called once per frame
	void Update () 
	{
		if(isGamePaused == false)
		if(!collidedStopMoving)
		{
			if(moving)
				if(movingSpeedObstacleRunOnTop > 0)
					transform.Translate(Vector3.forward * Time.deltaTime * (movingSpeedObstacleRunOnTop+1));
				else
					transform.Translate(Vector3.forward * Time.deltaTime * (movingSpeedObstacleRunOnTop-1));	
			else {
	            // take the square because square roots are expensive
	            if(thisTransform != null && playerTransform != null)
	            if (Mathf.Abs(Vector3.Dot(thisTransform.forward, playerTransform.forward)) > 0.99f && Vector3.SqrMagnitude(thisTransform.position - playerTransform.position) < movingSquareDistanceRunOnTop) {
	                moving = true;
	            }
	        }
    	}
	}


	void OnTriggerEnter(Collider colli)
	{
		if((colli.gameObject.tag).Equals("Player"))
		{
			moving = false;
			collidedStopMoving = true;

			//--------------------------------------------------------------------------------------------------------------------------------------------------------------------/
			if(PowerUpManager.instance.getActivePowerUps ()[0] != PowerUpTypes.Invincibility && PowerUpManager.instance.getActivePowerUps ()[1] != PowerUpTypes.Invincibility && GameManager.instance.IsGameRunnnig) {
				StartCoroutine("StartMovingAgain");
			}
			//--------------------------------------------------------------------------------------------------------------------------------------------------------------------/

		}
		if(colli.gameObject.layer == obstacleLayer  || colli.gameObject.layer == wallLayer)
		{
			moving = false;
			collidedStopMoving = true;
			StartCoroutine("StartMovingAgain");
		}

		if(colli.gameObject.layer == turnTriggerLayer)
		{
			//gameObject.SetActive(false);
			//transform.position = new Vector3(0,0,-10);
			moving = false;
			collidedStopMoving = true;
			//if(gameObject.activeSelf)
				StartCoroutine("StartMovingAgain");
		}
	}

	private IEnumerator StartMovingAgain()
	{
		yield return new WaitForSeconds(10);
		collidedStopMoving = false;
		//SetMyLoacation();
	}

	void Awake() {
		if(localPos.x == -1 && localPos.y == -1 && localPos.z == -1)
		{
			localPos.x = transform.localPosition.x;
			localPos.y = transform.localPosition.y;
			localPos.z = transform.localPosition.z;
			return;
		}
	}


	public void SetMyLoacation()
	{
		transform.localPosition = localPos;
		collidedStopMoving = false;
	}

}
