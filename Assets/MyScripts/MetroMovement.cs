﻿using UnityEngine;
using System.Collections;

public class MetroMovement : MonoBehaviour {
Vector3 startPos,temp;
Transform thisTransform;
	// Use this for initialization
	void Start () {
		thisTransform = transform;
		startPos = transform.localPosition;
		temp = new Vector3(0.2f,0,0);
	}
	
	// Update is called once per frame
	void Update () {
		if(thisTransform.localPosition.x > -30 )
		{
			thisTransform.localPosition -= temp;
		}
		else
		{
			thisTransform.localPosition = startPos;
		}
	}
}
