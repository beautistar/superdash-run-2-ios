﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class CombibingMeshes : MonoBehaviour {
	public Vector3 temperoryVector;
	// Use this for initialization
	void Start () {
		MeshFilter[] meshFilters = GetComponentsInChildren<MeshFilter>();
		CombineInstance[] combine = new CombineInstance[meshFilters.Length];
		int i = 0;
		while (i < meshFilters.Length) {
			combine[i].mesh = meshFilters[i].sharedMesh;
			Matrix4x4 tempMatrix = Matrix4x4.TRS(meshFilters[i].transform.localPosition,meshFilters[i].transform.localRotation,meshFilters[i].transform.localScale);
			combine[i].transform = tempMatrix;//meshFilters[i].transform;
			//combine[i].transform = meshFilters[i].transform.localToWorldMatrix;
			meshFilters[i].gameObject.active = false;
			i++;
		}
		transform.GetComponent<MeshFilter>().mesh = new Mesh();
		transform.GetComponent<MeshFilter>().mesh.CombineMeshes(combine,true,true);
		//transform.position = new Vector3 (-0.1432159f, -7.481266f, 47.57819f);
		//transform.localPosition = temperoryVector;
		//transform.localPosition = new Vector3 (0.02409592f,-7.407046f, 48.04066f);
		transform.gameObject.active = true;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
