﻿using UnityEngine;
using System.Collections;

public class BgAnimation : MonoBehaviour {
	Animator myAnimator;
	// Use this for initialization
	static public BgAnimation instance;
	void Awake()
	{
			instance = this;
	}
	void Start () {
		myAnimator = GetComponent<Animator> ();
	}
	
	// Update is called once per frame

	public void OnPlayButton()
	{
		myAnimator.SetTrigger("Play");
	}

	public void SetMyLayOut(bool temp)
	{
		gameObject.SetActive(temp);
	}

	public void OnRePlayButton()
	{
		gameObject.SetActive(true);
		myAnimator = GetComponent<Animator> ();
		myAnimator.SetTrigger("Replay");
	}
}
