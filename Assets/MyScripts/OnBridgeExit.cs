﻿using UnityEngine;
using System.Collections;

public class OnBridgeExit : MonoBehaviour 
{
private CameraController cameraController;
	// Use this for initialization
	void Start () 
	{
		cameraController = CameraController.instance;
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void OnTriggerEnter(Collider colli)
	{
		if((colli.gameObject.tag).Equals("Player"))
		{
			cameraController.CameraUnderBridge(false);
		}
	}

	/*void OnTriggerExit(Collider colli)
	{
		if((colli.gameObject.tag).Equals("Player"))
		{
			print("I Am Exit");
			cameraController.isCameraUnderBridge = false;
		}
	}

	void OnTriggerStay(Collider colli)
	{
		if((colli.gameObject.tag).Equals("Player"))
		{
			print("I Am Exit");
			cameraController.isCameraUnderBridge = false;
		}
	}*/
}

