﻿using UnityEngine;
using System.Collections;

public class ChaseCharacterPrefab : MonoBehaviour 
{
	public GameObject blackFather,whiteFather,whiteMother,blackMother;
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public GameObject ChasingCharacter()
	{
		int tempPlayer = PlayerPrefs.GetInt ("SelectedCharacter", 0);
		if (tempPlayer == 0 || tempPlayer == 2)
				return whiteFather;
				else if (tempPlayer == 1 || tempPlayer == 3)
						return blackFather;
						else if (tempPlayer == 4 || tempPlayer == 6)
								return whiteMother;
								else if (tempPlayer == 5 || tempPlayer == 7)
										return blackMother;
		return null;
	}
}
