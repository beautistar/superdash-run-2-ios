﻿using UnityEngine;
using System.Collections;

public class MyDirection : MonoBehaviour {

	public GameObject temp;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public int GetMyDirection()
	{
		if((transform.position.z - temp.transform.position.z) >= 8)
			return 1;
		else if((transform.position.z - temp.transform.position.z) <= -8)
				return 2;
		else if((transform.position.x - temp.transform.position.x) >= 8)
			return 3;
		else return 4;		

	}
}
