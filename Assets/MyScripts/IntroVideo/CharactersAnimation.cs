﻿using UnityEngine;
using System.Collections;

public class CharactersAnimation : MonoBehaviour {
	Animator myAnimator;
	// Use this for initialization
	static public CharactersAnimation instance;
	void Awake()
	{
		instance = this;
	}
	void Start () {
		
	}
	
	
	public void OnPlayButton()
	{
		myAnimator = GetComponent<Animator> ();
		myAnimator.SetTrigger("Play");
	}

	public void SetMyLayOut(bool temp)
	{
		gameObject.SetActive(temp);
	}

	public void OnRePlayButton()
	{
		gameObject.SetActive(true);
		myAnimator = GetComponent<Animator> ();
		myAnimator.SetTrigger("Replay");
	}
}
