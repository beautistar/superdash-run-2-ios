﻿using UnityEngine;
using System.Collections;

public class CharactersAnimationStay : MonoBehaviour {
	Animator myAnimator;
	static public CharactersAnimationStay instance;
	void Awake()
	{
		instance = this;
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnPlayButton()
	{
		myAnimator = GetComponent<Animator> ();
		myAnimator.SetTrigger("Play");
	}

	public void SetMyLayOut(bool temp)
	{
		gameObject.SetActive(temp);
	}

	public void OnRePlayButton()
	{
		gameObject.SetActive(true);
		myAnimator = GetComponent<Animator> ();
		myAnimator.SetTrigger("Replay");
	}
}
