﻿using UnityEngine;
using System.Collections;

public class MudSlide : MonoBehaviour {
PlayerController playerController;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other)
    {
    	
        if (other.gameObject.layer == LayerMask.NameToLayer("Player")) {
        	playerController = PlayerController.instance;
        	playerController.MudSlidetrigger(true);
        }
	}

	void OnTriggerExit(Collider other)
	{
		if (other.gameObject.layer == LayerMask.NameToLayer("Player")) {
			playerController = PlayerController.instance;
        	playerController.MudSlidetrigger(false);
        }
	}
}
