﻿using UnityEngine;
using System.Collections;

public class ZipLiningTutorialTrigger : MonoBehaviour {

	void OnTriggerEnter(Collider other){
		if (other.gameObject.layer == LayerMask.NameToLayer("Player")) {
			if(PlayerPrefs.GetInt("ZipLiningTutorial", 0) == 0)
				TutorialManager.Instance.ShowUp();
			//GetComponent<GUIText>().text = "Swipe Up To Jump";
		}
	}
}
