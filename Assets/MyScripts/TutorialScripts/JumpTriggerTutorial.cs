using UnityEngine;
using System.Collections;

public class JumpTriggerTutorial : MonoBehaviour {

	private bool _isIn = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame

	void OnTriggerEnter(Collider other){
		if (other.gameObject.layer == LayerMask.NameToLayer("Player") && !_isIn) {
			_isIn = true;
			TutorialManager.Instance.ShowUp();

			TutorialSign.Instance.ShowStartSign();
			//GetComponent<GUIText>().text = "Swipe Up To Jump";
		}
	}

	void OnTriggerExit(Collider other){
		if (other.gameObject.layer == LayerMask.NameToLayer("Player")) {
			_isIn = false;
			//TutorialManager.Instance.StopAnimation();
			//GetComponent<GUIText>().text = "";
		}
	}
}
