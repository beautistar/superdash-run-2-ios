using UnityEngine;
using System.Collections;

public class slideTriggerTutorial : MonoBehaviour {

	private bool _isIn = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame

	void OnTriggerEnter(Collider other){
		if (other.gameObject.layer == LayerMask.NameToLayer("Player") && !_isIn) {
			_isIn = true;
			TutorialManager.Instance.ShowDown();
			//GetComponent<GUIText>().text = "Swipe Down To Slide";
		}
	}

	void OnTriggerExit(Collider other){
		if (other.gameObject.layer == LayerMask.NameToLayer("Player")) {
			_isIn = false;
			TutorialManager.Instance.StopAnimation();
			//GetComponent<GUIText>().text = "";
		}
	}
}
