using UnityEngine;
using System.Collections;

public class leftTriggerTutorial : MonoBehaviour {
	//GameObject Tutorial;
	// Use this for initialization

	private bool _isIn = false;

	void Start () {
		//Tutorial = GameObject.FindGameObjectWithTag ("Tutorial").gameObject;
	}
	
	// Update is called once per frame

	void OnTriggerEnter(Collider other){
		if (other.gameObject.layer == LayerMask.NameToLayer("Player") && !_isIn ) {
			_isIn = true;
			TutorialManager.Instance.ShowLeft();
			//print("Swipe Left");
			//GetComponent<GUIText>().text = "Swipe Left";
		}
	}

	void OnTriggerExit(Collider other){
		if (other.gameObject.layer == LayerMask.NameToLayer("Player")) {
			_isIn = false;
			TutorialManager.Instance.StopAnimation();
			//print("Swipe Left");
			//GetComponent<GUIText>().text = "";
		}
	}
}
