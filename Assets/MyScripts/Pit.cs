﻿using UnityEngine;
using System.Collections;

public class Pit : MonoBehaviour 
{
BoxCollider boxColli;
	// Use this for initialization
	void Start () 
	{
		boxColli = GetComponent<BoxCollider>();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void OnCollisionEnter(Collision collision)
	{
		if((collision.gameObject.tag).Equals("Player"))
		{
			GetComponent<Collider>().isTrigger = true;
		}

	}
}
