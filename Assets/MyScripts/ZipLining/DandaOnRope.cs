﻿using UnityEngine;
using System.Collections;

public class DandaOnRope : MonoBehaviour {

	public GameObject [] myChildren = new GameObject[3];

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other)
	{
		if((other.gameObject.tag).Equals("Player") || (other.gameObject.transform.parent.tag).Equals("Player"))
		{
			print ("====Colliding----"+other.gameObject.tag+"-----"+other.gameObject.name);
			for(int i = 0;i<myChildren.GetLength(0);i++)
			{
				myChildren[i].GetComponent<MeshRenderer>().enabled = false;
			}
			StartCoroutine ("EnablingMeshRenderer");
		}
	}

	private IEnumerator EnablingMeshRenderer()
	{
		yield return new WaitForSeconds (0.5f);
		for(int i = 0;i<myChildren.GetLength(0);i++)
		{
			myChildren[i].GetComponent<MeshRenderer>().enabled = true;
		}
	}
}
