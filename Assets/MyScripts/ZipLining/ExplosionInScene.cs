﻿using UnityEngine;
using System.Collections;

public class ExplosionInScene : MonoBehaviour {
	public GameObject explosionPrefab;
	public Transform explosionLocation;
	GameObject seaCreature;
	public GameObject seaCreatureMesh;
	// Use this for initialization
	void Start () {
		if(seaCreatureMesh != null)
			seaCreatureMesh.SetActive (true);
	}

	void OnEnable()
	{
		if (seaCreature != null)
						Destroy (seaCreature);
		if(seaCreatureMesh != null)
			seaCreatureMesh.SetActive (true);
	}

	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other)
	{
		if ((other.gameObject.tag).Equals ("Player")) 
		{

			//print ("Called For Explosion-------");
			seaCreature =(GameObject) GameObject.Instantiate(explosionPrefab,explosionLocation.position,explosionLocation.rotation);	
			seaCreature.transform.parent = transform;
			if(seaCreatureMesh != null)
			{
				seaCreatureMesh.SetActive(false);
			}
		}
	}
}
