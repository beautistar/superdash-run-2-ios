﻿using UnityEngine;
using System.Collections;

public class Scene21_RandomBg : MonoBehaviour 
{
	public GameObject myBg1;
	public GameObject myBg2;
	void OnEnable()
	{
		print ("---OnEnable");
		float temp = Random.Range (0, 1);
		if (temp < 0.5f) {
						myBg1.SetActive (true);
						myBg2.SetActive (false);
				} else {
			myBg2.SetActive (true);
			myBg1.SetActive (false);
				}
	}
}
