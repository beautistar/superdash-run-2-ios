﻿using UnityEngine;
using System.Collections;

public class ScooterSound : MonoBehaviour {

	private AudioSource mySounds;

	void OnEnable()
	{
		mySounds = GetComponent<AudioSource>();
		if(SettingStaticData.Instance.getSoundOnOff())
        {
            mySounds.enabled = true;
            mySounds.volume = SettingStaticData.Instance.getSoundValue();
        }
        else
        {
        	mySounds.enabled = false;
        }
	}
}
