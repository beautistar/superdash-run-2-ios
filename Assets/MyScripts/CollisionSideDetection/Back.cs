﻿using UnityEngine;
using System.Collections;

public class Back : MonoBehaviour {

int mySelfTrigger;
protected int playerLayer;
BoxCollider boxCollider;
	// Use this for initialization
	void Start () {

		mySelfTrigger = 0;
		playerLayer = LayerMask.NameToLayer("Player");
		boxCollider = GetComponent<BoxCollider>();
		boxCollider.center = boxCollider.center - new Vector3(0,0,0.7f);
	}

	private bool _isIn = false;

	void OnTriggerEnter(Collider other) 
	{

        if (other.gameObject.layer == LayerMask.NameToLayer("Player") && !_isIn) {
			_isIn = true;

			Debug.LogError("=====>>>>OnTriggerEnter" + other.name + " - " + name);

        	if (!PowerUpManager.instance.isPowerUpActive(PowerUpTypes.SpeedIncrease) && !PowerUpManager.instance.isPowerUpActive(PowerUpTypes.BoltPack) && !PowerUpManager.instance.isPowerUpActive(PowerUpTypes.Invincibility) && !PowerUpManager.instance.isPowerUpActive(PowerUpTypes.SuperPogoStick)) {
	        	boxCollider.isTrigger = false;

	        	//StartCoroutine(ReSetMyTrigger());
	            mySelfTrigger = 1;
	            transform.parent.GetComponent<ObstacleObject>().AfterCollision(1,other);
        	}
		}

	}

	void OnTriggerExit(Collider other) {
		if (other.gameObject.layer == LayerMask.NameToLayer("Player")) {
			Debug.LogError("=====>>>> Trigger exit: " + name);
			_isIn = false;
		}
	}

	void OnTriggerStay(Collider other) 
	{
		if (other.gameObject.layer == LayerMask.NameToLayer ("Player")  ) 
		{
			if(PlayerController.instance.IsAfterLifeSaver())
			{
				transform.parent.gameObject.SetActive(false);
				//StartCoroutine("ReSetMyRenderer");
			}

		}
	}

	public int MySelfTriggeringGetter()
	{
		return mySelfTrigger;
	}

	public void MySelfTriggeringSetter()
	{
		mySelfTrigger = 0;
	}

	IEnumerator ReSetMyRenderer()
	{
		yield return new WaitForSeconds(0.4f);
		transform.parent.gameObject.SetActive(true);
	}

	IEnumerator ReSetMyTrigger()
	{
		yield return new WaitForSeconds(0.4f);
		boxCollider.isTrigger = true;
	}
}
