﻿using UnityEngine;
using System.Collections;

public class MovingCoins : MonoBehaviour {

public int movingSquareDistanceRunOnTop;
public int movingSpeedObstacleRunOnTop;
bool moving = false,collidedStopMoving = false;
Transform thisTransform;
Vector3 localPos = new Vector3(-1,-1,-1);
private Transform playerTransform;
	private int platformLayer;
    private int floorLayer;
	private int wallLayer;
    private int pitLayer;
    private int obstacleLayer;
    private int platformJumpLayer;
    private int movingObstacleLayer;
	// Use this for initialization
	void Start () {
		thisTransform = transform;
		playerTransform = PlayerController.instance.transform;
		platformLayer = LayerMask.NameToLayer("Platform");
        floorLayer = LayerMask.NameToLayer("Floor");
		wallLayer = LayerMask.NameToLayer("Wall");
        pitLayer = LayerMask.NameToLayer("Pit");
        obstacleLayer = LayerMask.NameToLayer("Obstacle");
        platformJumpLayer = LayerMask.NameToLayer("PlatformJump");
        movingObstacleLayer = LayerMask.NameToLayer("MovingObstacle");
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(!collidedStopMoving)
		{
			if(moving)
				transform.Translate(Vector3.forward * Time.deltaTime * movingSpeedObstacleRunOnTop);
			else {
	            // take the square because square roots are expensive
	            if (Mathf.Abs(Vector3.Dot(thisTransform.forward, playerTransform.forward)) > 0.99f && Vector3.SqrMagnitude(thisTransform.position - playerTransform.position) < movingSquareDistanceRunOnTop) {
	                moving = true;
	            }
	        }
    	}
	}

	void OnTriggerEnter(Collider colli)
	{
		if((colli.gameObject.tag).Equals("Player"))
		{
			moving = false;
			collidedStopMoving = true;
			StartCoroutine("StartMovingAgain");
		}
		if(colli.gameObject.layer == obstacleLayer  || colli.gameObject.layer == wallLayer)
		{
			moving = false;
			collidedStopMoving = true;
			StartCoroutine("StartMovingAgain");
		}
	}

	private IEnumerator StartMovingAgain()
	{
		yield return new WaitForSeconds(5);
		collidedStopMoving = false;
	}

	void OnEnable() {
		if(localPos.x == -1 && localPos.y == -1 && localPos.z == -1)
		{
			localPos.x = transform.localPosition.x;
			localPos.y = transform.localPosition.y;
			localPos.z = transform.localPosition.z;
			return;
		}
		else
		{
			transform.localPosition = localPos;
			collidedStopMoving = false;
		}
	}
}
