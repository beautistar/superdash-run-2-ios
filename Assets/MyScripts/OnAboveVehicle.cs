﻿using UnityEngine;
using System.Collections;

public class OnAboveVehicle : MonoBehaviour {
PlayerController playerController;
BoxCollider boxColli;
	// Use this for initialization
	void Start () {
		boxColli = GetComponent<BoxCollider>();
	}
	
	// Update is called once per frame
	void Update () {
		if(PowerUpManager.instance.isPowerUpActive(PowerUpTypes.Invincibility) || PowerUpManager.instance.isPowerUpActive(PowerUpTypes.BoltPack) || PowerUpManager.instance.isPowerUpActive(PowerUpTypes.SpeedIncrease)){
			boxColli.isTrigger = true; 
		}
		else{
			boxColli.isTrigger = false; 
		}
	}

	/*void OnCollisionEnter(Collision colli)
	{
		if (colli.gameObject.layer == LayerMask.NameToLayer("Player")) {
		if(PowerUpManager.instance.isPowerUpActive(PowerUpTypes.Invincibility) || PowerUpManager.instance.isPowerUpActive(PowerUpTypes.BoltPack))
			boxColli.isTrigger = true; 
		}
	}

	void OnCollisionStay(Collision colli) {
		if (colli.gameObject.layer == LayerMask.NameToLayer("Player")) {
		if(PowerUpManager.instance.isPowerUpActive(PowerUpTypes.Invincibility) || PowerUpManager.instance.isPowerUpActive(PowerUpTypes.BoltPack))
			boxColli.isTrigger = true; 
		}
	}

	void OnCollisionExit(Collision colli)
	{
		if (colli.gameObject.layer == LayerMask.NameToLayer("Player")) {
		//if(PowerUpManager.instance.isPowerUpActive(PowerUpTypes.Invincibility) || PowerUpManager.instance.isPowerUpActive(PowerUpTypes.BoltPack))
			boxColli.isTrigger = false;
		} 
	}*/

	/*void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.layer == LayerMask.NameToLayer("Player")) {
			boxColli.isTrigger = false;
        	playerController = PlayerController.instance;
        	playerController.RunAboveVehicle(true);
        	
        }
	}

	void OnTriggerExit(Collider other)
	{
		if (other.gameObject.layer == LayerMask.NameToLayer("Player")) {
			StartCoroutine("GladiatorHeightIncreasing");
        	playerController = PlayerController.instance;
        	playerController.RunAboveVehicle(false);
        }
	}*/

	private IEnumerator GladiatorHeightIncreasing()
	{
		yield return new WaitForSeconds(1f);
		boxColli.isTrigger = true;
	}
}
