﻿using UnityEngine;
using System.Collections;

public class MudSplashParent : MonoBehaviour 
{
	private MudSplashTrickleDown[] mudSplashTrickleDown;
	public void OnEnable1()
	{
		//GetComponentInChildren<MudSplashTrickleDown> ().OnEnable1 ();
		mudSplashTrickleDown = GetComponentsInChildren<MudSplashTrickleDown> ();

			
		StartCoroutine (TimeDuration ());
	}
	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	IEnumerator TimeDuration()
	{
		for (int i=0; i<mudSplashTrickleDown.Length; i++) 
		{
			mudSplashTrickleDown [i].OnEnable1 ();
			yield return new WaitForSeconds(0.1f);
		}
	}
}
