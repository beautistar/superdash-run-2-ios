﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameService : MonoBehaviour
{
	public static GameService instance = null;

	string stLeaderboardID;

	void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else if (instance != null)
		{
			Destroy(gameObject);
		}

		DontDestroyOnLoad(gameObject);

		stLeaderboardID = "SuperDashRun_Leaderboard";
	}

	void Start()
	{
		Connect ();

//		UM_ExampleStatusBar.text = "Connecting To Game Service";

		UM_GameServiceManager.OnPlayerConnected += OnPlayerConnected;
		UM_GameServiceManager.OnPlayerDisconnected += OnPlayerDisconnected;

		if(UM_GameServiceManager.Instance.ConnectionSate == UM_ConnectionState.CONNECTED) {
			OnPlayerConnected();
		}
	}

	private void OnPlayerConnected() {
//		UM_ExampleStatusBar.text = "Player Connected";
	}


	private void OnPlayerDisconnected() {
//		UM_ExampleStatusBar.text = "Player Disconnected";
	}

	private void Disconnect()
	{
		UM_GameServiceManager.Instance.Disconnect();
	}

	private void Connect()
	{
		UM_GameServiceManager.Instance.Connect();
	}

	public void ShowLeaderboardUI()
	{
		UM_GameServiceManager.Instance.ShowLeaderBoardUI (stLeaderboardID);
//		UM_ExampleStatusBar.text = "Showing " + stLeaderboardID + " UI";
	}

	public void SubmitScore(long score)
	{
//		if (score > GetCurrentScore ()) 
		{
			UM_GameServiceManager.ActionScoreSubmitted += HandleActionScoreSubmitted;
			UM_GameServiceManager.Instance.SubmitScore (stLeaderboardID, score);
//			UM_ExampleStatusBar.text = "Score " + score.ToString () + " Submited to " + stLeaderboardID;
		}
	}

	public long GetCurrentScore()
	{
		long s = UM_GameServiceManager.Instance.GetCurrentPlayerScore(stLeaderboardID).LongScore;
//		UM_ExampleStatusBar.text = "GetCurrentPlayerScore from  " + stLeaderboardID + " is: " + s;
		return s;
	}

	public void ShowAchievementUI()
	{
		UM_GameServiceManager.Instance.ShowAchievementsUI ();
//		UM_ExampleStatusBar.text = "Showing Achievements UI";
	}

	public void ResetAchievement()
	{
		UM_GameServiceManager.Instance.ResetAchievements();
//		UM_ExampleStatusBar.text = "Al acievmnets reseted";
	}

	public void UnlockAchievement(string stAchievementID)
	{
		UM_GameServiceManager.Instance.UnlockAchievement(stAchievementID);
//		UM_ExampleStatusBar.text = "Achievement " + stAchievementID  + " Reported";
	}

	void HandleActionScoreSubmitted (UM_LeaderboardResult res) {
		if(res.IsSucceeded) {
			UM_Score playerScore = res.Leaderboard.GetCurrentPlayerScore(UM_TimeSpan.ALL_TIME, UM_CollectionType.GLOBAL);
			if (playerScore != null) {
				Debug.Log("Score submitted, new player high score: " + playerScore.LongScore);	
			}
		} else {
			Debug.Log("Score submission failed: " + res.Error.Code + " / " + res.Error.Description);
		}

	}
}