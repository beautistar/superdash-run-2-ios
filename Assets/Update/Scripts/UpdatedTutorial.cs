﻿using UnityEngine;
using System.Collections;

public class UpdatedTutorial : MonoBehaviour {
    public GameObject tutorialEndMessage;

    public static UpdatedTutorial Instance;

    void Awake()
    {
        Instance = this;
    }

    public void ShowTutorialSuccessMsg()
    {
        tutorialEndMessage.SetActive(true);
        StartCoroutine(HideMessage());
    }

    private IEnumerator HideMessage()
    {
        yield return new WaitForSeconds(2.0f);

        HideTutorialSuccessMsg();
    }

    public void HideTutorialSuccessMsg()
    {
        tutorialEndMessage.SetActive(false);
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

}
