﻿using UnityEngine;
using System.Collections;

public class TimedObjectDestructor : MonoBehaviour {

	public float timeOut;
	public bool detachChildren = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void DestroyNow ()
	{
		if (detachChildren) {
			transform.DetachChildren ();
		}
		DestroyObject (gameObject);
	}
}
