﻿using UnityEngine;
using System.Collections;

public class AdsControl : MonoBehaviour {

	public static AdsControl instance = null;

	public const string ADS_REMOVED_KEY = "ads_removed";

	void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else if (instance != null)
		{
			Destroy(gameObject);
		}

		DontDestroyOnLoad(gameObject);
	}

	bool adsActive()
	{
		return PlayerPrefs.GetInt(ADS_REMOVED_KEY) == 0;
	}

	void Start()
	{
		//Required
		GoogleMobileAd.Init();

		//Optional, add data for better ad targeting
		GoogleMobileAd.SetGender(GoogleGender.Male);
		GoogleMobileAd.AddKeyword("game");
		GoogleMobileAd.SetBirthday(1989, AndroidMonth.MARCH, 18);
		GoogleMobileAd.TagForChildDirectedTreatment(false);

//		Causes a device to receive test ads. The deviceId can be obtained by viewing the device log output after creating a new ad
//		Fill your test device in the plugin setting, or you can add your device using example code bellow

		GoogleMobileAd.AddTestDevice("733770c317dcbf4675fe870d3df9ca42");

//		listening for InApp Event
//		You will only receive in-app purchase (IAP) ads if you specifically configure an IAP ad campaign in the AdMob front end.
		GoogleMobileAd.OnAdInAppRequest +=  OnInAppRequest;	

		if (GoogleMobileAd.IsInited) {
			GoogleMobileAd.LoadInterstitialAd ();
			GoogleMobileAd.LoadRewardedVideo ();
		}
	}

	void Update()
	{
	}

	public void ShowInterstitial() {
		if (GoogleMobileAd.IsInterstitialReady) {
			GoogleMobileAd.ShowInterstitialAd ();
		}
		GoogleMobileAd.LoadInterstitialAd ();
	}

	public void ShowVideo() {
		if (GoogleMobileAd.IsRewardedVideoReady) {
			GoogleMobileAd.ShowRewardedVideo ();
		}
		GoogleMobileAd.LoadRewardedVideo ();
	}

	public void LoadVideo()
	{
		GoogleMobileAd.LoadRewardedVideo ();
	}

	public void LoadInterstitial()
	{
		GoogleMobileAd.LoadInterstitialAd ();
	}

	private void OnInAppRequest(string productId) 
	{
		Debug.Log ("In App Request for product Id: " + productId + " received");

		GoogleMobileAd.RecordInAppResolution(GADInAppResolution.RESOLUTION_SUCCESS);
	}
}
