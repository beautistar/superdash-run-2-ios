using UnityEngine;
using System.Collections;

/*
 * Power ups are used to give the player an extra super ability
 */
[RequireComponent(typeof(BoxCollider))]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(AppearanceProbability))]
[RequireComponent(typeof(PowerUpAppearanceRules))]
public class PowerUpObject : CollidableObject
{
	public PowerUpTypes powerUpType;
	
	private GameManager gameManager;
	private DataManager dataManager;
	private AudioManager audioManager;
	private int playerLayer;

    public override void init()
    {
        base.init();
        objectType = ObjectType.PowerUp;
        
    }
	
	public override void Awake()
	{
		base.Awake();
		playerLayer = LayerMask.NameToLayer("Player");
	}
	
	public void Start()
	{
        gameManager = GameManager.instance;
	}
	
	void OnTriggerEnter(Collider other)
    {
		if (other.gameObject.layer == playerLayer) 
		{
			if(PlayerController.instance.IsSaveMeOnGetter())
			{
				return;
			}
			if(powerUpType != PowerUpTypes.SecrecyBox)
			{
	            gameManager.activatePowerUp(powerUpType, true);
	            dataManager = DataManager.instance;
	            dataManager.SetLevelPowerUpCount(powerUpType);
				deactivate();
			}
			else
			{
				PlayerController.instance.PlaySecrecyBoxParticleSystem();
				dataManager = DataManager.instance;
	            dataManager.SetLevelPowerUpCount(powerUpType);
	            audioManager = AudioManager.instance;
	            audioManager.playSoundEffect(SoundEffects.PowerUpSoundEffect);
	            deactivate();
			}
		}
	}
}
