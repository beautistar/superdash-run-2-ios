using UnityEngine;
using System.Collections;

/*
 * The player can only run into so many obstacle objects before it is game over.
 */
[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(AppearanceProbability))]
[RequireComponent(typeof(CollidableAppearanceRules))]
public class ObstacleObject : CollidableObject
{
    // Used for the player death animation. On a jump the player will flip over, while a duck the player will fall backwards
    public bool isJump;

    // True if the player is allowed to run on top of the obstacle. If the player hits the obstacle head on it will still hurt him.
    public bool canRunOnTop;

    // True if the object can be destroyed through an attack. The method obstacleAttacked will be called to play the handle the destruction
    public bool isDestructible;

    public ParticleSystem destructionParticles;
    public Animation destructionAnimation;
    public string destructionAnimationName = "Destruction";

    protected bool collideWithPlayer;
	protected int playerLayer;
    private WaitForSeconds destructionDelay;

    private GameManager gameManager;
    private BoxCollider boxCollider;
    private PlayerAnimation playerAnimation;
    private PlayerController playerController;
    private bool myselfCollision;
    //private Color myColor;
    public override void init()
    {
        base.init();
        objectType = ObjectType.Obstacle;
    }

    /*void Update () {
            if(PowerUpManager.instance != null)
            {
                if(gameObject.renderer.material.color != null)
                {
                if(PowerUpManager.instance.isPowerUpActive(PowerUpTypes.Invincibility) || PowerUpManager.instance.isPowerUpActive(PowerUpTypes.BoltPack) || PowerUpManager.instance.isPowerUpActive(PowerUpTypes.SpeedIncrease)){
                    gameObject.renderer.material.color =  Color.Lerp(Color.white, Color.black, Time.time);
                }
                else
                {
                    gameObject.renderer.material.color = myColor;
                }
                }
            }
    }*/
	
	public override void Awake()
	{
        /*if(gameObject.renderer.material.color != null)
            myColor = gameObject.renderer.material.color;*/
		base.Awake();
        playerLayer = LayerMask.NameToLayer("Player");
        myselfCollision = false;
	}
	
	public virtual void Start()
	{
        playerController = PlayerController.instance;
        gameManager = GameManager.instance;
        boxCollider = GetComponent<BoxCollider>();
        playerAnimation = PlayerController.instance.GetComponent<PlayerAnimation>();

        if (destructionAnimation && isDestructible) {
            destructionAnimation[destructionAnimationName].wrapMode = WrapMode.Once;
            destructionDelay = new WaitForSeconds(0.2f);
        }

        collideWithPlayer = true;
	}



    public virtual void OnTriggerEnter(Collider other) 
	{
        if (other.gameObject.layer == playerLayer && collideWithPlayer) {

			//-------------------------------------------------------------------------------------------------------------------------------------------------------------------//
			if((PowerUpManager.instance.getActivePowerUps ()[0] == PowerUpTypes.Invincibility || PowerUpManager.instance.getActivePowerUps ()[1] == PowerUpTypes.Invincibility) && !PowerUpManager.instance.GetIsActiveFromRestart1 ()) {
				playerController.OnCollideWhileScooterPowerUP (this.gameObject.transform);
				return;
			}
			//-------------------------------------------------------------------------------------------------------------------------------------------------------------------//

            bool collide = true;
            if (Vector3.Dot(Vector3.up, ((other.transform.position)  - (thisTransform.position ))) > 0) {
                isJump = true;
            }
            else
            {
                isJump = false;
            }
            if (canRunOnTop) {
				if (Vector3.Dot(Vector3.up, ((other.transform.position)  - (thisTransform.position + (gameObject.GetComponent<BoxCollider>().size)/2))) > 0) {
                    collide = false;
                    boxCollider.isTrigger = false;
					playerAnimation = PlayerController.instance.GetComponent<PlayerAnimation>();
                    playerAnimation.run();
                }
            }

           /*if(GetComponentInChildren<Left>().MySelfTriggeringGetter() != 0)
            {
                playerController.PlayHeadTurnAnimation();
                playerController.changeSlots(false);
                playerController.MaxCollisionSetter(2);
                GetComponentInChildren<Left>().MySelfTriggeringSetter();
                print("Here");
            }
            else if(GetComponentInChildren<Right>().MySelfTriggeringGetter() != 0)
            {
                playerController.PlayHeadTurnAnimation();
                playerController.changeSlots(true);
                playerController.MaxCollisionSetter(2);
                GetComponentInChildren<Right>().MySelfTriggeringSetter();
                print("Here");
            }
            else if(GetComponentInChildren<Back>().MySelfTriggeringGetter() == 1)
            {
                playerController.MaxCollisionSetter(1);
                GetComponentInChildren<Back>().MySelfTriggeringSetter();
                print("Here");
            }
            int temp = playerController.gameObject.GetComponent<MyDirection>().GetMyDirection();
            print("temp--"+temp);
            if(temp == 1)
            {
                Vector3 temp1 = new Vector3(thisTransform.position.x,thisTransform.position.y,thisTransform.position.z);
                Vector3 temp2 = temp1 - (gameObject.GetComponent<BoxCollider>().size)/2;
                Vector3 temp3 = temp1 + (gameObject.GetComponent<BoxCollider>().size)/2;
                //temp1 = 
                if (Vector3.Dot(Vector3.left, ((other.transform.position)  - temp2)) > 0) {
                    playerController.PlayHeadTurnAnimation();
                    playerController.changeSlots(false);
                    playerController.MaxCollisionSetter(2);
                }else if(Vector3.Dot(Vector3.right, ((other.transform.position)  - temp3)) > 0) {
                        playerController.PlayHeadTurnAnimation();
                        playerController.changeSlots(true);
                        playerController.MaxCollisionSetter(2);
                        //playerController.PlayHeadTurnAnimation();
                }
                else {//if (Vector3.Dot(Vector3.back, ((other.transform.position)  - (thisTransform.position - (gameObject.GetComponent<BoxCollider>().size)/2))) >= -0.1) {
                    playerController.MaxCollisionSetter(1);
                }
            }
            if(temp == 2)
            {
                Vector3 temp1 = new Vector3(thisTransform.position.x,thisTransform.position.y,thisTransform.position.z);
                Vector3 temp2 = temp1 - (gameObject.GetComponent<BoxCollider>().size)/2;
                Vector3 temp3 = temp1 + (gameObject.GetComponent<BoxCollider>().size)/2;
                if (Vector3.Dot(Vector3.right, ((other.transform.position)  - temp2)) > 0) {
                    playerController.PlayHeadTurnAnimation();
                    playerController.changeSlots(true);
                    playerController.MaxCollisionSetter(2);
                }else if(Vector3.Dot(Vector3.left, ((other.transform.position)  - temp3)) > 0) {
                        playerController.PlayHeadTurnAnimation();
                        playerController.changeSlots(false);
                        playerController.MaxCollisionSetter(2);
                        //playerController.PlayHeadTurnAnimation();
                }
                else {//if (Vector3.Dot(Vector3.back, ((other.transform.position)  - (thisTransform.position - (gameObject.GetComponent<BoxCollider>().size)/2))) >= -0.1) {
                    playerController.MaxCollisionSetter(1);
                }
            }
            if(temp == 3)
            {
                Vector3 temp1 = new Vector3(thisTransform.position.z,thisTransform.position.y,thisTransform.position.x);
                Vector3 temp2 = temp1 - (gameObject.GetComponent<BoxCollider>().size)/2;
                Vector3 temp3 = temp1 + (gameObject.GetComponent<BoxCollider>().size)/2;
                Vector3 temp4 = new Vector3(other.transform.position.z,other.transform.position.y,other.transform.position.x);
                if (Vector3.Dot(Vector3.left, (temp4  - temp2)) > 0) {
                    playerController.PlayHeadTurnAnimation();
                    playerController.changeSlots(true);
                    playerController.MaxCollisionSetter(2);
                }else if(Vector3.Dot(Vector3.right, (temp4  - temp3)) > 0) {
                        playerController.PlayHeadTurnAnimation();
                        playerController.changeSlots(false);
                        playerController.MaxCollisionSetter(2);
                        //playerController.PlayHeadTurnAnimation();
                }
                else {//if (Vector3.Dot(Vector3.back, ((other.transform.position)  - (thisTransform.position - (gameObject.GetComponent<BoxCollider>().size)/2))) >= -0.1) {
                    playerController.MaxCollisionSetter(1);
                }
            }
            if(temp == 4)
            {
                Vector3 temp1 = new Vector3(thisTransform.position.z,thisTransform.position.y,thisTransform.position.x);
                Vector3 temp2 = temp1 - (gameObject.GetComponent<BoxCollider>().size)/2;
                Vector3 temp3 = temp1 + (gameObject.GetComponent<BoxCollider>().size)/2;
                Vector3 temp4 = new Vector3(other.transform.position.z,other.transform.position.y,other.transform.position.x);
                if (Vector3.Dot(Vector3.left, (temp4  - temp2)) > 0) {
                    playerController.PlayHeadTurnAnimation();
                    playerController.changeSlots(false);
                    playerController.MaxCollisionSetter(2);
                }else if(Vector3.Dot(Vector3.right, (temp4  - temp3)) > 0) {
                        playerController.PlayHeadTurnAnimation();
                        playerController.changeSlots(true);
                        playerController.MaxCollisionSetter(2);
                        //playerController.PlayHeadTurnAnimation();
                }
                else {//if (Vector3.Dot(Vector3.back, ((other.transform.position)  - (thisTransform.position - (gameObject.GetComponent<BoxCollider>().size)/2))) >= -0.1) {
                    playerController.MaxCollisionSetter(1);
                }
            }
            //else 


            if (collide) {
                gameManager.obstacleCollision(this, other.ClosestPointOnBounds(thisTransform.position));
            }
            playerController.MaxCollisionSetter(1);*/
		}
	}

    public void AfterCollision(int temp,Collider other)
    {
		Debug.LogError("After collision: " + temp);
        if(!myselfCollision)
        {
            myselfCollision = true;
            if(temp == 2)
            {
                playerController.PlayHeadTurnAnimation();
                playerController.changeSlotsHeadTurn(false);
                playerController.MaxCollisionSetter(2);
            }
            else if(temp == 3)
            {
                playerController.PlayHeadTurnAnimation();
                playerController.changeSlotsHeadTurn(true);
                playerController.MaxCollisionSetter(2);
            }
            else if(temp == 1)
            {
                playerController.MaxCollisionSetter(1);
            }

            //if (collide) {
                    gameManager.obstacleCollision(this, other.ClosestPointOnBounds(thisTransform.position));
            //    }
             playerController.MaxCollisionSetter(1);
         }
         StartCoroutine(MySelfCollisionFalse());
        // myselfCollision = false;   
    }

    private IEnumerator MySelfCollisionFalse()
    {
        yield return new WaitForSeconds(0.1f);
        myselfCollision = false;
    }

    public void obstacleAttacked()
    {
        collideWithPlayer = false;

        if (destructionAnimation) {
            StartCoroutine(playDestructionAnimation());
        }
    }

    private IEnumerator playDestructionAnimation()
    {
        yield return destructionDelay;
        destructionAnimation.Play();
        destructionParticles.Play();
    }

    public override void collidableDeactivation()
    {
		Debug.Log ("3");
        base.collidableDeactivation();

        // reset
        collideWithPlayer = true;
        if (canRunOnTop) {
            boxCollider.isTrigger = true;
        }
        if (destructionAnimation) {
            destructionAnimation.Rewind();
            destructionAnimation.Play();
            destructionAnimation.Sample();
            destructionAnimation.Stop();

            if (destructionParticles)
                destructionParticles.Clear();
        }
    }
}
