using UnityEngine;
using System.Collections;

public class MovingObstacleTrigger : MonoBehaviour
{
    public MovingObstacleObject parent;

    public void OnTriggerEnter(Collider other)
    {
		print("Entered--"+other.gameObject.name);
    	if((other.gameObject.tag).Equals("Player"))
    		print("Entered--"+other.gameObject.name);
        parent.OnTriggerEnter(other);
    }
}
