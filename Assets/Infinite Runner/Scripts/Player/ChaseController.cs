using UnityEngine;
using System.Collections;
/*
 * The chase controller manages the enemy chase object. The chase object will approach the character will the player is about to
 * collide with too many obstacles, and will then attack when the player hits the final obstacle
 */
public class ChaseController : MonoBehaviour
{
    // the position/rotation of the chase object at the start of the game
    public Vector3 spawnPosition;
    public Vector3 spawnRotation;
    public GameObject myChildren;
    // the local distance offset from the player when the chase object is outside of view
    public Vector3 hiddenDistance;
    // the local distance offset from the player when the chase object is in view (creeping on the player)
    public Vector3 visibleDistance;

    // the move/rotation speed of the chase object when it is changing positions/rotations
    public float moveSpeed;
    public float rotateSpeed;
    // length of time that the chase object should appear in view at the start of the game
    public float previewDuration;
    // length of time that the chase object should appear in view after the player hit an obstacle
    public float visibleDuration;

    public ParticleSystem attackParticles;

    public string idleAnimationName = "Idle";
    public string runAnimationName = "Run";
    public string attackAnimationName = "RunCatch";

    private float startTime;
    private float approachTime;
    private float pauseTime;
    private int platformLayer;
    private bool isSaveMeOn;

    private PlayerController playerController;
    private Transform thisTransform;
    private Animation thisAnimation;
    private DataManager dataManager;
	private bool isInZipLining;
	private bool isLifeSaving;
	public void Start()
    {
		isInZipLining = false;
		isLifeSaving = false;
        isSaveMeOn = false;
        myChildren.SetActive(true);
        thisTransform = transform;
        thisAnimation = GetComponent<Animation>();
        
        thisTransform.position = spawnPosition;
        thisTransform.eulerAngles = spawnRotation;
        platformLayer = (1 << LayerMask.NameToLayer("Platform")) | (1 << LayerMask.NameToLayer("PlatformJump")) | (1 << LayerMask.NameToLayer("Floor"));

        // setup the animation wrap modes
        thisAnimation[idleAnimationName].wrapMode = WrapMode.Loop;
        thisAnimation[runAnimationName].wrapMode = WrapMode.Loop;
        thisAnimation[attackAnimationName].wrapMode = WrapMode.Once;
        thisAnimation.Play(idleAnimationName);

        enabled = false;
	}

    public void startGame()
    {
		isInZipLining = false;
		isLifeSaving = false;
        isSaveMeOn = false;
        myChildren.SetActive(true);
        playerController = PlayerController.instance;
        thisTransform.parent = playerController.transform;
        startTime = Time.time;
        approachTime = -visibleDuration;
        thisAnimation.Play(runAnimationName);
        enabled = true;
        GameManager.instance.onPauseGame += gamePaused;	
    }

    public void reset()
    {
		isInZipLining = false;
		isLifeSaving = false;
        isSaveMeOn = false;
        myChildren.SetActive(true);
        thisTransform.position = spawnPosition;
        thisTransform.eulerAngles = spawnRotation;
        attackParticles.Stop();
        attackParticles.Clear();
        thisAnimation.Play(idleAnimationName);

        enabled = false;
        GameManager.instance.onPauseGame -= gamePaused;	
    }

    public void Update()
    {
		if (PowerUpManager.instance.isPowerUpActive (PowerUpTypes.Invincibility) || PowerUpManager.instance.isPowerUpActive (PowerUpTypes.SpeedIncrease) ||isInZipLining || isLifeSaving) 
		{
			MyChildSetter (false);
		} 
		else 
		{
			MyChildSetter (true);
		}
        // at the start of the game move within the camera view so the player knows that they are being chased. Move to this same spot
        // if the player has hit too many obstacles. Also, don't move within the camera view if the player is on a sloped platform
        // since the chase object can obstruct the camera view
        if (!playerController.abovePlatform(false) && ((Time.time < approachTime + visibleDuration) || (Time.time < startTime + previewDuration) || (playerController.MaxCollisionGetter() != 0 && approachTime > 0))) {
            // move the x axis first, followed by y/z
            if (thisTransform.localPosition.x != visibleDistance.x || thisTransform.localPosition.z != visibleDistance.z) {
                Vector3 targetPosition = thisTransform.localPosition;
                if (thisTransform.localPosition.x != visibleDistance.x) {
                    targetPosition.x = Mathf.MoveTowards(thisTransform.localPosition.x, visibleDistance.x, moveSpeed);
                } else if (thisTransform.localPosition.z != visibleDistance.z) {
                    targetPosition.z = Mathf.MoveTowards(thisTransform.localPosition.z, visibleDistance.z, moveSpeed);
                }
                thisTransform.localPosition = targetPosition;
            }
            // keep the chase character on the ground if the player is in the air
            if (playerController.inAir()) {
                // adjust the vertical position for any height changes
                RaycastHit hit;
                if (Physics.Raycast(thisTransform.position + Vector3.up, -thisTransform.up, out hit, Mathf.Infinity, platformLayer)) {
                    Vector3 targetPosition = thisTransform.position;
                    targetPosition.y = hit.point.y + visibleDistance.y;
                    thisTransform.position = targetPosition;
                }
            }
            if(!(Time.time < approachTime + visibleDuration))
            {
                approachTime = -1;
                dataManager = DataManager.instance;
                dataManager.reSetCollisionCount(0);   
            }
        } else {
            // stay hidden for now
            if (thisTransform.localPosition != hiddenDistance) {
                thisTransform.localPosition = Vector3.MoveTowards(thisTransform.localPosition, hiddenDistance, moveSpeed);
            }
        }

        if (thisTransform.localRotation != Quaternion.identity) {
            thisTransform.localRotation = Quaternion.RotateTowards(thisTransform.localRotation, Quaternion.identity, rotateSpeed);
        }
    }

    public void approach()
    {
        approachTime = Time.time;
    }

    public bool isVisible()
    {
        return Time.time < approachTime + visibleDuration;
    }

    public void gameOver(GameOverType gameOverType)
    {
        // attack
        if (gameOverType != GameOverType.Pit && !isSaveMeOn) {
            if (thisTransform.localPosition.x != visibleDistance.x || thisTransform.localPosition.z != visibleDistance.z) {
                if (thisTransform.localPosition.x != visibleDistance.x) {
                    thisTransform.localPosition = new Vector3(visibleDistance.x,thisTransform.localPosition.y,thisTransform.localPosition.z);
                    //targetPosition.x = Mathf.MoveTowards(thisTransform.localPosition.x, visibleDistance.x, moveSpeed);
                } else if (thisTransform.localPosition.z != visibleDistance.z) {
                    thisTransform.localPosition = new Vector3(thisTransform.localPosition.x,thisTransform.localPosition.y,visibleDistance.z);
                    //targetPosition.z = Mathf.MoveTowards(thisTransform.localPosition.z, visibleDistance.z, moveSpeed);
                }
            }
            //myChildren.SetActive(false);
            thisAnimation.Play(attackAnimationName);
            isSaveMeOn = true;
            //attackParticles.Play();
        }/* else {
            thisAnimation.Stop();
        }*/
        enabled = false;
    }

    public void gamePaused(bool paused)
    {
        //thisAnimation.enabled = !paused;
        if (paused) {
            pauseTime = Time.time;
            enabled = false;
        } else {
            //if(isSaveMeOn == false )
                thisAnimation.Play(runAnimationName);
            /*if(isSaveMeOn)
                StartCoroutine("WaitForSomeTime");
            else
                DontWaitForTime();*/
            isSaveMeOn = false;
            startTime += (Time.time - pauseTime);
            approachTime += (Time.time - pauseTime);
            enabled = true;
        }
    }

	public void MyChildSetter(bool temp)
	{
		myChildren.SetActive (temp);
	}

	public void IsInZipLiningSetter(bool temp)
	{
		isInZipLining = temp;
	}

	public void IsLifeSavingSetter(bool temp)
	{
		isLifeSaving = temp;
		MyChildSetter (false);
		StartCoroutine (IsLifeSavingCoroutine ());
	}

	IEnumerator IsLifeSavingCoroutine()
	{
		yield return new WaitForSeconds (3f);
		isLifeSaving = false;
	}

    /*private IEnumerator WaitForSomeTime()
    {
            yield return new WaitForSeconds(3f);
            isSaveMeOn = false;
            startTime += (Time.time - pauseTime);
            approachTime += (Time.time - pauseTime);
            enabled = true;
    }
    
    private void DontWaitForTime()
    {
        isSaveMeOn = false;
            startTime += (Time.time - pauseTime);
            approachTime += (Time.time - pauseTime);
            enabled = true;
    }*/
}
