using UnityEngine;
using System.Collections;

// The player feel into a bad area. Die.
[RequireComponent(typeof(BoxCollider))]
[RequireComponent(typeof(Rigidbody))]
public class DeathTrigger : MonoBehaviour
{
	BoxCollider boxColli;
	private PowerUpManager powerUpManager;
	
	void Start () 
	{
		powerUpManager = PowerUpManager.instance;
		boxColli = GetComponent<BoxCollider>();
	}

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Player")) {
        	if(powerUpManager.isPowerUpActive(PowerUpTypes.SpeedIncrease) || powerUpManager.isPowerUpActive(PowerUpTypes.Invincibility) || powerUpManager.isPowerUpActive(PowerUpTypes.BoltPack) || powerUpManager.isPowerUpActive1(PowerUpTypes.SpeedIncrease) || powerUpManager.isPowerUpActive1(PowerUpTypes.Invincibility) || powerUpManager.isPowerUpActive1(PowerUpTypes.BoltPack))
            {
				if(gameObject.tag.Equals("PitInZipLining"))
				{
					if(powerUpManager.isPowerUpActive1(PowerUpTypes.SpeedIncrease) || powerUpManager.isPowerUpActive1(PowerUpTypes.Invincibility))
						powerUpManager.deactivatePowerUp1();
					else
						powerUpManager.deactivatePowerUp();
					boxColli.isTrigger = true;
					if(PlayerController.instance != null)
					{
						if(PlayerController.instance.IsSaveMeOnGetter())
							return;
						if(PlayerController.instance.IsZipLining())
							return;
						PlayerController.instance.OnLifeSaverAsking(GameOverType.Pit, true);
					}
				}
				else
				{
	               	boxColli.isTrigger = false;
	               	StartCoroutine(ToMakeTrigger());
	               	return;
				}
            }
            else
            {
            	boxColli.isTrigger = true;
				if(PlayerController.instance != null)
				{
					if(PlayerController.instance.IsSaveMeOnGetter())
						return;
					PlayerController.instance.OnLifeSaverAsking(GameOverType.Pit, true);
				}
            }
        }
    }

    IEnumerator ToMakeTrigger() {
        yield return new WaitForSeconds(0.5f);
        boxColli.isTrigger = true;
    }
}
