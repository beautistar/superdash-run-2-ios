﻿using UnityEngine;
using System.Collections;
using System;

public class MyGUIManager : MonoBehaviour 
{
	static public MyGUIManager instance;
	public static event Action GameStartedEvent;
	public static event Action GameEndEvent;
	public static event Action RestartGameEvent;

	public delegate void SaveMeScreenDelegate(bool response);
	public GameObject endGameScreen;
	public GameObject playScreen;
	public GameObject mainMenuScreen;
	public GameObject uiCamera;
	public GameObject shortVideo;
	private Transform shortVideoTransform;
	public GameObject shortVideo1;
	public GameObject inGameScreen;
	public GameObject pause;
	public GameObject SaveMe;
	public GameObject BG;
	public GameObject LoadingScreen ;
	public bool StartBGMusic = false;
	public tk2dTextMesh PlayScreenHighScore ;
	public static bool GameStarted = false;
	private bool isSaved;

	bool isFirstTime = false;

	public int PlayCount  = 0;

	public void Awake(){
		instance = this;	
		BG.SetActive (true);
		PlayerPrefs.SetInt ("RemindDays",PlayerPrefs.GetInt ("RemindDays", 0) + 1 );
		LoadingScreen.SetActive (true);
		LoadingManager.Instance.LoadFiles ();
	}

	void OnEnable() {
		LoadingManager.OnLoadComplete += HandleOnLoadComplete;
	}

	void HandleOnLoadComplete () {
		BG.SetActive (false);
		SoundManager.Instance.PlayThemeMusic ();
		StartCoroutine ("StartOutrun");
	}

	bool OnPlayScreen = false;

	void Update() {
		if (Input.GetKey (KeyCode.Escape) && OnPlayScreen) {
			Application.Quit ();
		}

		if (Input.GetKey (KeyCode.Escape) && GameManager.instance.IsGameRunnnig) {
			GameManager.instance.pauseGame(true);
		}

	}

	IEnumerator StartOutrun(){

		OnPlayScreen = false;
		if (SettingStaticData.Instance.getTutorial ()) {
			//Debug.LogError("TUTORIAL");
			BG.SetActive (false);
			PlayTutorial ();
			yield return new WaitForSeconds (0);
			Debug.Log ("StartOutrun");
			SetIntroVideo (false);
			isFirstTime = true;
		    GameManager.instance.startGame (false);
		} else {
			ShowPlayScreen ();
			PopUpManager.Instance.InitPopUps ();
			playScreenManager.Instance.PlayLogoAnimation ();
		}
	}
	
	public void SetIntroVideo(bool temp){
		if(!temp)
		{
			StartCoroutine("ResetIntroVideo");
		}
		else
		{
			shortVideo.SetActive(true);
			shortVideo.GetComponent<IntroVideo>().OnAwake();
		}
	}

	IEnumerator ResetIntroVideo()
	{
		OnPlayScreen = false;
		yield return new WaitForSeconds(0.8f);
		shortVideo.GetComponent<IntroVideo>().OnSleep();
		shortVideo.SetActive (false);
	}

	void PlayTutorial(){
		OnPlayScreen = false;
		PlayVideoManager.Instance.PlayVideo ();
	}

	public void StartGame(){
		OnPlayScreen = false;
		StartBGMusic = true;

		PlayCount++;
		pause.SetActive (false);
		playScreen.SetActive (false);
		mainMenuScreen.SetActive(false);
		endGameScreen.SetActive(false);
		inGameScreen.SetActive(true);
		GameStarted = true;
		inGameScreen.GetComponent<InGameUIManager> ().StartGame ();
		//SoundManager.Instance.PlayThemeMusic ();
		UIGameManager.Instance.HideWall ();
		UIGameManager.Instance.HideMenuWall ();
		GameStartedEvent ();
	}

	public IEnumerator SaveMeScreen(SaveMeScreenDelegate callback){
		OnPlayScreen = false;
		SaveMe.SetActive(true);
		yield return new WaitForSeconds(1.0f);
		SaveMe.SetActive(false);
		if(isSaved)
			callback(true);
		else 
			callback(false);
		isSaved = false;
	}

	
	public void EndGame(){
		OnPlayScreen = false;
		//GameEndEvent ();
		pause.SetActive (false);
		inGameScreen.SetActive(false);
		uiCamera.SetActive(true);
		endGameScreen.SetActive(true);
		endGameScreen.GetComponent<GameOverManager>().SetScoreCoin();
		GameStarted = false;
		//SoundManager.Instance.EndThemeMusic ();
		//Debug.Log (PlayCount);

//		if (PlayCount > 3) {
//			AdsControl.instance.ShowInterstitial ();
//			PlayCount = 0;
//		}
//		OnPlayScreen = false;
		AdsControl.instance.ShowInterstitial ();
	}

	public void QuitGame(){

		OnPlayScreen = false;
		pause.SetActive (false);
		inGameScreen.SetActive(false);
		uiCamera.SetActive(true);
		endGameScreen.SetActive(false);
		//endGameScreen.GetComponent<GameOverManager>().SetScoreCoin();
		mainMenuScreen.SetActive(true);
		SetIntroVideo(true);
		GameStarted = false;
		//SoundManager.Instance.EndThemeMusic ();
		UIGameManager.Instance.ShowMenuWall ();
	}

	public void RestartGame(){
		OnPlayScreen = false;
		pause.SetActive (false);
		inGameScreen.SetActive(true);
		uiCamera.SetActive(true);
		endGameScreen.SetActive(false);
		InGameUIManager.Instance.SetCoin (0);
		GameStarted = true;

		RestartGameEvent ();
	}

	public void EndToHomeScreen(){
		OnPlayScreen = false;

		pause.SetActive (false);
		mainMenuScreen.SetActive(true);
		endGameScreen.SetActive(false);
		GameStarted = false;
		//SoundManager.Instance.EndThemeMusic ();
		SetIntroVideo(true);
		UIGameManager.Instance.ShowMenuWall ();
	}
	
	public void ShowPlayScreen() {
		UIGameManager.Instance.HideWall ();
		StartBGMusic = true;

		if (isFirstTime) 
		{
			playScreen.SetActive (true);
			playScreenManager.Instance.PlayLogoAnimation ();
			isFirstTime = false;
		}

		PlayScreenHighScore.text = "High Score " +  DataManager.instance.getHighScore ().ToString ();
		BG.SetActive (false);
		pause.SetActive (false);
		playScreen.SetActive (true);
		mainMenuScreen.SetActive(false);
		endGameScreen.SetActive(false);
		uiCamera.SetActive(true);
		inGameScreen.SetActive(false);
		shortVideo.SetActive(true);
		GameStarted = false;
		OnPlayScreen = true;
		UIGameManager.Instance.HideMenuWall ();

		if (!firstShow) {

			AdsControl.instance.ShowInterstitial ();

			firstShow = false;
		}
	}

	private static bool firstShow = true;

	public void ShowMenuScreen(){
		OnPlayScreen = false;
		StartBGMusic = true;
		pause.SetActive (false);

		playScreen.SetActive (false);
		mainMenuScreen.SetActive(true);
		endGameScreen.SetActive(false);
		uiCamera.SetActive(true);
		inGameScreen.SetActive(false);
		GameStarted = false;
		OnPlayScreen = false;
		UIGameManager.Instance.ShowMenuWall ();

	}

	public void PauseScreen() {
		GameManager.instance.IsGameRunnnig = false;
		OnPlayScreen = false;
		TutorialManager.Instance.gameObject.SetActive (false);
		//SaveMe.SetActive (false);
		inGameScreen.SetActive(false);
		//uiCamera.SetActive(true);
		pause.GetComponent<GameOverManager>().SetScoreCoin();
		pause.SetActive (true);
		OnPlayScreen = false;
	}

	public void ResumeScreen() {
		OnPlayScreen = false;
		if(!TutorialManager.Instance.IsStopAnimation)
			TutorialManager.Instance.gameObject.SetActive (true);
		pause.SetActive (false);
		inGameScreen.SetActive(true);
		InGameUIManager.Instance.ResumeGame ();
		OnPlayScreen = false;
		//uiCamera.SetActive(true);
		//pause.GetComponent<GameOverManager>().SetScoreCoin();
	}

	void SetScreens(object[] o){
		OnPlayScreen = false;
		playScreen.SetActive (false);
		mainMenuScreen.SetActive(true);
		endGameScreen.SetActive(false);
		uiCamera.SetActive(true);
		inGameScreen.SetActive(false);
		pause.SetActive (true);	
	}

	public void IsSavedSetter(bool temp){
		isSaved = temp;
	}

	void HandleDismissedOverlay () {
		StopCoroutine ("RemoveADs");
		IsAdsOn = false;
		UIGameManager.Instance.HideWall ();
	}
	bool IsAdsOn = false;
	
	void HandleShowingOverlay () {
		IsAdsOn = true;
		StartCoroutine ("RemoveADs");
		UIGameManager.Instance.HideWall ();
	}
	
	IEnumerator RemoveADs() {
		yield return new WaitForSeconds(15);
		if (IsAdsOn) {
//			AdsControl.instance.RemoveAds();
		}
		IsAdsOn = false;
	}
}
