using UnityEngine;
using System.Collections;

/*
 * The data manager is a singleton which manages the data across games. It will persist any permanent data such as the
 * total number of coins or power up level
 */
public class DataManager : MonoBehaviour {
	
	static public DataManager instance;
	public bool Clear = false;
	public float scoreMult;
	
	private float score;
	private int totalCoins;
	private int levelCoins;
    private int levelLifeSavers;
	private int collisions;
    private int levelDistance;
    private int levelDistanceWithoutTrip;
    private int collectingNoCoinsDistance;
    private int levelSuperPogoStick;
    private int levelBoltPacks;
    private int levelMagnets;
    public int levelSixMultiplier;
	private int levelScooterCruiser;
    private int levelSecrecyBox;
    private int levelBlastOff;
    private int levelJumps;
    private int levelLifeSaversUsed;
	private int LevelDoubleCoins;


    private int TotalCoinsLifeTime;

	private int[] currentPowerupLevel;
	
	private GUIManager guiManager;
    private SocialManager socialManager;
    private MissionManager missionManager;
	private StaticData staticData;

	public int SingleUse = 4;
	public bool GiveCoins = false;
	public bool GiveLifeSavers = false;

	public void Awake() {
		instance = this;
		if (Clear)
			PlayerPrefs.DeleteAll ();

		if(GiveCoins)
			adjustTotalCoins (1000000);

		if(GiveLifeSavers)
			setTotalLifeSavers (500);
	}
	
	public void Start(){	
		//guiManager = GUIManager.instance;
        socialManager = SocialManager.instance;
        missionManager = MissionManager.instance;
		staticData = StaticData.instance;
		score = 0;
		levelCoins = 0;
        levelLifeSavers = 0;
		collisions = 0;
        levelDistance = 0;
        levelDistanceWithoutTrip = 0;
        collectingNoCoinsDistance = 0;
		totalCoins = PlayerPrefs.GetInt("Coins", 0);
        levelMagnets = 0; //
        levelJumps = 0;
        levelBlastOff = 0; 
        levelBoltPacks = 0; //
        levelLifeSaversUsed = 0;
        levelSuperPogoStick = 0; // 
		levelSixMultiplier = 0;
		LevelDoubleCoins = 0; //
		//adjustTotalLifeSavers (1);
		//SetLevelPowerUpCount (PowerUpTypes.SecrecyBox);
		/*currentPowerupLevel = new int[(int)PowerUpTypes.None];
		for (int i = 0; i < (int)PowerUpTypes.None; ++i) {
            currentPowerupLevel[i] = PlayerPrefs.GetInt(string.Format("PowerUp{0}", i), 0);
            if (currentPowerupLevel[i] == 0 && GameManager.instance.enableAllPowerUps) {
                currentPowerupLevel[i] = 1;
            }
        }*/
		//ClearAllPrefabs ();
        CurrentPowerUpLevels();
        // first character is always available
        purchaseCharacter(0);
	}
	
    private void CurrentPowerUpLevels() {
        currentPowerupLevel = new int[(int)PowerUpTypes.None];
        for (int i = 0; i < (int)PowerUpTypes.None; ++i) {
            currentPowerupLevel[i] = PlayerPrefs.GetInt(string.Format("PowerUp{0}", i), 0);
			if (currentPowerupLevel[i] == 0 && GameManager.instance.enableAllPowerUps && (i<4 || i ==  7)) {
                currentPowerupLevel[i] = 1;
            }
        }
    } 

    public void ClearAllPrefabs() {   
        PlayerPrefs.SetInt("Coins", 0);

        for (int i = 0; i < getCharacterCount(); ++i) {
            PlayerPrefs.SetInt(string.Format("CharacterPurchased{0}", i), 0);
        }

        for (int i = 0; i < getPurchaseCount(); ++i) {
            PlayerPrefs.SetInt(string.Format("PermanentPurchased{0}", i), 0);
        }

        for (int i = 0; i < (int)PowerUpTypes.None; ++i) {
            PlayerPrefs.SetInt(string.Format("PowerUp{0}", i), 0);
        }

        for (int i = 0; i < getSingleUseCount(); ++i) {
            PlayerPrefs.SetInt(string.Format("TotalSingleUse{0}", i), 0);
        }

        PlayerPrefs.SetInt("Coins",100000);
    }

    public void addToScore(float amount) {
        if(levelSixMultiplier == 0)
            score += (amount * 4f * missionManager.getScoreMultiplier());
        else    
            score += ((amount * 4f  * missionManager.getScoreMultiplier() * (levelSixMultiplier*6)));
		InGameUIManager.Instance.SetScore (Mathf.RoundToInt (score));

        //guiManager.setInGameScore(Mathf.RoundToInt(score));
    }
	
	public int getScore(bool withMultiplier = true){
        return Mathf.RoundToInt(score);
        //print("Score --"+ ((Mathf.RoundToInt(score * (withMultiplier ? missionManager.getScoreMultiplier() : 1)))*(levelSixMultiplier*6)));
        //if(levelSixMultiplier == 0)
        //    return Mathf.RoundToInt(score * (withMultiplier ? missionManager.getScoreMultiplier() : 1));
        //else
        //    return (Mathf.RoundToInt(score * (withMultiplier ? missionManager.getScoreMultiplier() : 1)))*(levelSixMultiplier*6); 	
	}
	
	public void obstacleCollision(){
		collisions++;
	}
	
	public int getCollisionCount(){
		return collisions;	
	}
	
    public void reSetCollisionCount(int temp) {
        collisions = temp;  
    }

	public void addToCoins(int amount){
		levelCoins += amount;
        //print("levelCoins"+levelCoins);
		InGameUIManager.Instance.SetCoin (levelCoins);
		//guiManager.setInGameCoinCount(levelCoins);
	}

    public void addToLifeSaver(int temp)  {
		setLifeSaversFromGame (1);
    }
	
	public int getLevelCoins(){
	
		return levelCoins;	
	}

    public void adjustTotalCoins(int amount) {
        totalCoins += amount;
        PlayerPrefs.SetInt("Coins", totalCoins);
    }
	
	public int getTotalCoins(){
		return totalCoins;
	}
	
	public int getHighScore(){
		return PlayerPrefs.GetInt("HighScore", -1);
	}

    public void adjustTotalLifeSavers(int Value){
		setTotalLifeSavers (Value);
    }

	public int getPlayCount(){
		return PlayerPrefs.GetInt("PlayCount");	
	}

    public int getLongestRunFromGame() {
        return levelDistance;
    }

    public void setLongestRunFromGame(float temp){
        levelDistance = (int)temp;
        if(levelCoins == 0) 
        {
            if(levelDistance >= 300)
                collectingNoCoinsDistance = 300;
            if(levelDistance >= 500)
                collectingNoCoinsDistance = 500;
            if(levelDistance >= 1000)
                collectingNoCoinsDistance = 1000;
            //print("collectingNoCoinsDistance"+collectingNoCoinsDistance);        
        }
    }

    public void setLongestRunFromGameWithoutTrip(float temp){
        levelDistanceWithoutTrip = (int)temp;
    }

    public int getLongestRunFromGameWithoutTrip() {
        return levelDistanceWithoutTrip;
    }

    public int getLongestRunWithoutCoinFromGame(){
        return collectingNoCoinsDistance;
    }

	public bool IsLevelHighscore = false;

	public void gameOver() {
        // save the high score, coin count, and play count
        if (getScore() > getHighScore()) {
			IsLevelHighscore = true;
            PlayerPrefs.SetInt("HighScore", getScore());
            //socialManager.recordScore(getScore());
        }
		else
			IsLevelHighscore = false;

		//Debug.Log ("levelCoins "+levelCoins);
		//Debug.Log ("getScore "+getScore());
		//Debug.Log ("getLongestRunFromGame "+getLongestRunFromGame());
        //-------------------------------------------------------------------------------------

        if (getLongestRunFromGame() > getLongestRun()) {
            setLongestRun ( getLongestRunFromGame ());
            //socialManager.recordScore(getScore());
        }

        if (getLevelCoins() > getMostCoins()) {
            setMostCoins( getLevelCoins());
            //socialManager.recordScore(getScore());
        }

        if (getLifeSaversFromGame() > getMostLifeSavers()) {
            setMostLifeSavers  (getLifeSaversFromGame ());
            //socialManager.recordScore(getScore());
        }

        totalCoins += levelCoins;
        PlayerPrefs.SetInt("Coins", totalCoins);

        setTotalGame ();
        setTotalDistance (getLongestRunFromGame());
        setTotalCoinsLifetime (levelCoins);
        //setTotalLifeSavers (getLifeSaversFromGame ());

//#if !UNITY_EDITOR
        postScoreOGN();
//#endif

        //--------------------------------------------------------------------------------------
    }

    private void postScoreOGN()
    {
        //Area730.AnalyticsController.instance.postSimpleScore(getScore(), "LEVEL_SCORE");
    }


    public void reset()
	{
		score = 0;
		levelCoins = 0;
        levelLifeSavers = 0;
		collisions = 0;
        levelDistance = 0;
        levelDistanceWithoutTrip = 0;
        collectingNoCoinsDistance = 0;
        levelMagnets = 0;  //
        levelJumps = 0;
        levelBlastOff = 0; 
        levelBoltPacks = 0; //
        levelSecrecyBox = 0;
        levelLifeSaversUsed = 0;
        levelSuperPogoStick = 0;  //
        levelSixMultiplier = 0;
		LevelDoubleCoins = 0; //
		//print("collision"+collisions);
		//guiManager.setInGameScore(Mathf.RoundToInt(score));
		//guiManager.setInGameCoinCount(levelCoins);
	}

    public int getCharacterCount()
    {
        return staticData.characterCount;
    }

    public string getCharacterTitle(int character)
    {
        return staticData.getCharacterTitle(character);
    }

    public string getCharacterDescription(int character)
    {
        return staticData.getCharacterDescription(character);
    }

    public int getCharacterCost(int character)
    {
        if (PlayerPrefs.GetInt(string.Format("CharacterPurchased{0}", character), 0) == 1)
            return -1; // -1 cost if the character is already purchased
        return staticData.getCharacterCost(character);
    }

    public void purchaseCharacter(int character)
    {
        PlayerPrefs.SetInt(string.Format("CharacterPurchased{0}", character), 1);
    }

    public void setSelectedCharacter(int character)
    {
        if(getCharacterCost(character) == -1)
            purchaseCharacter(character);
        if (PlayerPrefs.GetInt(string.Format("CharacterPurchased{0}", character), 0) == 1) {
            PlayerPrefs.SetInt("SelectedCharacter", character);
        }
    }

    public int getSelectedCharacter()
    {
        return PlayerPrefs.GetInt("SelectedCharacter", 0);
    }

    public GameObject getCharacterPrefab(int character)
    {
        return staticData.getCharacterPrefab(character);
    }

    public GameObject getChaseObjectPrefab()
    {
        return staticData.getChaseObjectPrefab();
    }

    public string getPowerUpTitle(PowerUpTypes powerUpType)
    {
        return staticData.getPowerUpTitle(powerUpType);
    }

    public string getPowerUpDescription(PowerUpTypes powerUpType)
    {
        return staticData.getPowerUpDescription(powerUpType);
    }

    public GameObject getPowerUpPrefab(PowerUpTypes powerUpType)
    {
        return staticData.getPowerUpPrefab(powerUpType);
    }

    public int getPowerUpLevel(PowerUpTypes powerUpTypes)
    {
        CurrentPowerUpLevels();
        return currentPowerupLevel[(int)powerUpTypes];
    }

    public float getPowerUpLength(PowerUpTypes powerUpType)
    {
        return staticData.getPowerUpLength(powerUpType, currentPowerupLevel[(int)powerUpType]);
    }

    public int getPowerUpCost(PowerUpTypes powerUpType)
    {  
        if (currentPowerupLevel[(int)powerUpType] < staticData.getTotalPowerUpLevels()) {
            return staticData.getPowerUpCost(powerUpType, currentPowerupLevel[(int)powerUpType]);
        }
        return -1; // out of power up upgrades
    }

	public int getPowerUpCost(PowerUpTypes powerUpType, bool SingleUse)
	{  
		return staticData.getPowerUpCost(powerUpType, currentPowerupLevel[(int)powerUpType],true);
	}

	public int GetTotalPowerLevels()
	{
		return staticData.getTotalPowerUpLevels ();
	}

    public void upgradePowerUp(PowerUpTypes powerUpType)
    {
        currentPowerupLevel[(int)powerUpType]++;
        PlayerPrefs.SetInt(string.Format("PowerUp{0}", (int)powerUpType), currentPowerupLevel[(int)powerUpType]);
    }

	public void degradePowerUp(PowerUpTypes powerUpType)
	{
		currentPowerupLevel[(int)powerUpType]--;
		PlayerPrefs.SetInt(string.Format("PowerUp{0}", (int)powerUpType), currentPowerupLevel[(int)powerUpType]);
	}
    public int getMissionGoal(MissionType missionType)
    {
        return staticData.getMissionGoal(missionType);
    }

    public string getMissionDescription(MissionType missionType)
    {
        return staticData.getMissionDescription(missionType);
    }

	public string getMissionTitle(MissionType missionType)
	{
		return staticData.getMissionDescription(missionType);
	}

	public int getMissionCount()
	{
		return staticData.missionDescription.Length;
	}
    //------------------------------------------------------------------------------------------------------

    /*public int getDistanceFromGame()
    {
        return 1;
    }*/
    
    public int getLifeSaversFromGame()
    {
        return levelLifeSavers;
    }

	public void setLifeSaversFromGame(int Lifesavers)
	{
		levelLifeSavers += Lifesavers;
		adjustTotalLifeSavers (1);
	}

    public int getPurchaseCount()
    {
        return staticData.PermanentPurchasesCount;
    }
    
    public string getPurchaseTitle(int Order)
    {
        return staticData.getPurchaseTitle (Order);;
    }
    
    public string getPurchaseDescription(int Order)
    {
        return staticData.getPurchaseDescription(Order);
    }
    
    public int getPurchaseCost(int Order)
    {
        if (PlayerPrefs.GetInt(string.Format("PermanentPurchased{0}", Order), 0) == 1)
            return -1; // -1 cost if the character is already purchased
		return 0;
        //return staticData.getSingleUseCost(Order);
    }

    public void purchasePermanentPurchase(int Order)
    {
        PlayerPrefs.SetInt(string.Format("PermanentPurchased{0}", Order), 1);
    }

    public int getSingleUseCount()
    {
        return staticData.SingleUseCount;
    }
    
    public string getSingleUseTitle(int Order)
    {
        return staticData.getSingleUseTitle (Order);;
    }
    
    public string getSingleUseDescription()
    {
        return staticData.getSingleUseDescription();
    }

    public string getSingleUseFullDescription(int Order)
    {
        return staticData.getSingleUseFullDescription(Order);
    }
    
    public int getSingleUseCost(PowerUpTypes powerTypes)
    {
		return staticData.getSingleUseCost(powerTypes);
    }

    public int getTotalSingleUse(int Order)
    {
        return PlayerPrefs.GetInt(string.Format("TotalSingleUse{0}", Order) );
    }

    public void purchaseSingleUse(int Order)
    {
        int Total = getTotalSingleUse (Order);
        Total++;
        PlayerPrefs.SetInt(string.Format("TotalSingleUse{0}", Order),Total);
    }

    public int getMultiplier()
    {
		if (MissionManager.instance != null)
			return (int)MissionManager.instance.getScoreMultiplier ();
						return 1;
    }

    public void setMultiplier(int Value)
    {
        int Count = getMultiplier ();
        Count += Value;
        PlayerPrefs.SetInt("Multiplier",Count); 
    }

    public int getLongestRun()
    {
         return PlayerPrefs.GetInt("LongestRun", 0); 
    }
    public void setLongestRun(int Run)
    {
        //int Count = getLongestRun ();
        //Count += Run;
        PlayerPrefs.SetInt("LongestRun",Run); 
    }

    public int getMostCoins()
    {
         return PlayerPrefs.GetInt("MostCoins", 0); 
    }
    public void setMostCoins(int Coins)
    {
        //int Count = getMostCoins ();
        //Count += Coins;
        PlayerPrefs.SetInt("MostCoins",Coins); 
    }

    public int getMostLifeSavers()
    {
        return PlayerPrefs.GetInt("MostLifeSavers", 0); 
    }
    public void setMostLifeSavers(int LifeSavers)
    {
        //int Count = getMostLifeSavers ();
        //Count += LifeSavers;
        PlayerPrefs.SetInt("MostLifeSavers",LifeSavers); 
    }

    public int getTotalGame()
    {
        return PlayerPrefs.GetInt("TotalGame", 0); 
    }
    public void setTotalGame()
    {

        int Count = getTotalGame ();
        Count ++;
        PlayerPrefs.SetInt("TotalGame",Count); 
    }
    
    public int getTotalDistance()
    {
         return PlayerPrefs.GetInt("TotalDistance", 0); 
    }
    public void setTotalDistance(int Distance)
    {
        int Count = getTotalDistance ();
        Count += Distance;
        PlayerPrefs.SetInt("TotalDistance",Count); 
    }
    
    public int getTotalCoinsLifetime()
    {
        return PlayerPrefs.GetInt("TotalCoins", 0); 
    }
    public void setTotalCoinsLifetime(int Coins)
    {
        int Count = getTotalCoinsLifetime ();
        Count += Coins;
        PlayerPrefs.SetInt("TotalCoins",Count); 
    }

    public int getTotalLifeSavers()
    {
        return PlayerPrefs.GetInt("TotalLifeSavers", 0); 
    }
    public void setTotalLifeSavers(int LifeSavers)
    {
        int Count = getTotalLifeSavers ();
		Count += LifeSavers; 
        //print("Count"+Count);
        PlayerPrefs.SetInt("TotalLifeSavers",Count); 
    }

    public string getAppVersion()
    {

#if UNITY_ANDROID
		if(UIGameManager.Instance.IsAndroid)
			return PlayerPrefs.GetString("AppVersion", 	UIGameManager.Instance.AndroidVersion); 
		else
			return PlayerPrefs.GetString("AppVersion", UIGameManager.Instance.AmazonVersion); 
#endif
		
#if UNITY_IPHONE
		return PlayerPrefs.GetString("AppVersion", 	UIGameManager.Instance.iOSVersion); 
#endif
		return "";
    
    }
    public void setAppVersion(int Value)
    {
        PlayerPrefs.SetInt("AppVersion",Value); 
    }

    public void SetLevelPowerUpCount(PowerUpTypes temp)
    {
        if (temp == PowerUpTypes.CoinMagnet)
						levelMagnets++;
				else if (temp == PowerUpTypes.SuperPogoStick)
						levelSuperPogoStick++;
				else if (temp == PowerUpTypes.SixMultiplier)
						levelSixMultiplier++;
				else if (temp == PowerUpTypes.BoltPack)
						levelBoltPacks++;
				else if (temp == PowerUpTypes.Invincibility)
						levelScooterCruiser++;
				else if (temp == PowerUpTypes.SecrecyBox)
						levelSecrecyBox++;
				else if (temp == PowerUpTypes.SpeedIncrease)
						levelBlastOff++;
				else if (temp == PowerUpTypes.DoubleCoin)
						LevelDoubleCoins++;
    }

    public int GetLevelPowerUpCount(PowerUpTypes temp)
    {
        if (temp == PowerUpTypes.CoinMagnet)
						return levelMagnets;
				else if (temp == PowerUpTypes.SuperPogoStick)
						return levelSuperPogoStick;
				else if (temp == PowerUpTypes.SixMultiplier)
						return levelSixMultiplier;
				else if (temp == PowerUpTypes.BoltPack)	
						return levelBoltPacks;
				else if (temp == PowerUpTypes.Invincibility)	
						return levelScooterCruiser;
                else if (temp == PowerUpTypes.SecrecyBox)
                        return levelSecrecyBox;
                else if (temp == PowerUpTypes.SpeedIncrease)
                        return levelBlastOff;
				else if (temp == PowerUpTypes.DoubleCoin)
						return LevelDoubleCoins;
        return 0;        
    }

    public void SetNumberOfLifeSaverUsed()
    {
        levelLifeSaversUsed++;
    }

    public int GetNumberOfLifeSaverUsed()
    {
        return levelLifeSaversUsed;
    }

    public void SetNumberOfJumps()
    {
        levelJumps++;
    }

    public int GetNumberOfJumps()
    {
        return levelJumps;
    }

    //-----------------------------------------------------------------------------------

	public int TotalPowerInARun() {
		return( LevelDoubleCoins + levelSuperPogoStick + levelBoltPacks + levelMagnets);
	}

	public int TotalSecrecyBoxInARun() {
		return levelSecrecyBox;
	}

	public int GetTotalSaveMeUsed() {
		return 0;
	}

	public int GetBlastUsedInARun() {
		return levelBlastOff;
	}

	public void ActivateSaveMeUsedInGame() {
		PlayerPrefs.SetInt ("ActivateSaveMeUsedInGame", 1);
		PlayerPrefs.SetInt ("SaveMeUsedInGame", 0);
	}

	public void SetSaveMeUsedInGame(int Count) {
		PlayerPrefs.SetInt ("SaveMeUsedInGame", GetSaveMeUsedInGame() + Count);
	}

	public int GetSaveMeUsedInGame() {
		return PlayerPrefs.GetInt ("SaveMeUsedInGame", 0);
	}
}
