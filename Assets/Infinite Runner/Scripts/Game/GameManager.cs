using UnityEngine;
using System;
using System.Collections;

/*
 * CoroutineData is a quick class used to save off any timinig information needed when the game is paused.
 */
public class CoroutineData { 
	public float startTime; 
	public float duration; 
	public CoroutineData() { startTime = 0; duration = 0; }
	public void calcuateNewDuration() { duration -= Time.time - startTime; }
}

/*
 * The game manager is a singleton which manages the game state. It coordinates with all of the other classes to tell them
 * when to start different game states such as pausing the game or ending the game.
 */
public enum GameOverType { Wall, JumpObstacle, DuckObstacle, Pit, Quit };
public class GameManager : MonoBehaviour {
	
	static public GameManager instance;

    //public GameObject temp;
    public delegate void PlayerSpawnHandler();
    public event PlayerSpawnHandler onPlayerSpawn;
	public delegate void PauseHandler(bool paused);
	public event PauseHandler onPauseGame;
	public bool IsGameRunnnig = false;
    public bool godMode;
    public bool enableAllPowerUps;
	public bool showTutorial;
    public bool runInBackground;

    private int activeCharacter;
    private GameObject character;
    private GameObject chaseCharacter;

    private bool gamePaused;
    private bool gameActive;
	
	private InfiniteObjectGenerator infiniteObjectGenerator;
	private PlayerController playerController;
	private GUIManager guiManager;
    private MyGUIManager myGuiManager;
    private DataManager dataManager;
    private AudioManager audioManager;
    private PowerUpManager powerUpManager;
    private MissionManager missionManager;
    private InputController inputController;
    private ChaseController chaseController;
    private CameraController cameraController;
    private CoinGUICollection coinGUICollection;
    private bool isSaveMe,isGotResult;
	//private UIControl uiControl;

	public void Awake() {
		instance = this;	
	}
	
	public void Start ()
	{
        //temp.SetActive(false);
		infiniteObjectGenerator = InfiniteObjectGenerator.instance;
		guiManager = GUIManager.instance;
        myGuiManager = MyGUIManager.instance;
		dataManager = DataManager.instance;
        audioManager = AudioManager.instance;
		powerUpManager = PowerUpManager.instance;
        missionManager = MissionManager.instance;
        inputController = InputController.instance;
        cameraController = CameraController.instance;
        coinGUICollection = CoinGUICollection.instance;
        showTutorial = SettingStaticData.Instance.getTutorial();
        //uiControl = UIControl.instance;

        Application.runInBackground = runInBackground;
        activeCharacter = -1;
        spawnCharacter();
        spawnChaseObject();

		#if UNITY_ANDROID//!UNITY_EDITOR && 
        // register OGN analytics evenet
        registerLaunchEvent();
#endif
    }

    private void spawnCharacter()
    {
        //print ("activeCharacter" + activeCharacter);
        if (activeCharacter == dataManager.getSelectedCharacter()) {
            playerController.init();
            return;
        }

        if (character != null) {
            Destroy(character);
        }

        activeCharacter = dataManager.getSelectedCharacter();
        character = GameObject.Instantiate(dataManager.getCharacterPrefab(activeCharacter)) as GameObject;
        playerController = PlayerController.instance;
        playerController.init();

        if (onPlayerSpawn != null) {
            onPlayerSpawn();
        }
    }

    private void spawnChaseObject()
    {
        GameObject prefab;
        /*if(prefab != null){
            DestroyImmediate(prefab,true);
            prefab = dataManager.getChaseObjectPrefab();
            chaseController = (GameObject.Instantiate(prefab) as GameObject).GetComponent<ChaseController>();
        }*/
        if ((prefab = dataManager.getChaseObjectPrefab()) != null) {
    
            if(chaseCharacter != null){
          
                Destroy(chaseCharacter);
            }
            chaseCharacter = (GameObject.Instantiate(prefab) as GameObject);
            chaseController = chaseCharacter.GetComponent<ChaseController>();
        }
    }
	
	public void startGame(bool fromRestart)
	{
		IsGameRunnnig = true;
        showTutorial = SettingStaticData.Instance.getTutorial();
		//uiControl.SetMyState(false);
        myGuiManager.StartGame();
        gameActive = true;
        inputController.startGame();
		//guiManager.showGUI(GUIState.InGame);
        audioManager.playBackgroundMusic(true);
        cameraController.setPlayerAsParent(fromRestart);
        infiniteObjectGenerator.startGame();
		playerController.startGame();
        if (chaseController != null)
            chaseController.startGame();
        //print("mySelfStarted");  

		#if UNITY_ANDROID//!UNITY_EDITOR && 
//        	registerGameStartEvent();
		#endif
    }

    public void toggleTutorial()
    {
        showTutorial = !showTutorial;
        infiniteObjectGenerator.reset();
        if (showTutorial) {
            infiniteObjectGenerator.showStartupObjects(true);
        } else {
            // show the startup objects if there are any
            if (!infiniteObjectGenerator.showStartupObjects(false))
                infiniteObjectGenerator.spawnObjectRun(false);
        }
        infiniteObjectGenerator.readyFromReset();
    }
	
	public void obstacleCollision(ObstacleObject obstacle, Vector3 position)
	{
        if (!powerUpManager.isPowerUpActive(PowerUpTypes.Invincibility) && !powerUpManager.isPowerUpActive(PowerUpTypes.BoltPack) && !powerUpManager.isPowerUpActive(PowerUpTypes.SpeedIncrease) && !godMode && gameActive) {
            
			playerController.obstacleCollision(obstacle.getTransform(), position);

			dataManager.obstacleCollision();

			Debug.LogError("data: " + dataManager.getCollisionCount() + ", max: " + playerController.MaxCollisionGetter());

            if (dataManager.getCollisionCount() >= playerController.MaxCollisionGetter()) {
				Debug.LogError("1111");
                playerController.OnLifeSaverAsking(obstacle.isJump ? GameOverType.JumpObstacle : GameOverType.DuckObstacle, true);
                audioManager.playSoundEffect(SoundEffects.ObstacleCollisionSoundEffect);
            } else {
				Debug.LogError("2222");
                // the chase object will end the game
                if (playerController.MaxCollisionGetter() == 0 && chaseController != null) {
                    if (chaseController.isVisible()) {
                        audioManager.playSoundEffect(SoundEffects.ObstacleCollisionSoundEffect);
                        gameOver(obstacle.isJump ? GameOverType.JumpObstacle : GameOverType.DuckObstacle, true);
                    } else {
                        chaseController.approach();
                        audioManager.playSoundEffect(SoundEffects.ObstacleCollisionSoundEffect);
                    }
                } else {
                    // have the chase object approach the character when the collision count gets close
                    if (chaseController != null && dataManager.getCollisionCount() == playerController.MaxCollisionGetter() - 1) {
                        chaseController.approach();
                    }
                    playerController.ObstacleSideCollision();
                }
            }
		}
	}
	
    // initial collection is true when the player first collects a coin. It will be false when the coin is done animating to the coin element on the GUI
    // returns the value of the coin with the double coin power up considered
	public int coinCollected(){
        int coinValue = (powerUpManager.isPowerUpActive(PowerUpTypes.DoubleCoin) ? 2 : 1);
        audioManager.playSoundEffect(SoundEffects.CoinSoundEffect);
        return coinValue;
	}

    public void coinCollected(int coinValue){
		if (BankStaticData.Instance.getDoubleCoins ()) {
			coinValue = coinValue*2;	
		}

        dataManager.addToCoins(coinValue);
    }

    public void lifeSaverCollected(int coinValue){
		dataManager.addToLifeSaver (1);
    }

    public void activatePowerUp(PowerUpTypes powerUpType, bool activate) {
        if (activate) {
            if(powerUpManager.getActivePowerUp() == PowerUpTypes.None || powerUpManager.getActivePowerUp() == powerUpType) {
                // deactivate the current power up (if a power up is active) and activate the new one
                powerUpManager.deactivatePowerUp();
                powerUpManager.activatePowerUp(powerUpType);
                audioManager.playSoundEffect(SoundEffects.PowerUpSoundEffect);
            }
            else {
                activatePowerUp1(powerUpType,activate);
            }
        }
        playerController.activatePowerUp(powerUpType, activate);
        //guiManager.activatePowerUp(powerUpType, activate, dataManager.getPowerUpLength(powerUpType));
    }

    public void activatePowerUp1(PowerUpTypes powerUpType, bool activate){
        if (activate) {
            // deactivate the current power up (if a power up is active) and activate the new one
            powerUpManager.deactivatePowerUp1();
            powerUpManager.activatePowerUp1(powerUpType);
            audioManager.playSoundEffect(SoundEffects.PowerUpSoundEffect);
        }
        playerController.activatePowerUp(powerUpType, activate);
        //guiManager.activatePowerUp(powerUpType, activate, dataManager.getPowerUpLength(powerUpType));
    }


	
	public void gameOver(GameOverType gameOverType, bool waitForFrame){
		//dataManager.addToLifeSaver(10);
		//Debug.LogError("Game over: " + gameOverType.ToString() + "  " + waitForFrame );
		IsGameRunnnig = false;
        if (!gameActive && waitForFrame)
            return;
        gameActive = false;
        if (waitForFrame) {
			TutorialManager.Instance.StopAnimation ();
            // mecanim doesn't trigger the event if we wait until the frame is over
            playerController.gameOver(gameOverType);
            StartCoroutine(waitForFrameGameOver(gameOverType));
            //temp.SetActive(true);
        } else {
            inputController.gameOver();
            // Mission Manager's gameOver must be called before the Data Manager's gameOver so the Data Manager can grab the 
            // score multiplier from the Mission Manager to determine the final score
            if(gameOverType != GameOverType.Quit)
				missionManager.gameOver();
            coinGUICollection.gameOver();
            dataManager.gameOver();

            

            //if (chaseController != null)
            //    chaseController.gameOver(gameOverType);
            
            audioManager.playBackgroundMusic(false);
            audioManager.stopBackgroundMusic();

            //audioManager.playSoundEffect(SoundEffects.GameOverSoundEffect);
            guiManager.gameOver();
            cameraController.gameOver(gameOverType);
        }

		if(UM_GameServiceManager.Instance.ConnectionSate == UM_ConnectionState.CONNECTED) 
		{
			GameService.instance.SubmitScore ((long)DataManager.instance.getHighScore ());
		}

		#if UNITY_ANDROID//!UNITY_EDITOR && 
        registerGameOverEvent();
#endif
    }

    void OnApplicationQuit()
    {
		#if UNITY_ANDROID//!UNITY_EDITOR && 
        registerQuitEvent();
#endif
    }

    private void registerQuitEvent()
    {
        //Area730.AnalyticsController.instance.sendEvent(ognsdk.Event.GAME_EXITED);
    }

    private void registerLaunchEvent()
    {
        //Area730.AnalyticsController.instance.sendEvent(ognsdk.Event.GAME_LAUNCHED);
    }

    private void registerGameStartEvent()
    {
        //Area730.AnalyticsController.instance.sendEvent(ognsdk.Event.PLAY_BEGAN);
    }

    private void registerGameOverEvent()
    {
        //Area730.AnalyticsController.instance.sendEvent(ognsdk.Event.PLAY_ENDED);
    }


    public void SaveMeButton(GameOverType gameOvertype, bool Temp) {
		TutorialManager.Instance.StopAnimation ();
        chaseController.gameOver(gameOvertype) ;
        StartCoroutine ("Delay", gameOvertype);
		IsGameRunnnig = false;
    }

    
    IEnumerator Delay(GameOverType gameOvertype) {
        yield return new WaitForSeconds (0.1f);
        if (onPauseGame != null)
            onPauseGame(true);
        inputController.enabled = !true;        
        gamePaused = true;
		SaveManager.Instance.OnGameOver (gameOvertype);
    }

	public void ShowSecrecyBox(GameOverType gameOverType) {

		if (DataManager.instance.GetLevelPowerUpCount (PowerUpTypes.SecrecyBox) == 0) {
			cameraController.IsFromGameOver(true);

			if (onPauseGame != null)
				onPauseGame(false);
			gamePaused = false;
			gameOver (gameOverType, true);
		} 
		else {
            SecrecyboxManager.Instance.ShowSecrecy (gameOverType);
		}
	}

    public void gameQuit(GameOverType gameOverType, bool waitForFrame){
        if (!gameActive && waitForFrame)
            return;
        gameActive = false;

        playerController.gameOver(gameOverType);
        inputController.gameOver();
        if (chaseController != null)
            chaseController.gameOver(gameOverType);
        cameraController.gameOver(gameOverType);
        myGuiManager.QuitGame();
		IsGameRunnnig = false;
    }
	
	// Game over may be called from a trigger so wait for the physics loop to end
    private IEnumerator waitForFrameGameOver(GameOverType gameOverType){
		yield return new WaitForEndOfFrame();

        gameOver(gameOverType, false);
        myGuiManager.EndGame();
        // Wait a second for the ending animations to play
        yield return new WaitForSeconds(1.0f);
        
        Updatemanager.instance.OnUpdateCoins();
        ;//guiManager.showGUI(GUIState.EndGame);
	}
	
	public void restartGame(bool start){
		IsGameRunnnig = true;
        //temp.SetActive(false); 
        if (gamePaused) {
            if (onPauseGame != null)
                onPauseGame(false);
            gameOver(GameOverType.Quit, false);
        }
		dataManager.reset();
		infiniteObjectGenerator.reset();
		powerUpManager.reset();
		playerController.reset();
        cameraController.reset();
        if (chaseController != null)
            chaseController.reset();
        showTutorial = SettingStaticData.Instance.getTutorial();    
        if (showTutorial) {
            infiniteObjectGenerator.showStartupObjects(true);
        } else {
            // show the startup objects if there are any
            if (!infiniteObjectGenerator.showStartupObjects(false))
            {
                infiniteObjectGenerator.spawnObjectRun(true);
            }
        }
        infiniteObjectGenerator.readyFromReset();

        if (start)
        {
            startGame(true);
            myGuiManager.RestartGame();
        }
	}

    public void backToMainMenu(bool restart){

        if (gamePaused) {
            if (onPauseGame != null)
                onPauseGame(false);
            gameOver(GameOverType.Quit, false);
        }
        //print("")
        if (restart)
            restartGame(false);
        myGuiManager.EndToHomeScreen();
		IsGameRunnnig = false;
		//guiManager.showGUI(GUIState.MainMenu);
	}

    // activate/deactivate the character when going into the store. The GUIManager will manage the preview
    public void showStore(bool show) {
        // ensure the correct character is used
#if UNITY_3_5
		//print("mayank");
        character.SetActiveRecursively(!show);
#else
        InfiniteRunnerStarterPackUtility.ActiveRecursively(character.transform, !show);
#endif
        if (!show) {
            //print("MySelfHere");
            spawnCharacter();
            spawnChaseObject();
        }
    }
	
	public void pauseGame(bool pause)
    {
//		UM_ExampleStatusBar.text = pause.ToString ();

		if (pause)
		  myGuiManager.PauseScreen ();
		else 
		  myGuiManager.ResumeScreen ();

		// guiManager.showGUI(pause ? GUIState.Pause : GUIState.InGame);
        audioManager.playBackgroundMusic(!pause);

		if (!pause)				
			StartCoroutine ("DelayResume",pause);
		else {
			if (onPauseGame != null)
				onPauseGame(pause);
			inputController.enabled = !pause;		
			gamePaused = pause;	
		}
	}

	IEnumerator DelayResume(bool pause) {
		UIGameManager.Instance.ShowWall ("Resume in 3..");
		yield return new WaitForSeconds (1);
		UIGameManager.Instance.ShowWall ("Resume in 2..");
		yield return new WaitForSeconds (1);
		UIGameManager.Instance.ShowWall ("Resume in 1..");
		yield return new WaitForSeconds (1f);
		UIGameManager.Instance.ShowWall ("Go Go Go..");
		yield return new WaitForSeconds (0.3f);
		UIGameManager.Instance.HideWall ();
		IsGameRunnnig = true;
		if (onPauseGame != null)
			onPauseGame(pause);

		inputController.enabled = !pause;		
		gamePaused = pause;
	}
	
	public void upgradePowerUp(PowerUpTypes powerUpType)
	{
        // Can't upgrade if the player can't afford the power up
		Debug.Log ("(upgradePowerUp) Order "+(powerUpType));
        int cost = dataManager.getPowerUpCost(powerUpType);
        if (dataManager.getTotalCoins() < cost) {
			UIGameManager.Instance.PopUp ("Insuffiecient Coins");
            return;
        }
		dataManager.upgradePowerUp(powerUpType);
        dataManager.adjustTotalCoins(-cost);
		
        //--------------------------------------------------------------
        Updatemanager.instance.OnUpdateStore (StoreMenus.Upgrade);
        //-------------------------------------------------------------
	}

    public void selectCharacter(int character)
    {
        int characterCost = dataManager.getCharacterCost(character);
        if (characterCost == -1) { // can only select a character if it has been purchased
            if (dataManager.getSelectedCharacter() != character) {
                dataManager.setSelectedCharacter(character);
            }
        }
    }

    public void purchaseCharacter(int character)
    {
        int cost = dataManager.getCharacterCost(character);
        if (dataManager.getTotalCoins() < cost) {
			//UIGameManager.Instance.PopUp ("Insuffiecient Coins");
            return;
        }
        dataManager.purchaseCharacter(character);
        dataManager.setSelectedCharacter(character);
        dataManager.adjustTotalCoins(-cost);

        //--------------------------------------------
        Updatemanager.instance.OnUpdateStore (StoreMenus.Character);
        //-------------------------------------------
    }
	
	public void OnApplicationPause(bool pause)
	{
        if (gamePaused)
            return;

		if (onPauseGame != null)
			onPauseGame(pause);
	}

    //-------------------------------------------------------------------------------------------------------------------------

    public void purchasePermamnentPurchase(int Order)
    {
        int cost = dataManager.getPurchaseCost(Order);
        if (dataManager.getTotalCoins() < cost) {
			UIGameManager.Instance.PopUp ("Insuffiecient Coins");
            return;
        }
        dataManager.purchasePermanentPurchase(Order);
        dataManager.adjustTotalCoins(-cost);

        Updatemanager.instance.OnUpdateStore (StoreMenus.PermanentPurchases);
    }

	public bool purchaseAds()
	{
		int cost = (int) (BankStaticData.Instance.getCoinsAmount (0));
		if (dataManager.getTotalCoins() < cost) {
			UIGameManager.Instance.PopUp ("Insuffiecient Coins");
			return false;
		}
		dataManager.adjustTotalCoins(-cost);
		BankManager.Instance.SuccesfulPurchaseBankCoins (0);
		Updatemanager.instance.OnUpdateBank ();
		return true;

	}

    public void purchaseSingleUse(PowerUpTypes powerType)
	{
		// Can't upgrade if the player can't afford the power up
		//Debug.Log (powerType);
		int cost = dataManager.getPowerUpCost(powerType,true);
		//Debug.Log (cost);
		//Debug.Log (dataManager.getTotalCoins ());

		if (dataManager.getTotalCoins() < cost) {
			UIGameManager.Instance.PopUp ("Insuffiecient Coins");
			//Debug.Log ("Failed");
			return;
		}
		//Debug.Log ("Paased");

		dataManager.upgradePowerUp(powerType);
		dataManager.adjustTotalCoins(-cost);
    
        Updatemanager.instance.OnUpdateStore (StoreMenus.SingleUse);
    }

	public void decreaseSingleUse(PowerUpTypes powerType)
	{
		dataManager.degradePowerUp(powerType);
		Updatemanager.instance.OnUpdateStore (StoreMenus.SingleUse);
	}

	public void decreaseSingleUseOnly(PowerUpTypes powerType)
	{
		dataManager.degradePowerUp(powerType);
	}

    public bool GetShowTutorial()
    {
        return showTutorial = SettingStaticData.Instance.getTutorial();
    }

	public void CancelingGamePauseFromSecrecyBox()
	{
		if (onPauseGame != null)
			onPauseGame(false);
		gamePaused = false;
	}


    //-------------------------------------------------------------------------------------------------------------------------
}
