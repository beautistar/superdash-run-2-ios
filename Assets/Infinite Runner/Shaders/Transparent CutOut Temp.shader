﻿Shader "Infinite Runner/Transparent CutUot Temp" {
	Properties {
		_MainTex ("Main Texture", 2D) = "black" {}
		_TintColor ("Tint Color", Color) = (0, 0, 0, 0)
		_Cutoff ("Alpha Cutoff", Range(0, 1)) = 0.0
		_NearCurve ("Near Curve", Vector) = (0, 0, 0, 0)
		_FarCurve ("Far Curve", Vector) = (0, 0, 0, 0)
	}
	SubShader {
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
		Blend SrcAlpha OneMinusSrcAlpha
		
        Pass { // pass 0

			CGPROGRAM
			#pragma vertex vert_img
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest 
			#include "UnityCG.cginc"
						
			uniform sampler2D _MainTex;
			uniform fixed4 _TintColor;
			uniform float _Cutoff;
			uniform float4 _NearCurve;
			uniform float4 _FarCurve;

			struct fragmentInput
			{
				float4 pos : POSITION;
				fixed4 color : COLOR;
				float2 uv : TEXCOORD0;
				float distanceSquared : TEXCOORD1;
			};

			fragmentInput vert(appdata_full v)
			{
				fragmentInput o;

				// Apply the curve
                //float4 pos = mul(UNITY_MATRIX_MV, v.vertex);
                //o.distanceSquared = pos.z * pos.z * _Dist;
                pos.x += (_NearCurve.x - max(1.0 - o.distanceSquared / _FarCurve.x, 0.0) * _NearCurve.x);
                pos.y += (_NearCurve.y - max(1.0 - o.distanceSquared / _FarCurve.y, 0.0) * _NearCurve.y);
                //o.pos = mul(UNITY_MATRIX_P, pos); 
				//o.color = v.color;
				//o.uv = TRANSFORM_TEX(v.texcoord,_MainTex);

				return o;
			}
			
			fixed4 frag(v2f_img i) : COLOR
			{
				fixed4 color = tex2D(_MainTex, i.uv);

				if (color.a < _Cutoff) {
					color.a = 0;
				}

				return color * _TintColor;
			}
			
			ENDCG
        } // end pass
	} 
	FallBack "Diffuse"
}
